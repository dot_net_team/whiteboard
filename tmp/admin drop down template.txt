

 ----------- font drop down ---------------------------

		<div class="form-group">
            @Html.LabelFor(model => model.HomePage_Top_Header_String_FONT, htmlAttributes: new { @class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.DropDownListFor(model => model.HomePage_Top_Header_String_FONT, BusinessLayer.StaticValues.GetAllTypeFace(), Model.HomePage_Head_PageTitle_FONT, new { @class = "form-control" })
                @Html.ValidationMessageFor(model => model.HomePage_Top_Header_String_FONT, "", new { @class = "text-danger" })
            </div>
        </div>
		
		
		
	 ----------- icon drop down ---------------------------
	 
		<div class="form-group">
            @Html.LabelFor(model => model.HomePage_Head_Tab1_ICON, htmlAttributes: new { @class = "control-label col-md-2" })
            <div class="col-md-10">
                @{List<SelectListItem> DropdownItemList = (List<SelectListItem>)(ViewBag.FontAwesomeList);                }
                @Html.DropDownListFor(model => model.HomePage_Head_Tab1_ICON, DropdownItemList, Model.HomePage_Head_Tab1_ICON, new { @class = "fa form-control", @style = "\"font-family: 'FontAwesome',Arial;\"", @id = "selectamp" })
                @Html.ValidationMessageFor(model => model.HomePage_Head_Tab1_ICON, "", new { @class = "text-danger" })
            </div>
        </div>
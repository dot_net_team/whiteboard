﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebHelper
{
    public class FlaggedAccountsHelper
    {
        //try to connect this to database in the future
        public static string GetFlagMessage(int i)
        {
            switch (i)
            {
                //case 0 is active, no need to check
                case 1:
                    return "Inactive";
                case 2:
                    return "Banned";
                case 3:
                    return "Deleted";
                default:
                    return "err";
            }

        }
    }
}

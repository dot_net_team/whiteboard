﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WebHelper
{
    public class WebSession
    {
        /*
         * something is wrong with the GetSession, 
         * Current.session is saved as a key,
         * but returns null when retriving it (in some instances)
         */

        private static string GetSession(string SessionKey)
        {
            SessionKey = SessionKey.Remove(0, 4);
            //HttpContext.Current.Session["hakuna_matata"] = "matata_hakuna";
            //var y = HttpContext.Current.Session["hakuna_matata"];
            var tmp = HttpContext.Current.Session[SessionKey];
            if (tmp == null)
                return null;
            return tmp.ToString();
        }
        private static void SetSession(string SessionKey, string val)
        {
            SessionKey = SessionKey.Remove(0, 4);
            HttpContext.Current.Session[SessionKey] = val;
        }

        public static string COOKIE_Token
        {
            get
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies.Get("Token");
                if (cookie != null)
                    return cookie.Value;
                else return null;
            }
            set
            {
                HttpContext.Current.Response.Cookies.Remove("Token");
                var userCookie = new HttpCookie("Token", value);
                userCookie.Expires.AddDays(365);
                HttpContext.Current.Response.SetCookie(userCookie);
            }
        }

        public static void COOKIE_Clear()
        {
            HttpContext.Current.Response.Cookies.Remove("Token");
            //
            //delete user cookie in the database
            //
            //spDeleteUserCookie
        }

        public static string Token
        {
            get { return GetSession(MethodBase.GetCurrentMethod().Name); }set { SetSession(MethodBase.GetCurrentMethod().Name, value); }
        }

        public static string Email
        {
            get { return GetSession(MethodBase.GetCurrentMethod().Name); }
            set { SetSession(MethodBase.GetCurrentMethod().Name, value); }

        }

        public static int AccountID
        {
            get { return Convert.ToInt32((GetSession(MethodBase.GetCurrentMethod().Name) ?? null)); }
            set { SetSession(MethodBase.GetCurrentMethod().Name, value.ToString()); }
        }

        public static int UserID
        {
            get { return Convert.ToInt32(MethodBase.GetCurrentMethod().Name); }
            set { SetSession(MethodBase.GetCurrentMethod().Name, value.ToString()); }
        }

        public static string UserName
        {
            get { return GetSession(MethodBase.GetCurrentMethod().Name); }
            set { SetSession(MethodBase.GetCurrentMethod().Name, value); }
        }

        public static string ParamAccessOnce01
        {
            get
            {
                string Message = GetSession(MethodBase.GetCurrentMethod().Name);
                SetSession(MethodBase.GetCurrentMethod().Name, null);
                return Message;
            }
            set { SetSession(MethodBase.GetCurrentMethod().Name, value); }
        }

        public static string ParamAccessTmp1
        {
            get
            {
                string Message = GetSession(MethodBase.GetCurrentMethod().Name);
                return Message;
            }
            set { SetSession(MethodBase.GetCurrentMethod().Name, value); }
        }

        public static string ParamAccessTmp2
        {
            get
            {
                string Message = GetSession(MethodBase.GetCurrentMethod().Name);
                return Message;
            }
            set { SetSession(MethodBase.GetCurrentMethod().Name, value); }
        }

        public static void SessionClear()
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebHelper
{
    public class PasswordSalter
    {

        public static string GetSalted(string pw)
        {
            string salt = PreferredHash(DateTime.UtcNow.ToString());
            string salted = PreferredHash(string.Format("{0}{1}", pw, salt));

            return string.Format("{0}{1}", salted, salt);
        }
        
        public static bool VerifySalt(string input, string stored)
        {
            string salt = ExtractSaltValue(stored);
            string salted = PreferredHash(string.Format("{0}{1}", input, salt));

            if (string.Format("{0}{1}", salted, salt) == stored)
                return true;
            else
                return false;
        }

        private static string ExtractSaltValue(string hashed)
        {
            if(hashed.Length != 128)
            {
                throw new Exception("Wrong password hash exception");
            }
            string tmp = "";
            for(int i = 64; i < 128; i++)
            {
                tmp += hashed[i];
            }
            return tmp;
        }

        public static string PreferredHash(string input)
        {
            string hashed = Hash.SHA256(input);
            if(hashed.Length > 64)
            {
                //trim
                string tmp = "";
                for(int i=0; i < 64; i++)
                {
                    tmp += hashed[i];
                }
                hashed = tmp;
            }
            else if(hashed.Length != 64)
            {
                throw new Exception("Hash lenght error"); //create new custom exception for this --noli
            }
            return hashed;
        }
    }
}

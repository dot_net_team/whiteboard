﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WhiteBoard.Tests.WebHelperUnitTest
{
    [TestClass]
    public class WebhelperUnitTest
    {
        [TestMethod]
        public void PasswordSalting()
        {
            string password = "p@ssw0rD!!$$";
            string simulatedDBpassword = WebHelper.PasswordSalter.GetSalted(password);

            Assert.AreEqual(WebHelper.PasswordSalter.VerifySalt(password, simulatedDBpassword), true);

        }

        [TestMethod]
        public void Login()
        {
            string s1 = "yes";
            string s2 = "yes";
            Assert.AreEqual(s1,s2);
        }

        [TestMethod]
        public void Logout()
        {
            Assert.AreEqual("yes", "no");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

namespace BusinessLayer
{
    public class ConfigurationStrings_HomeModel
    {
        [Display(Name = "Editor ID")]
        public int CurrentEditor { get; set; }

        public ConfigurationStrings_HomeModel()
        {
        }

        public ConfigurationStrings_HomeModel(int Editor)
        {
            CurrentEditor = Editor;
        }

        [Display(Name = "Website Name")]
        public string HomePage_Head_WebsiteName
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        [Display(Name = "Page Title")]
        public string HomePage_Head_PageTitle
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        [Display(Name = "Font")]
        public string HomePage_Head_PageTitle_FONT
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get {
                return ConfigurationManagementAccess.GetFontSafe(MethodBase.GetCurrentMethod().Name);
            }
        }

        [Display(Name = "header Title")]
        public string HomePage_Top_Header_String
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        [Display(Name = "Font")]
        public string HomePage_Top_Header_String_FONT
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        [Display(Name = "header subtitle")]
        public string HomePage_Top_Header_SubString
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }


        [Display(Name = "H2 Font")]
        public string HomePage_Head_PageTitle_H2_FONT
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetFontSafe(MethodBase.GetCurrentMethod().Name); }
        }

        [Display(Name = "H4 Font")]
        public string HomePage_Head_PageTitle_H4_FONT
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        [Display(Name = "Lists Font")]
        public string HomePage_Head_PageTitle_Li_FONT
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        [Display(Name = "Tab 1")]
        public string HomePage_Head_Tab1
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        [Display(Name = "Tab 1: icon")]
        public string HomePage_Head_Tab1_ICON
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetFASafeString(MethodBase.GetCurrentMethod().Name); }
        }

        [Display(Name = "tab 1 description")]
        public string HomePage_Head_Tab1_detail
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        public string HomePage_Head_Tab2
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        [Display(Name = "Tab 2: icon")]
        public string HomePage_Head_Tab2_ICON
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetFASafeString(MethodBase.GetCurrentMethod().Name); }
        }

        public string HomePage_Head_Tab2_detail
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        public string HomePage_Head_Tab3
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        [Display(Name = "Tab 3: icon")]
        public string HomePage_Head_Tab3_ICON
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetFASafeString(MethodBase.GetCurrentMethod().Name); }
        }

        public string HomePage_Head_Tab3_detail
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        public string HomePage_Head_Tab_Login
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        public string HomePage_Head_Tab_Logout
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        public string HomePage_Copyright
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        public string HomePage_Tab1_1
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Tab1_2
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Tab1_3
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        public string HomePage_Tab1_1_1
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Tab1_2_1
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Tab1_3_1
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }

        ////////////////////////////////////////////
        ////////////////////////////////////////////
        // the next following strings should be connected to the bussiness layer becasu this value affects computations
        ////////////////////////////////////////////

        #region Bachelor
        public string HomePage_Plan1_Name
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Plan1_Monthly
        {
            set {
                int v = Convert.ToInt32(value.Replace("$","")) * 100;
                PaymentManagementAccess.UpdateSubcriptionPayment(1, v);

                ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value);
            }

            get {
                string gs = ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name);
                double d = Convert.ToDouble(gs.Replace("$", ""));
                string s = string.Format("${0}", d);
                return s; }
        }
        public string HomePage_Plan1_NumberOfCourse
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Plan1_NumberOfLesson
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Plan1_NumberOfQuestionBank
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Plan1_TransactionFee
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        #endregion
        #region Doctor
        public string HomePage_Plan2_Name
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Plan2_Monthly
        {
            set
            {
                int v = Convert.ToInt32(value.Replace("$", "")) * 100;
                PaymentManagementAccess.UpdateSubcriptionPayment(3, v);
                ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value);
            }

            get
            {
                string gs = ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name);
                double d = Convert.ToDouble(gs.Replace("$", ""));
                string s = string.Format("${0}", d);
                return s;
            }
        
        }
        public string HomePage_Plan2_NumberOfCourse
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Plan2_NumberOfLesson
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Plan2_NumberOfQuestionBank
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Plan2_TransactionFee
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Plan2_ExtraFeature1
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        #endregion
        #region Master
        public string HomePage_Plan3_Name
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Plan3_Monthly
        {
            set
            {
                int v = Convert.ToInt32(value.Replace("$", "")) * 100;
                PaymentManagementAccess.UpdateSubcriptionPayment(2, v);
                ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value);
            }

            get
            {
                string gs = ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name);
                double d = Convert.ToDouble(gs.Replace("$", ""));
                string s = string.Format("${0}", d);
                return s;
            }
        }
        public string HomePage_Plan3_NumberOfLesson
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Plan3_NumberOfCourse
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Plan3_NumberOfQuestionBank
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        public string HomePage_Plan3_TransactionFee
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
        #endregion
    }

    public class COnfigurationStrings_Banner
    {
        [Display(Name = "Editor ID")]
        public int CurrentEditor { get; set; }

        public COnfigurationStrings_Banner()
        {
        }

        public COnfigurationStrings_Banner(int Editor)
        {
            CurrentEditor = Editor;
        }

        [Display(Name = "Website Banner")]
        public string HomePage_Head_Banner
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetString(MethodBase.GetCurrentMethod().Name); }
        }
    }
}

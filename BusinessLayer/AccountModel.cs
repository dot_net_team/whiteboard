﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;

namespace BusinessLayer
{
    public interface IAccountCRUD
    {
        void Create();
        void Read(string parameter);
        void Update();
        void Delete();
    }

    public class AccountModel : IAccountCRUD
    {
        protected AccountDBAccess AccountsDB;

        public int AccountID { get; set; }

        //BASIC ACCOUNT INFO
        #region basic info
        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(16, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Confirm password doesn't match, Type again !")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        public virtual byte Role { get; set; }

        [Required]
        [StringLength(35, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [DataType(DataType.Text)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(35, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [DataType(DataType.Text)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Country")]
        public string Country { get; set; }

        #endregion

        public DateTime DateCreated { get; set; }
        public DateTime LastLogin { get; set; }

        //credit card info temp holder (destroy after sending to third party)
        #region credit card validation
        [Required]
        [StringLength(35, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [DataType(DataType.Text)]
        [Display(Name = "Name of Card Holder")]
        public string CardHolderName { get; set; }

        [Required]
        [DataType(DataType.CreditCard)]
        [Display(Name = "Card Number")]
        public long CardNumber { get; set; }

        [Required]
        [Range(1,12,ErrorMessage = "Enter month 01-12")]
        [Display(Name = "Card Expiration Month")]
        public int CardExpirationMonth{ get; set; }

        [Required]
        [Range(16, 99, ErrorMessage = "Enter Year e.g. 31 for 2031")]
        [Display(Name = "Card Expiration Year")]
        public int CardExpirationYear { get; set; }

        [Required]
        [Range(100, 999, ErrorMessage = "Enter 3 digit CVC located at the back of the card")]
        [Display(Name = "Card CVC")]
        public int CardCVC { get; set; }
        #endregion

        /// <summary>
        /// required field by sir Ng
        /// --0 active, 1 inactive, 2 banned, 3 deleted,
        /// </summary>
        public int Flag { get; set; }

        public void DestroyCardInfo()
        {
            CardHolderName = null;
            CardCVC = 0;
            CardExpirationMonth = 0;
            CardExpirationYear = 0;
            CardNumber = 0;
        }

        public AccountModel()
        {
            AccountsDB = new AccountDBAccess();
        }

        public string[] ReadRole(string email)
        {
            List<string> Roles = new List<string>();
            using (SqlDataReader Reader = AccountsDB.ReadRole(Email))
            {
                if (Reader.HasRows == false)
                {
                    throw new AccountNotFoundException();
                }
                
                while(Reader.Read())
                {
                    Roles.Add(Reader["Name"].ToString());
                }
            }
            return Roles.ToArray();
        }

        public virtual void Read(int AccountID)
        {
            using (SqlDataReader Reader = AccountsDB.ReadAccountByID(AccountID))
            {
                this.FillData(Reader);
            }
            
        }

        public virtual void Read(string Email)
        {
            using (SqlDataReader Reader = AccountsDB.ReadAccountByEmail(Email) )
            {
                this.FillData(Reader);
            }
        }

        public void ReadByID(int AccountID)
        {
            using (SqlDataReader Reader = AccountsDB.ReadAccountByID(AccountID))
            {
                this.FillData(Reader);
            }
        }

        protected virtual void FillData(SqlDataReader Reader)
        {
            if (Reader.HasRows == false)
            {
                throw new AccountNotFoundException();
            }
            Reader.Read();
            ///Account
            this.AccountID = Convert.ToInt32(Reader["ID"]);
            this.Email = Reader["Email"].ToString();
            this.Role = Convert.ToByte(Reader["Role"]);
            this.Password = Reader["Password"].ToString();
            ///Detail
            this.FirstName = Reader["FirstName"].ToString();
            this.LastName = Reader["LastName"].ToString();
            this.DateCreated = Convert.ToDateTime(Reader["DateCreated"]).ToLocalTime();
            this.LastLogin = Convert.ToDateTime(Reader["LastLogin"]).ToLocalTime();
            this.Country = Reader["Country"].ToString();
            this.Flag = Convert.ToInt32(Reader["Flag"]);

            AccountsDB.CloseConnection();
        }

        public virtual void Create()
        {
            throw new NotImplementedException();
        }

        public virtual void Update()
        {
            throw new NotImplementedException();
        }

        internal void UpdatePassword()
        {
            AccountsDB.EditAccountPassword(this.AccountID, this.Password);
        }

        public virtual void Delete()
        {
            throw new NotImplementedException();
        }

        
    }

    public class AccountNotFoundException : Exception
    {
        public AccountNotFoundException() : base() { }
        public AccountNotFoundException(string message) : base(message) { }
    }
}

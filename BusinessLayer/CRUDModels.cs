﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class AccountCrudModel
    {
        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(35, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [DataType(DataType.Text)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(35, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [DataType(DataType.Text)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Country")]
        public string Country { get; set; }
    }

    public class StudentCrudModel : AccountCrudModel
    {
        private StudentModel currentstud;

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "School")]
        public string School { get; set; }

        public void ReadCurrentData(int id)
        {
            currentstud = new StudentModel();
            currentstud.Read(id);

            this.Email = currentstud.Email;
            this.FirstName = currentstud.FirstName;
            this.LastName = currentstud.LastName;
            this.Country = currentstud.Country;
            this.School = currentstud.School;
        }

        public void SaveCurrentData(int id)
        {
            currentstud = new StudentModel();
            currentstud.Read(id);

            currentstud.Email = this.Email;
            currentstud.FirstName = this.FirstName;
            currentstud.LastName = this.LastName;
            currentstud.Country = this.Country;
            currentstud.School = this.School;

            currentstud.Update();
        }
    }

    public class InstructorCRUDModel : AccountCrudModel
    {
        InstructorModel CurrentInstructor;

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Organization")]
        public string Organization { get; set; }

        public void ReadCurrentData(int id)
        {
            CurrentInstructor = new InstructorModel();
            CurrentInstructor.Read(id);

            this.Email = CurrentInstructor.Email;
            this.FirstName = CurrentInstructor.FirstName;
            this.LastName = CurrentInstructor.LastName;
            this.Country = CurrentInstructor.Country;
            this.Organization = CurrentInstructor.Organization;
        }

        public void SaveCurrentData(int id)
        {
            CurrentInstructor = new InstructorModel();
            CurrentInstructor.Read(id);

            CurrentInstructor.Email = this.Email;
            CurrentInstructor.FirstName = this.FirstName;
            CurrentInstructor.LastName = this.LastName;
            CurrentInstructor.Country = this.Country;
            CurrentInstructor.Organization = this.Organization;
            

            CurrentInstructor.Update();
        }
    }

    public class ChangePasswordModel
    {
        private AccountModel current = new AccountModel();     

        public void SaveCurrentData(int id)
        {
            current = new AccountModel();
            current.Read(id);

            current.Password = this.Password;

            current.UpdatePassword();
        }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current Password")]
        [StringLength(16, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        public string CurrentPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Confirm password doesn't match, Type again !")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }
    }

}

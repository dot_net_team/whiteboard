﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class WebTriggers
    {
        public static bool LoggedInEvent(int id)
        {
            return DataAccessLayer.UpdateDBAccess.UpdateLastLogin(id);
        }
    }
}

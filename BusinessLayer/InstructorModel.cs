﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Payments.Stripe;
using DataAccessLayer;

namespace BusinessLayer
{

    
    public class InstructorModel : AccountModel
    {
        protected CourseDBAccess CoursesDB;
        protected InstructorDBAccess InstructorDB;
        public InstructorModel() : base()
        {
            InstructorDB = new InstructorDBAccess();

            base.Role = (byte)1;
        }

        public int InstructorID { get; set; }
        public string LessonTitle { get; set; }
        public string Description { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Subject Area")]
        public string SubjectArea { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "School / Organization")]
        public string Organization { get; set; }

        [Required]
        [Display(Name = "Subscription")]
        public int SubscriptionPlan { get; set; }

        [Required]
        [Display(Name = "Payment Mode")]
        public int Payment { get; set; }

        public override void Create()
        {
            bool paid = false;
            //
            // Stripe here before saving data to db
            //

            string API_KEY = ConfigurationManager.AppSettings["StripeKey"];
            StripePayment CurrentPayment = new StripePayment(API_KEY);
            StripeCreditCardInfo CardInfo = new StripeCreditCardInfo();

            CardInfo.ExpirationMonth = this.CardExpirationMonth;
            CardInfo.ExpirationYear = this.CardExpirationYear;
            CardInfo.Number = this.CardNumber.ToString();
            CardInfo.CVC = this.CardCVC.ToString();

            int AmountToPay = DataAccessLayer.ItemsDBAccess.GetSubscriptionPriceById(this.Payment);

            switch (Payment)
            {
                case 1:
                    AmountToPay *= 1 * 100; //monthly
                    break;
                case 2:
                    AmountToPay *= 12 * 100; // yearly
                    break;
            }

            try
            {
                StripeCharge CurrentCharge = CurrentPayment.Charge(AmountToPay, "usd", CardInfo, "Whiteboard::Instructor::Registered: " + this.LastName + ", "+ this.FirstName);
                paid = true;
            }
            catch(StripeException SEx)
            {
                throw SEx;
            }

            //////////
            //make sure we are paid before saing data to db
            /////////
            if (paid)
            {
                base.AccountsDB.CreateInstructorAccount(this.Email, this.Password, this.FirstName, this.LastName, this.Title, this.SubjectArea, this.Organization, this.Payment, this.SubscriptionPlan, this.Country);
            }
            else
            {
                throw new Exception("Registration Error");
            }
        }

        //public void CreateInstructor(string Email, string Password, string FirstName, string LastName, string Title, string SubjectArea,string Organization,int Payment, int Subscription)
        //{
        //    ///
        //    base.Email = Email;
        //    base.Password = Password;
        //    base.FirstName = FirstName;
        //    base.LastName = LastName;
        //    ///
        //    this.Title = Title;
        //    this.SubjectArea = SubjectArea;
        //    this.Organization = Organization;
        //    ///
        //    this.Payment = Payment;
        //    this.SubscriptionPlan = Subscription;

        //    ///
        //    this.Create();
        //}

        public override void Delete()
        {
            base.AccountsDB.EditAccountFlagInDB(this.AccountID, 3);
        }

        public override void Read(int ID)
        {
            base.Read(ID);
            using (SqlDataReader Reader = AccountsDB.ReadInstructor(base.AccountID))
            {
                FillData(Reader);
                
            }
        }

        public override void Read(string Email)
        {
            base.Read(Email);
            using (SqlDataReader Reader = AccountsDB.ReadInstructor(base.AccountID))
            {
                FillData(Reader);
            }
        }

        private new void FillData(SqlDataReader Reader)
        {
            if (Reader.HasRows == false)
            {
                throw new AccountNotFoundException();
            }
            ///this
            Reader.Read();
            this.Title = Reader["Title"].ToString();
            this.InstructorID = Convert.ToInt32(Reader["ID"].ToString());
            this.SubjectArea = Reader["SubjectArea"].ToString();
            this.Organization = Reader["Organization"].ToString();
            this.SubscriptionPlan = Convert.ToInt32(Reader["Plan"]);
            this.Payment = Convert.ToInt32(Reader["PaymentMode"]);
            base.AccountsDB.CloseConnection();
        }

        public override void Update()
        {
            base.AccountsDB.EditInstructorAccount(this.AccountID, this.FirstName, this.LastName, this.Organization, this.Country);
        }

        public static SideMenuModel SideMenu(System.Web.Mvc.UrlHelper Url,int AccountId)
        {
            // System.Web.Mvc.UrlHelper Url = new System.Web.Mvc.UrlHelper();

            SideMenuModel Menu = new SideMenuModel();
            Menu.TopName = "Instructor DashBoard";
            Menu.SubMenuName = "Tools";

            Menu.SubMenuLink.Add(Url.Action("Course", "Instructor", new { id = AccountId }));
            Menu.SubMenuListName.Add("Courses");

            Menu.SubMenuLink.Add(Url.Action("StudentProgress", "Instructor", new { AccountID = AccountId }));
            Menu.SubMenuListName.Add("Student Progress");

            //Menu.SubMenuLink.Add(Url.Action("QuestionBank", "Instructor", new { AccountID = AccountId }));
            //Menu.SubMenuListName.Add("Question Bank");

            Menu.SubMenuLink.Add(Url.Action("EditInfo", "Instructor"));
            Menu.SubMenuListName.Add("Edit");

            Menu.SubMenuLink.Add(Url.Action("DeleteAccount", "Instructor"));
            Menu.SubMenuListName.Add("Delete");

            Menu.SubMenuLink.Add(Url.Action("ChangePassword", "Instructor"));
            Menu.SubMenuListName.Add("Change Password");

            //@Html.ActionLink("Courses", "Course", new { id = Model.InstructorID }, null)

            Menu.SubMenuLink.Add(Url.Action("Index", "Instructor"));
            Menu.SubMenuListName.Add("Back to List");

            return Menu;
        }
    }
}

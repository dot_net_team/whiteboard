﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;

namespace BusinessLayer
{
    public class QuizModel
    {

        protected CourseDBAccess CoursesDB;
        protected InstructorDBAccess InstructorDB;

        public QuizModel()
        {

            CoursesDB = new CourseDBAccess();
            InstructorDB = new InstructorDBAccess();
        }
        public int LessonID { get; set; }
        public int CourseID { get; set; }
        public string TextContext { get; set; }
        public string Title { get; set; }
        public int QuestionBankID { get; set; }
        public int numItems { get; set; }
        public int passingGrade { get; set; }

        public virtual void ReadLessons()
        {

            using (SqlDataReader Reader1 = InstructorDB.ReadLessons())
            {
                this.datafill(Reader1);
            }
        }

        public virtual void Addquiz(string QuizTitle,int LessonID,int ID,int items,int passing)
        {

            InstructorDB.AddQuiz(QuizTitle,LessonID, ID, items, passing);
        }
        public List<QuizModel> lessonlist = new List<QuizModel>();
        protected virtual void datafill(SqlDataReader Reader1)
        {

            if (Reader1.HasRows == false)
            {
                //throw new AccountNotFoundException(); // don't use Accoutn not found exception here lol... haha --cleanup branch
                //throw new Exception("Course not found Exception");
            }
            while (Reader1.Read())
            {
                QuizModel obj = new QuizModel();
                //Courses
                obj.LessonID = Convert.ToInt32(Reader1["ID"]);
                obj.CourseID = Convert.ToInt32(Reader1["CourseID"]);
                obj.TextContext = Reader1["TextContent"].ToString();
                obj.Title = Reader1["Title"].ToString();


                lessonlist.Add(obj);

            }



        }

       
    }
}

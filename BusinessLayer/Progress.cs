﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{

    public class ProgressModel
    {
        protected CourseDBAccess CoursesDB;

        public int StudentID { get; set; }
        public int CourseID { get; set; }
        public int Progress { get; set; }
        public string InstructorID { get; set; }
        public int AccountID { get; set; }

        public List<ProgressModel> listProgress = new List<ProgressModel>();
        public ProgressModel()
        {
            CoursesDB = new CourseDBAccess();
        }

        public void Read()
        {
            using (SqlDataReader Reader = CoursesDB.GetInstructorIDbyAccID(AccountID.ToString()))
            {
                while (Reader.Read())
                {
                    InstructorID = Reader["ID"].ToString();
                }
            }
            using (SqlDataReader Reader = CoursesDB.GetProgressforInstructor(Convert.ToInt32(InstructorID)))
            {
                if (Reader.HasRows)
                    this.FillInstructorProgress(Reader);
            }




        }



        protected virtual void FillInstructorProgress(SqlDataReader Reader)
        {
            if (Reader.HasRows == false)
            {
                //throw new AccountNotFoundException(); // don't use Accoutn not found exception here lol... haha --cleanup branch
                throw new Exception("Course not found Exception");
            }
            while (Reader.Read())
            {
                ProgressModel ProgressModelobj = new ProgressModel();
                ProgressModelobj.StudentID = Convert.ToInt32(Reader["StudentID"]);
                ProgressModelobj.CourseID = Convert.ToInt32(Reader["CourseID"]);
                ProgressModelobj.Progress = Convert.ToInt32(Reader["Progress"]);

                listProgress.Add(ProgressModelobj);

            }
        }

    }
}


﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ConfigurationColorModel_Home
    {
        [Display(Name = "Editor ID")]
        public int CurrentEditor { get; set; }

        public ConfigurationColorModel_Home()
        {
        }

        public ConfigurationColorModel_Home(int Editor)
        {
            CurrentEditor = Editor;
        }

        [Display(Name = "Navigation ")]
        public string HomePage_Navigation
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetColorSafe(MethodBase.GetCurrentMethod().Name); }
        }

        [Display(Name = "Website Name")]
        public string HomePage_Head_WebsiteName_COLOR
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetColorSafe(MethodBase.GetCurrentMethod().Name); }
        }

        [Display(Name = "Header")]
        public string HomePage_Top_Header_COLOR
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetColorSafe(MethodBase.GetCurrentMethod().Name); }
        }

        [Display(Name = "H2")]
        public string HomePage_H2_COLOR
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetColorSafe(MethodBase.GetCurrentMethod().Name); }
        }

        [Display(Name = "H4")]
        public string HomePage_H4_COLOR
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetColorSafe(MethodBase.GetCurrentMethod().Name); }
        }

        [Display(Name = "Lists")]
        public string HomePage_Li_COLOR
        {
            set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
            get { return ConfigurationManagementAccess.GetColorSafe(MethodBase.GetCurrentMethod().Name); }
        }

        //[Display(Name = "Footer")]
        //public string HomePage_Top_Footer_COLOR
        //{
        //    set { ConfigurationManagementAccess.EditString(MethodBase.GetCurrentMethod().Name, CurrentEditor, value); }
        //    get { return ConfigurationManagementAccess.GetColorSafe(MethodBase.GetCurrentMethod().Name); }
        //}
    }
}

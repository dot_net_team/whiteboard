﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BusinessLayer
{

    public class AdminModel : AccountModel, IAccountCRUD
    {

        public AdminModel() : base()
        {
            base.Role = (byte)0;
        }

        public override void Create()
        {
                AccountsDB.CreateAdminAccount(Email, Password, FirstName, LastName);
        }
        public void FillInfo(string Email, string Password, string FirstName, string LastName)
        {
            base.Email = Email;
            base.Password = Password;
     
            base.FirstName = FirstName;
            base.LastName = LastName;
        }
        public override void Delete()
        {
            throw new NotImplementedException();
        }

        public override void Read(string Email)
        {
            base.Read(Email);
        }

        public override void Update()
        {
            throw new NotImplementedException();
        }

        public List<AccountModel> GetAllAccounts()
        {
            List<AccountModel> Accounts = new List<AccountModel>();

            using (SqlDataReader Reader = base.AccountsDB.GetAllAccountsFromDB() )
            {
                while (Reader.Read())
                {
                    AccountModel account = new AccountModel();

                    account.Read(Convert.ToInt32(Reader["ID"]));

                    Accounts.Add(account);
                }
                base.AccountsDB.CloseConnection();
            }
            return Accounts;
        }

        public void EditAccountFlag(int passedId, int flag)
        {
            AccountsDB.EditAccountFlagInDB(passedId, flag);
        }

        public static SideMenuModel SideMenu(UrlHelper Url)
        {
           // System.Web.Mvc.UrlHelper Url = new System.Web.Mvc.UrlHelper();

            SideMenuModel Menu = new SideMenuModel();
            Menu.TopName = "Admin DashBoard";
            Menu.SubMenuName = "Tools";

            Menu.SubMenuLink.Add(Url.Action("StringChanger", "Admin"));
            Menu.SubMenuListName.Add("Edit Strings");

            Menu.SubMenuLink.Add(Url.Action("ColorChanger", "Admin"));
            Menu.SubMenuListName.Add("Edit Colors");

            Menu.SubMenuLink.Add(Url.Action("ViewAllUsers", "Admin"));
            Menu.SubMenuListName.Add("View All Users");

            Menu.SubMenuLink.Add(Url.Action("Logs", "Admin"));
            Menu.SubMenuListName.Add("View DB Log");

            Menu.SubMenuLink.Add(Url.Action("Index", "Admin"));
            Menu.SubMenuListName.Add("Back to List");

            return Menu;
        }
    }
}

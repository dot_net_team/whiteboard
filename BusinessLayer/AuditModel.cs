﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class AuditModel
    {
        public int LogID { get; set; }
        public int UserID { get; set; }
        public string Command { get; set; }
        public string SqlCommand { get; set; }
        public string Description { get; set; }
        public DateTime DateExecuted { get; set; }
    }

    public class AuditModels
    {
        public List<AuditModel> Audits { get; private set; }
        public void ReAudit(int o,int n)
        {
            AuditDBAccess db = new AuditDBAccess();
            SqlDataReader Reader = db.ReadLogs(o, n);
            if (Reader.HasRows == false)
            {
                AuditModel NewAudit = new AuditModel();

                NewAudit.LogID = 0;
                NewAudit.UserID = 0;
                NewAudit.Command = "none";
                NewAudit.SqlCommand = "none";
                NewAudit.DateExecuted = DateTime.MinValue;
                Audits.Add(NewAudit);
            }
            //this
            while (Reader.Read())
            {
                AuditModel NewAudit = new AuditModel();

                NewAudit.LogID = Convert.ToInt32(Reader["LOG_ID"]);
                NewAudit.UserID = Convert.ToInt32(Reader["USER_ID"]);
                NewAudit.Command = Reader["COMMAND"].ToString();
                NewAudit.SqlCommand = Reader["SQLCOMMAND"].ToString();
                NewAudit.Description = Reader["DESCRIPTION"].ToString();
                NewAudit.DateExecuted = Convert.ToDateTime(Reader["DATE_EXECUTED"]);
                Audits.Add(NewAudit);
            }
            db.CloseConnection();
        }
        public AuditModels()
        {
            Audits = new List<AuditModel>();          
        }
    }
}

﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class TokenModel
    {

        public static void SetToken(int AccountID, int Role,string Token)
        {
            AccountDBAccess AccountsDB = new AccountDBAccess();

            AccountsDB.SaveCookie(AccountID, Role, Token);
        }

        public static bool VerifyToken(string token)
        {
            return ItemsDBAccess.VerifyToken(token);
        }

        public static int Role(string token)
        {
            return ItemsDBAccess.IdentifyAccountRoleByToken(token);
        }

        public static int AccountID(string token)
        {
            return ItemsDBAccess.IdentifyAccountIDByToken(token);
        }

        public static void Clear(int id)
        {
            AccountDBAccess AccountsDB = new AccountDBAccess();
            AccountsDB.ClearCookieData(id);
        }
    }
}

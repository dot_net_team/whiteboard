﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class SideMenuModel
    {
        public SideMenuModel()
        {
            SubMenuListName = new List<string>();
            SubMenuLink = new List<string>();
            TopName = "Dashboard";
            SubMenuName = " - ";
        }
        public string TopName { get; set; }
        public string SubMenuName { get; set; }
        public List<string> SubMenuListName { get; set; }
        public List<string> SubMenuLink { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Xamarin.Payments.Stripe;
using System.Configuration;

namespace BusinessLayer
{
    public interface ICourseCRUD
    {
        void Create();
        void Update();
        void Delete();
    }

    public class CourseModel : ICourseCRUD
    {
        protected CourseDBAccess CoursesDB;
        protected InstructorDBAccess InstructorDB;

        public CourseModel()
        {
            CoursesDB = new CourseDBAccess();
            InstructorDB = new InstructorDBAccess();
        }



        //BASIC INFO
        #region basic info
        public int CourseID { get; set; }
        public string ID { get; set; }
        public string InstructorID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageLocation { get; set; }
        public string InstructorName { get; set; }
        public string CoursePrice { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string Matchfound { get; set; }
        #endregion

        #region Files
        public int FileID { get; set; }
        public string FileLoc { get; set; }
        public string FileTitle { get; set; }

        public List<CourseModel> listFiles = new List<CourseModel>();

        public void ReadFileByID()
        {
            using (SqlDataReader Reader = CoursesDB.ReadFilesByID(this.FileID))
            {
                this.FillFileData(Reader);
            }
        }

        public void ReadFilesByLessonID()
        {
            using (SqlDataReader Reader = CoursesDB.ReadFilesByLessonID(this.LessonID))
            {
                if (Reader.HasRows)
                    this.FillFileData(Reader);
            }
        }

        public virtual void UploadFile()
        {
            CoursesDB.UploadFile(this.LessonID, this.FileLoc, this.FileTitle);
        }

        public virtual void DeleteFile()
        {
            CoursesDB.DeleteFile(this.FileID);
        }

        public virtual int GetLessonIDbyFileID()
        {
            using (SqlDataReader Reader = CoursesDB.GetLessonIDbyFileID(this.FileID))
            {
                LessonID = Convert.ToInt32(Reader[0]);
            }

            return LessonID;
        }

        protected virtual void FillFileData(SqlDataReader Reader)
        {
            if (Reader.HasRows == false)
            {
                //throw new AccountNotFoundException(); // don't use Accoutn not found exception here lol... haha --cleanup branch
                throw new Exception("Course not found Exception");
            }
            while (Reader.Read())
            {
                CourseModel FileModelobj = new CourseModel();
                FileModelobj.FileID = Convert.ToInt32(Reader["ID"]);
                FileModelobj.LessonID = Convert.ToInt32(Reader["LessonID"]);
                FileModelobj.FileLoc = Reader["FileLocation"].ToString();
                FileModelobj.FileTitle = Reader["FileTitle"].ToString();

                listFiles.Add(FileModelobj);
            }

            CoursesDB.CloseConnection();
        }

        #endregion

        #region Lesson
        public int LInstID { get; set; }
        public int LessonID { get; set; }
        public string LCourseID { get; set; }
        public string LTitle { get; set; }
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string LContent { get; set; }

        public virtual void ReadLessonByID(string ID)
        {
            using (SqlDataReader Reader = CoursesDB.ReadLessonByID(ID))
            {
                this.FillLessonData(Reader);
            }
        }

        public virtual void ReadLessonByCourseID(string CourseID)
        {
            using (SqlDataReader Reader = CoursesDB.ReadLessonByCourseID(CourseID))
            {
                if (Reader.HasRows)
                    this.FillLessonData(Reader);
            }
        }

        public List<CourseModel> listLesson = new List<CourseModel>();

        public void CreateLesson()
        {
            CoursesDB.AddLesson(Convert.ToInt32(this.LCourseID), this.LTitle, this.LContent);
        }

        public void UpdateLesson()
        {
            CoursesDB.UpdateLesson(this.LessonID, this.LTitle, this.LContent);
        }

        public void DeleteLesson()
        {
            CoursesDB.DeleteLesson(this.LessonID);
        }

        protected virtual void FillLessonData(SqlDataReader Reader)
        {
            if (Reader.HasRows == false)
            {
                //throw new AccountNotFoundException(); // don't use Accoutn not found exception here lol... haha --cleanup branch
                throw new Exception("Course not found Exception");
            }
            while (Reader.Read())
            {
                CourseModel LessonModelobj = new CourseModel();
                LessonModelobj.LessonID = Convert.ToInt32(Reader["ID"]);
                LessonModelobj.LCourseID = Reader["CourseID"].ToString();
                LessonModelobj.LTitle = Reader["Title"].ToString();
                LessonModelobj.LContent = Reader["TextContent"].ToString();
                LessonModelobj.LInstID = (spGetInstructorIDbyCourseID(Convert.ToInt32(LessonModelobj.LCourseID)));

                listLesson.Add(LessonModelobj);

            }

            CoursesDB.CloseConnection();
        }

        #endregion

        public virtual string GetInstructorIDbyAccID(string ID)
        {
            using (SqlDataReader Reader = CoursesDB.GetInstructorIDbyAccID(ID))
            {
                while (Reader.Read())
                {
                    InstructorID = Reader["ID"].ToString();
                }
                CoursesDB.CloseConnection();
                return InstructorID;
            }


        }

        public virtual void ReadCourseByID(string ID)
        {
            using (SqlDataReader Reader = CoursesDB.ReadCourseByID(ID))
            {
                this.FillData(Reader);
            }
        }

        public virtual int spGetInstructorIDbyCourseID(int CourseID)
        {
            using (SqlDataReader Reader = InstructorDB.spGetInstructorIDbyCourseID(CourseID))
            {
                while (Reader.Read())
                {
                    LInstID = Convert.ToInt32(Reader["InstructorID"].ToString());
                }
                return LInstID;
            }
        }

        public virtual string spGetInstructorName(int CourseID)
        {
            using (SqlDataReader Reader = InstructorDB.spGetInstructorName(CourseID))
            {
                while (Reader.Read())
                {
                    InstructorName = Reader["Fullname"].ToString();
                }
                return InstructorName;

            }

        }

         
        public virtual void ReadCourseSubscriptionByStudID(int Stud_ID, int CourseID)
        {
            using (SqlDataReader Reader = CoursesDB.ReadCourseSubscriptionByStudID(Stud_ID, CourseID))
            {
                /*  if(Stud_ID==Convert.ToInt32(Reader["StudentID"].ToString()))
                  {
                      Matchfound = Reader["ID"].ToString() + " " + Reader["CourseID"].ToString();
                  }*/
                while (Reader.Read())
                {
                    Matchfound = Reader["ID"].ToString();
                }
            }
            CoursesDB.CloseConnection();
        }

        public virtual void GetCourseSubscriptionsOfStudent(int Student_ID)
        {
            using (SqlDataReader Reader = CoursesDB.spGetCourseSubscriptionsOfStudent(Student_ID))
            {
                while(Reader.Read())
                {
                    //ReadCourseByID(Reader["CourseID"].ToString());
                    CourseModel CoursesObj = new CourseModel();
                    CoursesObj.CourseID = Convert.ToInt32(Reader["CourseID"].ToString());

                    MyCourses.Add(CoursesObj);
                }
            }
        }

        public virtual void spInsertIntoCourseSubscription(int Stud_ID,int CourseID)
        {
            StudentModel Stud = new StudentModel();
            
            Stud.ReadByID(spGetAccountIDFromStudentID(Stud_ID));
            bool paid = false;
            //
            // Stripe here before saving data to db
            //

            string API_KEY = ConfigurationManager.AppSettings["StripeKey"];
            StripePayment CurrentPayment = new StripePayment(API_KEY);
            StripeCreditCardInfo CardInfo = new StripeCreditCardInfo();

            //tmp card info
            CardInfo.ExpirationMonth = 12;
            CardInfo.ExpirationYear = 19;
            CardInfo.Number = "4111111111111111";
            CardInfo.CVC = "123";

            int AmountToPay = this.spGetCoursePrice(CourseID);


            try
            {
                StripeCharge CurrentCharge = CurrentPayment.Charge(AmountToPay, "usd", CardInfo, "Whiteboard::Student::Subscribed: " + Stud.LastName + ", " + Stud.FirstName);
                paid = true;
            }
            catch (StripeException SEx)
            {
                throw SEx;
            }
            if (paid)
            {
                CoursesDB.spInsertIntoCourseSubscription(Stud_ID, CourseID);
            }else
            {
                throw new Exception("CourseNotPaidException");
            }
        }

        public virtual void spGetFavoriteCourses(int Stud_ID)
        {
            using (SqlDataReader Reader = CoursesDB.spGetFavoriteCourses(Stud_ID))
            {
                this.FillData(Reader);        
            }
        }

        public virtual void CheckFavoritebyStudID(int Student_ID)
        {
            using (SqlDataReader Reader = CoursesDB.CheckFavoritebyStudID(Student_ID))
            {
                while(Reader.Read())
                {
                    CourseModel MyFavorites = new CourseModel();
                    MyFavorites.CategoryID = Convert.ToInt32(Reader["TagID"].ToString());

                    FavoriteList.Add(MyFavorites);
                }
            }
        }

        public virtual void RemoveFromFavourite(int Stud_ID,int Tag_ID)
        {
            CoursesDB.spRemoveFromFavourite(Stud_ID, Tag_ID);
        }

        public virtual void AddFavorite(int CategoryID,int Stud_ID)
        {
            CoursesDB.spAddFavorite(CategoryID,Stud_ID);
        }

        public virtual void GetCourseByCategory(int CategoryID)
        {
            using (SqlDataReader Reader = CoursesDB.spGetCourseByCategory(CategoryID))
            {
                this.FillData(Reader);
            }
        }

        public int spGetAccountIDFromStudentID(int studid)
        {
            AccountDBAccess Accounts = new AccountDBAccess();
            using (SqlDataReader Reader = Accounts.GetAccountIDFromStudID(studid))
            {
                if (Reader.HasRows)
                {
                    Reader.Read();
                    var tmp = Reader[0].ToString();
                    Accounts.CloseConnection();
                    return Convert.ToInt32(tmp);
                }
                Accounts.CloseConnection();
                return 0;
            }
        }

        public virtual int spGetCoursePrice(int CourseID)
        {
            using (SqlDataReader Reader = CoursesDB.spGetCoursePrice(CourseID))
            {
                if (Reader.HasRows)
                {
                    Reader.Read();
                    var tmp = Convert.ToInt32(Reader["CoursePrice"]) * 100; 
                    CoursesDB.CloseConnection();
                    return tmp;
                }
                else
                {
                    CoursesDB.CloseConnection();
                    throw new Exception("PricelessCourseException");
                }
            }
        }

        public virtual void Create()
        {
            CoursesDB.CreateCourse(Convert.ToInt32(this.InstructorID), this.Title, this.Description, this.ImageLocation, Convert.ToInt32(this.CategoryID), this.CoursePrice);
        }


        public virtual void Update()
        {
            CoursesDB.EditCourse(Convert.ToInt32(this.CourseID), this.Title, this.Description, this.ImageLocation, Convert.ToInt32(this.CategoryID), this.CoursePrice);
        }

        public virtual void Delete()
        {
            int[] lesson = new int[999];
            int i = 0;
            using (SqlDataReader Reader = CoursesDB.GetLessonIDbyCourseID(this.CourseID))
            {
                while (Reader.Read())
                {
                    lesson[i] = Convert.ToInt32(Reader["ID"]);
                    i++;
                }
            }
            CoursesDB.CloseConnection();
            CoursesDB.DeleteCourse(Convert.ToInt32(this.CourseID), lesson);
        }


        public void ReadAllCourse()
        {
            using (SqlDataReader Reader = CoursesDB.ReadAllCourse())
            {
                this.FillData(Reader);
            }
        }

        public void ReadAllCategories()
        {
            using (SqlDataReader Reader = CoursesDB.ReadAllCategories())
            {
                while(Reader.Read())
                {
                    CourseModel CategoryObj = new CourseModel();

                    CategoryObj.CategoryID = Convert.ToInt32(Reader["TagID"].ToString());
                    CategoryObj.CategoryName = Reader["CategoryName"].ToString();

                    CategoryList.Add(CategoryObj);
                }
                CoursesDB.CloseConnection();
            }
        }

        public void Create(string lessontitle, string description)
        {
            throw new NotImplementedException();
        }

        public virtual int spGetStudentById(int ID)
        {
            using (SqlDataReader Reader = CoursesDB.spGetStudentById(ID))
            {
                while (Reader.Read())
                {
                    ID = Convert.ToInt32(Reader["ID"].ToString());
                }
                CoursesDB.CloseConnection();
                return ID;
            }
        }

        public string GetInstructorIDbyCourseID(string ID)
        {
            using (SqlDataReader Reader = CoursesDB.GetInstructorIDbyCourseID(ID))
            {
                while (Reader.Read())
                {
                    InstructorID = Reader["InstructorID"].ToString();
                }
                CoursesDB.CloseConnection();
                return InstructorID;
            }
        }

        public void ReadCourseByInstructorID(string ID)
        {
            //using (SqlDataReader Reader = CoursesDB.ReadCourseByInstructorID(ID))
            using (SqlDataReader Reader = CoursesDB.ReadCourseByInstructorID(ID))
            {
                this.FillData(Reader);
            }
        }


        public List<CourseModel> list = new List<CourseModel>();
        public List<CourseModel> CategoryList = new List<CourseModel>();
        public List<CourseModel> FavoriteList = new List<CourseModel>();
        public List<CourseModel> MyCourses = new List<CourseModel>();
      
        protected virtual void FillData(SqlDataReader Reader)
        {
            if (Reader.HasRows == false)
            {
                //throw new AccountNotFoundException(); // don't use Accoutn not found exception here lol... haha --cleanup branch
                //throw new Exception("Course not found Exception");
            }
            while (Reader.Read())
            {
                CourseModel CourseModelobj = new CourseModel();
                //Courses
                CourseModelobj.CourseID = Convert.ToInt32(Reader["ID"]);
                CourseModelobj.Title = Reader["Title"].ToString();
                CourseModelobj.Description = Reader["Description"].ToString();
                CourseModelobj.InstructorID = Reader["InstructorID"].ToString();
                CourseModelobj.ImageLocation = Reader["ImageLocation"].ToString();
                CourseModelobj.InstructorName = (spGetInstructorName(Convert.ToInt32(Reader["ID"])));
                CourseModelobj.CoursePrice = Reader["CoursePrice"].ToString();

                list.Add(CourseModelobj);

            }
            CoursesDB.CloseConnection();
        }
    }
}

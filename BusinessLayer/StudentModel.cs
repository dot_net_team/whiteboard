﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{


    public class StudentModel : AccountModel
    {
        public int StudentID { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "School")]
        public string School { get; set; }

        public StudentModel() : base()
        {
            base.Role = (byte)2;
        }

        public override void Create()
        {
            base.AccountsDB.CreateStudentAccount(this.Email, this.Password, this.FirstName, this.LastName, this.School,this.Country);
        }

        //public void CreateStudent(string Email, string Password, string FirstName, string LastName, string School)
        //{
            
        //    ///
        //    base.Email = Email;
        //    base.Password = Password;
        //    base.FirstName = FirstName;
        //    base.LastName = LastName;
        //    ///
        //    this.School = School;
        //    ///
        //    this.Create();
        //}

        public override void Delete()
        {
            base.AccountsDB.EditAccountFlagInDB(this.AccountID, 3);
        }

        public override void Read(int AccountID)
        {
            base.Read(AccountID);
            using (SqlDataReader Reader = AccountsDB.ReadStudent(base.AccountID))
            {
                this.FillData(Reader);
            }
        }

        public override void Read(string Email)
        {
            base.Read(Email);
            using (SqlDataReader Reader = AccountsDB.ReadStudent(base.AccountID))
            {
                this.FillData(Reader);
            }
        }

        private new void FillData(SqlDataReader Reader)
        {
            if (Reader.HasRows == false)
            {
                throw new AccountNotFoundException();   
            }
            ///this
            Reader.Read();
            this.School = Reader["School"].ToString();
            this.StudentID = Convert.ToInt32(Reader["ID"]);
            base.AccountsDB.CloseConnection();
        }

        public override void Update()
        {
            base.AccountsDB.EditStudentAccount(this.AccountID,this.FirstName, this.LastName, this.School, this.Country);
        }

        public static SideMenuModel SideMenu(System.Web.Mvc.UrlHelper Url)
        {
            // System.Web.Mvc.UrlHelper Url = new System.Web.Mvc.UrlHelper();

            SideMenuModel Menu = new SideMenuModel();
            Menu.TopName = "Student DashBoard";
            Menu.SubMenuName = "Tools";

            Menu.SubMenuLink.Add(Url.Action("BrowseCourse", "Student"));
            Menu.SubMenuListName.Add("Browse Courses");

            Menu.SubMenuLink.Add(Url.Action("EditInfo", "Student"));
            Menu.SubMenuListName.Add("Edit");

            Menu.SubMenuLink.Add(Url.Action("DeleteAccount", "Student"));
            Menu.SubMenuListName.Add("Delete");

            Menu.SubMenuLink.Add(Url.Action("ChangePassword", "Student"));
            Menu.SubMenuListName.Add("Change Password");

            Menu.SubMenuLink.Add(Url.Action("Index", "Student"));
            Menu.SubMenuListName.Add("Back to List");

            return Menu;
        }
    }
}

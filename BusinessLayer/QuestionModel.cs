﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;


namespace BusinessLayer
{
    public interface IbankCrudd
    {
        void Create(int InstructorID, string Lesson, string Description);

        void add(int QuestionBankID, string QuestionType, string Question, string Answer, string ChoiceA, string ChoiceB, string ChoiceC, string ChoiceD);
        //void Update();
        //void Delete();
    }
    public class QuestionModel : IbankCrudd
    {
        protected CourseDBAccess CoursesDB;
        protected InstructorDBAccess InstructorDB;
        // protected string LessonTitle, Description = string.Empty;

        public QuestionModel()
        {

            CoursesDB = new CourseDBAccess();
            InstructorDB = new InstructorDBAccess();
        }


        #region info
        public int InstructorID { get; set; }
        public string LessonTitle { get; set; }
        public string Description { get; set; }

        public int QuestionBankID { get; set; }
        
        public string QuestionType { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string []choices = new string[5];
        #endregion


      
        public virtual void Create(int InstructorID, string Lesson, string Description)
        {

            InstructorDB.CreateQuestionBank(InstructorID, LessonTitle, Description);
        }

        public virtual void add(int QuestionBankID, string QuestionType, string Question, string Answer, string ChoiceA, string ChoiceB, string ChoiceC, string ChoiceD)
        {
            this.QuestionBankID = QuestionBankID;
          
            InstructorDB.AddQuestions(QuestionBankID, QuestionType, Question, Answer);
            if (QuestionType == "MultipleChoice")
            {
                int QID = 0;
                SqlDataReader Reader=InstructorDB.GetQuestionID(Question);
                while (Reader.Read())
                {
                 QID  = Convert.ToInt32(Reader["ID"]);
                }
                for (int i = 0; i <= 3; i++)
                {
                    InstructorDB.AddChoice(QID,ChoiceA,ChoiceB,ChoiceC,ChoiceD);
                }

            }

        }


      
        public virtual void ReadQuestionbank(int ID)
        {
           
            using (SqlDataReader Reader = InstructorDB.getQuestionBank(ID))
            {
                this.FillData(Reader);
            }
        }

        public List<QuestionModel> list = new List<QuestionModel>();
        protected virtual void FillData(SqlDataReader Reader)
        {

            if (Reader.HasRows == false)
            {
                //throw new AccountNotFoundException(); // don't use Accoutn not found exception here lol... haha --cleanup branch
                //throw new Exception("Course not found Exception");
            }
            while (Reader.Read())
            {
                QuestionModel QuestionModelobj = new QuestionModel();
                //Courses
                QuestionModelobj.QuestionBankID = Convert.ToInt32(Reader["ID"]);
                QuestionModelobj.InstructorID = Convert.ToInt32(Reader["InstructorID"]);
                QuestionModelobj.LessonTitle = Reader["LessonTitle"].ToString();
                QuestionModelobj.Description = Reader["Description"].ToString();


                list.Add(QuestionModelobj);

            }
        }




        public virtual void ReadQuestionInQB(int ID)
        {
          
            using (SqlDataReader Reader1 = InstructorDB.getQuestioninQB(ID))
            {
                this.datafill(Reader1);
            }
        }
        public List<QuestionModel> questionlist = new List<QuestionModel>();
        protected virtual void datafill(SqlDataReader Reader1)
        {

            if (Reader1.HasRows == false)
            {
                //throw new AccountNotFoundException(); // don't use Accoutn not found exception here lol... haha --cleanup branch
                //throw new Exception("Course not found Exception");
            }
            while (Reader1.Read())
            {
                QuestionModel obj = new QuestionModel();
                //Courses
                obj.QuestionBankID = Convert.ToInt32(Reader1["QuestionID"]);
                obj.QuestionType = Reader1["QuestionType"].ToString();
                obj.Question = Reader1["Question"].ToString();
                obj.Answer = Reader1["Answer"].ToString();


                questionlist.Add(obj);

            }




        }



    }
    

    
}

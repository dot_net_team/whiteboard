﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BusinessLayer
{
    public class StaticValues
    {
        public static List<System.Web.Mvc.SelectListItem> GetAllFontAwesomeStrings()
        {
            return DataAccessLayer.ConfigurationManagementAccess.GetListofFontAwesomeStringsInDB();
        }

        public static List<System.Web.Mvc.SelectListItem> GetAllTypeFace()
        {
            return DataAccessLayer.ConfigurationManagementAccess.GetListofTypeFacesInDB();
        }

        public static List<System.Web.Mvc.SelectListItem> GetAllBanners()
        {
            return DataAccessLayer.ConfigurationManagementAccess.GetBannersInDB();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BusinessLayer
{
    public interface IAuthenticator
    {
        /// <summary>
        /// should redirect users to an error 40* forbidden if they are not signed in
        /// with a proper privillage
        /// </summary>
        /// <returns>null if authenticated</returns>
        ActionResult IsAuthenticated();
    }
}

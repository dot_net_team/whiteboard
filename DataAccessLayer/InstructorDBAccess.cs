﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DataAccessLayer
{
    public class InstructorDBAccess
    {
        Database db;
  
        public InstructorDBAccess()
        {
            db = new Database();
        }

        public SqlDataReader spGetInstructorName(int CourseID)
        {   
            return db.ReadQuery(string.Format("spGetInstructorName {0}", CourseID));
        }

        public SqlDataReader spGetInstructorIDbyCourseID(int CourseID)
        {
            return db.ReadQuery(string.Format("spGetInstructorIDbyCourseID {0}", CourseID));
        }
        public SqlDataReader getQuestioninQB(int ID)
        {
            
            return db.ReadQuery(string.Format("spGetQuestionsbyID '{0}'", ID));
        }
        public void CreateQuestionBank(int InstructorID, string Description, string LessonTitle)
        {
            db.ExecuteProcedure(string.Format("spCreateQuestionBank '{0}', '{1}', '{2}'", InstructorID, LessonTitle, Description));
        }
        public void AddQuestions(int QuestionID, string QuestionType, string Question,string Answer)
        {
            db.ExecuteProcedure(string.Format("spAddQuestion '{0}', '{1}', '{2}', '{3}'",QuestionID, QuestionType, Question,Answer));
        }
        public SqlDataReader getQuestionBank(int ID)
        {
           return db.ReadQuery(string.Format("spGetQuestionBankbyID '{0}'", ID));
        }

        public void AddChoice(int QuestionID,string choiceA, string choiceB, string choiceC, string choiceD)
        {
            db.ExecuteProcedure(string.Format("spAddChoice '{0}', '{1}', '{2}', '{3}', '{4}'", QuestionID, choiceA, choiceB, choiceC, choiceD));

        }
        public void AddQuiz(string QuizTitle,int LessonID, int QuestionBankID, int items, int passing)
        {
            db.ExecuteProcedure(string.Format("spAddQuiz '{0}','{1}','{2}','{3}','{4}'",QuizTitle,LessonID, QuestionBankID, items, passing));
        }

        public SqlDataReader GetQuestionID(string Question)
        {
            return db.ReadQuery(string.Format("spGetQuestionID '{0}'", Question));
        }

        public SqlDataReader ReadLessons()
        {
            return db.ReadQuery(string.Format("spReadLessons"));
        }

    }
}

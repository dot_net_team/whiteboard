﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class PaymentManagementAccess
    {
        public static bool UpdateSubcriptionPayment(int id, int price)
        {
            Database db = new Database();
            return db.ExecuteProcedure(string.Format("spUpdateSubscriptionPrice {0},{1}", id, price));
        }
    }
}

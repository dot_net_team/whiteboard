﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace DataAccessLayer
{
    public class CourseDBAccess : BaseDBAccess
    {

        public CourseDBAccess() : base() { }
        public static List<System.Web.Mvc.SelectListItem> GetAllCategories()
        {
            return StaticListReader("spReadAllCategories", "CategoryName", "TagID");
        }


        public static List<System.Web.Mvc.SelectListItem> StaticListReader(string QueryString, string ColumnText, string ColumnValue)
        {
            Database db = new Database();
            List<System.Web.Mvc.SelectListItem> items = new List<System.Web.Mvc.SelectListItem>();
            using (SqlDataReader reader = db.ReadQuery(QueryString))
            {
                while (reader.Read())
                {
                    string ct = reader[ColumnText].ToString();
                    string cv = reader[ColumnValue].ToString();
                    items.Add(new System.Web.Mvc.SelectListItem
                    {
                        Text = ct,
                        Value = cv
                    });
                }
            }
            db.CloseConnection();
            db = null;
            return items;
        }

        public SqlDataReader GetInstructorIDbyAccID(string AccID)
        {
            return db.ReadQuery(string.Format("spGetInstructorIDbyAccID {0}", AccID));
        }

        public SqlDataReader ReadCourseByID(string CourseID)
        {

            return db.ReadQuery(string.Format("spGetCourseByID {0}", CourseID));
        }

        public SqlDataReader ReadAllCourse()
        {
            return db.ReadQuery(string.Format("spGetCourse"));
        }

        public SqlDataReader ReadCourseSubscriptionByStudID(int Stud_ID, int COURSE_ID)
        {
            return db.ReadQuery(string.Format("spGetCourseSubscriptionByStudID {0},{1}", Stud_ID, COURSE_ID));
        }

        public SqlDataReader ReadCourseByInstructorID(string ID)
        {
            return db.ReadQuery(string.Format("spGetCourseByInstructorID {0}", ID));
        }

        public void CreateCourse(int InstructorID, string Title, string Desc, string ImgLoc, int CategoryID, string CoursePrice)
        {
            db.ExecuteProcedure(string.Format("spCreateCourse '{0}', '{1}', '{2}', '{3}', {4}, {5}", InstructorID, Title, Desc, ImgLoc, CategoryID, Convert.ToDecimal(CoursePrice)));
        }

        public void EditCourse(int CourseID, string title, string Desc, string ImgLoc, int CategoryID, string CoursePrice)
        {
            db.ExecuteProcedure(string.Format("spUpdateCourse {0}, '{1}', '{2}', '{3}', {4}, {5}", CourseID, title, Desc, ImgLoc, CategoryID, CoursePrice));
        }

        public SqlDataReader GetLessonIDbyCourseID(int CourseID)
        {
            return db.ReadQuery(string.Format("spGetLessonIDbyCourseID {0}", CourseID));
        }

        public void DeleteCourse(int CourseID, int[] lesson)
        {
            int i = 0;
            while (lesson[i] != 0)
            {
                db.ExecuteProcedure(string.Format("spDeleteFileByLessonID {0}", lesson[i]));
                i++;
            }
            db.ExecuteProcedure(string.Format("spDeleteCourse {0}", CourseID));
        }

        public void spInsertIntoCourseSubscription(int Stud_ID, int COURSE_ID)
        {
            db.ExecuteProcedure(string.Format("spInsertIntoCourseSubscription {0},{1}", Stud_ID, COURSE_ID));
        }

        public SqlDataReader GetInstructorIDbyCourseID(string ID)
        {
            return db.ReadQuery(string.Format("spGetInstructorIDbyCourseID {0}", ID));
        }


        public SqlDataReader spGetStudentById(int ID)
        {
            return db.ReadQuery(string.Format("spGetStudentById {0}", ID));
        }

        public SqlDataReader spGetFavoriteCourses(int Stud_ID)
        {
            return db.ReadQuery(string.Format("spGetFavoriteCourses {0}", Stud_ID));
        }

        public SqlDataReader spGetCoursePrice(int CourseID)
        {
            return db.ReadQuery(string.Format("spGetCoursePriceByCourseID {0}", CourseID));
        }

        //LESSONS DB ACCESS

        public SqlDataReader ReadLessonByID(string LessonID)
        {
            return db.ReadQuery(string.Format("spGetLessonByID {0}", LessonID));
        }

        public SqlDataReader ReadLessonByCourseID(string CourseID)
        {
            return db.ReadQuery(string.Format("spGetLessonByCourseID {0}", CourseID));
        }

        public SqlDataReader AddLesson(int CourseID, string Title, string Content)
        {
            return db.ReadQuery(string.Format("spCreateLesson {0}, '{1}', '{2}'", CourseID, Title, Content));
        }

        public SqlDataReader UpdateLesson(int ID, string Title, string Content)
        {
            return db.ReadQuery(string.Format("spUpdateLesson {0}, '{1}', '{2}'", ID, Title, Content));
        }

        public void DeleteLesson(int ID)
        {
            db.ExecuteProcedure(string.Format("spDeleteFileByLessonID {0}", ID));
            db.ReadQuery(string.Format("spDeleteLesson {0}", ID));
        }

        public SqlDataReader ReadAllCategories()
        {
            return db.ReadQuery(string.Format("spReadAllCategories"));
        }

        public void UploadFile(int lessonid, string fileloc, string title)
        {
            db.ReadQuery(string.Format("spAddFile {0}, '{1}', '{2}'", lessonid, fileloc, title));
        }

        public SqlDataReader ReadFilesByLessonID(int lessonid)
        {
            return db.ReadQuery(string.Format("spReadFilesByLessonID {0}", lessonid));
        }

        public SqlDataReader ReadFilesByID(int fileid)
        {
            return db.ReadQuery(string.Format("spReadFilesByID {0}", fileid));
        }
        public SqlDataReader GetLessonIDbyFileID(int fileid)
        {
            return db.ReadQuery(string.Format("spGetLessonIDbyFileID '{0}'", fileid));
        }

        public void DeleteFile(int fileid)
        {
            db.ReadQuery(string.Format("spDeleteFile {0}", fileid));
        }


        public SqlDataReader GetProgressforInstructor(int ID)
        {
            return db.ReadQuery(string.Format("spGetProgressforInstructor {0}", ID));
        }

        public SqlDataReader CheckFavoritebyStudID(int Student_ID)
        {
            return db.ReadQuery(string.Format("spCheckFavoritebyStudentID  {0}",Student_ID));
        }

        public SqlDataReader spGetCourseByCategory(int Category_ID)
        {
            return db.ReadQuery(string.Format("spGetCourseByCategory {0}", Category_ID));
        }

        public SqlDataReader spGetCourseSubscriptionsOfStudent(int Student_ID)
        {
            return db.ReadQuery(string.Format("spGetCourseSubscriptionsOfStudent {0}", Student_ID));
        }
        public void spRemoveFromFavourite(int Student_ID,int TAG_ID)
        {
            db.ExecuteProcedure(string.Format("spRemoveFromFavourite {0},{1}", Student_ID, TAG_ID));
        }

        public void spAddFavorite(int TAG_ID,int Student_ID)
        {
            db.ExecuteProcedure(string.Format("spAddToFavourite {0},{1}", TAG_ID,Student_ID));
        }
    }
}
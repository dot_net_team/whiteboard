﻿ using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class AuditDBAccess : BaseDBAccess
    {
        public AuditDBAccess() : base()
        {
          
        }

        public SqlDataReader ReadLogs(int Offset,int Next)
        {
            return db.ReadQuery(string.Format("spReadLog {0},{1}",Offset,Next));
        }
    }
}

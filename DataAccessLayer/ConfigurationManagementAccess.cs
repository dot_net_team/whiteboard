﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class ConfigurationManagementAccess
    {
        /// <summary>
        /// Pass Only Reflection based title, this is automatic
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public static string GetString(string title)
        {
            title = title.Remove(0, 4);
            Database db = new Database();
            using (SqlDataReader reader = db.ReadQuery(string.Format("spGetConfString '{0}'", title)))
            {
                while (reader.Read())
                {
                    string tmp = reader["Data"].ToString();
                    db.CloseConnection();
                    return tmp;
                }
            }
     
            return null;
        }


        public static string GetColorSafe(string title)
        {
            title = title.Remove(0, 4);
            Database db = new Database();
            using (SqlDataReader reader = db.ReadQuery(string.Format("spGetConfString '{0}'", title)))
            {
                string color = "NULL";
                while (reader.Read())
                {
                    color = reader["Data"].ToString();
                }
                db.CloseConnection();

                if (color == "NULL")
                {
                    color = "black";
                }

                return color; 
            }
        }

        public static string GetFontSafe(string title)
        {
            title = title.Remove(0, 4);
            Database db = new Database();
            using (SqlDataReader reader = db.ReadQuery(string.Format("spGetConfString '{0}'", title)))
            {
                string Font = "NULL";
                while (reader.Read())
                {
                    Font = reader["Data"].ToString();
                }
                db.CloseConnection();

                if (Font == "NULL")
                {
                    Font = "Tahoma";
                }
                return Font; 
            }
        }

        //public static string GetFont(string title)
        //{
        //    title = title.Remove(0, 4);
        //    Database db = new Database();
        //    using (SqlDataReader reader = db.ReadQuery(string.Format("spGetConfString '{0}'", title)))
        //    {
        //        while (reader.Read())
        //        {
        //            return reader["Data"].ToString();
        //        }
        //    }
        //    return null;
        //}

        public static string GetFASafeString(string title)
        {
            //title = title.Remove(0, 4);
            //Database db = new Database();
            //string[] FA = new string[2];
            //using (SqlDataReader reader = db.ReadQuery(string.Format("spGetFaString '{0}'", title)))
            //{
            //    while (reader.Read())
            //    {
            //        if (reader["Title"].ToString().Contains("fa-"))
            //        {
            //            FA = new string[2];
            //            FA[0] = reader["Title"].ToString();
            //            FA[1] = reader["Data"].ToString();
            //            return FA;
            //        }
            //    }
            //}
            //FA = new string[2];
            //FA[0] = "fa-ban";
            //FA[1] = "&#xf05e";
            //return FA;
            string FA = GetString(title);
            if (FA == "NULL")
            {
                return "fa-ban";
            }
            else
            {
                return FA;
            }



        }

        public static List<System.Web.Mvc.SelectListItem> GetListofFontAwesomeStringsInDB()
        {
            return ItemsDBAccess.StaticListReader("spGetAllFontAwesome", "Title"); // returns class eg fa-ban
           // return ItemsDBAccess.StaticListReader("spGetAllFontAwesome","Data", "Title"); // returns html escape eg &weq
        }

        public static List<System.Web.Mvc.SelectListItem> GetBannersInDB()
        {
            return ItemsDBAccess.StaticListReader("spGetAllBanners", "Data","Data"); // returns class eg fa-ban
                                                                                   // return ItemsDBAccess.StaticListReader("spGetAllFontAwesome","Data", "Title"); // returns html escape eg &weq
        }

        public static List<System.Web.Mvc.SelectListItem> GetListofTypeFacesInDB()
        {
            //return ItemsDBAccess.StaticListReader("spGetAllFontAwesome", "Title");
            return ItemsDBAccess.StaticListReader("spGetAllTypeFace", "Data");
        }

        public static bool EditString(string title,int editor_id,string data)
        {
            if(editor_id == -1) { return false; }
            title = title.Remove(0, 4);
            Database db = new Database();
            return db.ExecuteProcedure(string.Format("spEditConfString {0},'{1}','{2}'", editor_id, title, data));

        }

        private class NoRowException : Exception { }

    }
}

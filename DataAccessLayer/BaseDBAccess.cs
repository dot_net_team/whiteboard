﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class BaseDBAccess
    {
        protected Database db;

        public BaseDBAccess()
        {
            db = new Database();
        }

        public void CloseConnection()
        {
            db.CloseConnection();
        }
    }
}

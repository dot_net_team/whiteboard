﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class UpdateDBAccess
    {
        public static bool UpdateLastLogin(int id)
        {
            Database db = new Database();
            return db.ExecuteProcedure(string.Format("spUpdateLastLogin {0}", id));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class ItemsDBAccess
    {
        public static List<System.Web.Mvc.SelectListItem> GetCountryList()
        {
            return StaticListReader("spGetCountryList", "CountryName");
        }

        public static List<System.Web.Mvc.SelectListItem> GetNameTitleList()
        {
            return StaticListReader("spGetNameTitle", "Title");
        }

        public static List<System.Web.Mvc.SelectListItem> GetSubscriptionPlanList()
        {
            return StaticListReader("spGetSubscriptionPlanList", "SubscriptionPlanName", "ID");
        }

        public static List<System.Web.Mvc.SelectListItem> GetPaymentModeList()
        {
            return StaticListReader("spPaymentModeList", "PaymentModePlanName", "ID");
        }

       public static List<System.Web.Mvc.SelectListItem> StaticListReader(string QueryString, string ColumnText, string ColumnValue)
        {
            Database db = new Database();
            List<System.Web.Mvc.SelectListItem> items = new List<System.Web.Mvc.SelectListItem>();
            using (SqlDataReader reader = db.ReadQuery(QueryString))
            {
                while (reader.Read())
                {
                    string ct = reader[ColumnText].ToString();
                    string cv = reader[ColumnValue].ToString();
                    items.Add(new System.Web.Mvc.SelectListItem
                    {
                        Text = ct,
                        Value = cv
                    });
                }
            }
            db.CloseConnection();
            db = null;
            return items;
        }

        public static int GetSubscriptionPriceById(int id)
        {
            Database db = new Database();
            using (SqlDataReader reader = db.ReadQuery(string.Format("spGetSubscriptionPrice {0}", id)))
            {
                while (reader.Read())
                {
                    return Convert.ToInt32(reader["SubscriptionPrice"]);
                }
            }
            db.CloseConnection();
            return 0;
        }

        public static string ConvertListTypeToString(ConvertableListType c, int id)
        {
            switch (c)
            {
                case ConvertableListType.PaymentMode:
                    return GetString("SubscriptionPlanList", id);
                case ConvertableListType.SubcriptionPlan:
                    return GetString("PaymentModeList", id);
                case ConvertableListType.Roles:
                    return GetString("[Roles]", id, "RoleId");
                default:
                    return null;
            }
        }

        private static string GetString(string Column, int id)
        {
            return GetString(Column, id, "ID");
        }
        private static string GetString(string Column, int id, string col2)
        {
            Database db = new Database();
            using (SqlDataReader reader = db.ReadQuery(string.Format("SELECT * FROM {0} WHERE {2}={1}", Column, id, col2)))
            {
                while (reader.Read())
                {
                    return reader[1].ToString();
                }
            }
            db.CloseConnection();
            return null;
        }

        public static bool VerifyToken(string token)
        {
            if (token == null) token = "0";
            Database db = new Database();
            bool result;
            using (SqlDataReader reader = db.ReadQuery(string.Format("spGetTokenInfo '{0}'", token)))
            {
                result = reader.HasRows;
            }
            db.CloseConnection();
            return result;
        }

        public static int IdentifyAccountIDByToken(string token)
        {
            if (token == null) token = "0";
            Database db = new Database();
            using (SqlDataReader reader = db.ReadQuery(string.Format("spGetTokenInfo '{0}'", token)))
            {
                while (reader.Read())
                {
                    return Convert.ToInt32(reader["AccountID"]);
                }
            }
            db.CloseConnection();
            return 0;
        }

        public static int IdentifyAccountRoleByToken(string token)
        {
            //if (token == null) token = "0";
            if (token == null) token = "-1";
            Database db = new Database();
            using (SqlDataReader reader = db.ReadQuery(string.Format("spGetTokenInfo '{0}'", token)))
            {
                if(reader.HasRows == false)
                {
                    db.CloseConnection();
                    return -1;
                }
                while (reader.Read())
                {
                    int tmp = Convert.ToInt32(reader["AccountRole"]);
                    db.CloseConnection();
                    return tmp;
                }
            }
            db.CloseConnection();
            return -1;
        }

        public static string IdentifyRoleByAccountID(int id)
        {
            //spGetAccountRoleByID
            Database db = new Database();
            using (SqlDataReader reader = db.ReadQuery(string.Format("spGetAccountRoleByID {0}", id)))
            {
                while (reader.Read())
                {
                    string tmp = reader[0].ToString();
                    db.CloseConnection();
                    return tmp;
                }
            }
            db.CloseConnection();
            return null;
        }

        public enum ConvertableListType
        {
            PaymentMode, SubcriptionPlan, Roles
        }

        public static List<System.Web.Mvc.SelectListItem> StaticListReader(string QueryString, string Column)
        {
            return StaticListReader(QueryString, Column, Column);
        }
    }
}

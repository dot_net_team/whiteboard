﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class AccountDBAccess : BaseDBAccess
    {
        public AccountDBAccess() : base()
        {
            
        }
        /// <summary>
        /// Data passed here should be safe and validated already
        /// </summary>
        public void CreateAdminAccount(string Email, string Password, string FirstName, string LastName)
        {
            db.ExecuteProcedure(string.Format("EXECUTE spRegisterAccount '{0}','{1}','ADMIN','{2}','{3}',NULL"
                , Email, Password, FirstName, LastName));
        }

        /// <summary>
        /// Data passed here should be safe and validated already
        /// </summary>
        public void CreateStudentAccount(string Email, string Password, string FirstName, string LastName, string School,string country)
        {
            db.ExecuteProcedure(string.Format("spRegisterStudent '{0}','{1}','{2}','{3}','{4}','{5}'"
                , Email, Password, FirstName, LastName, School,country));
        }

        public SqlDataReader GetAllAccountsFromDB()
        {
            return db.ReadQuery(string.Format("spGetAllAccountID"));
        }

        /// <summary>
        /// Data passed here should be safe and validated already
        /// </summary>
        public void SaveCookie(int AccountID, int Role,string Token)
        {
            db.ExecuteProcedure(string.Format("spSaveCookie '{0}',{1},{2}"
                , Token, AccountID, Role));
        }

        /// <summary>
        /// Data passed here should be safe and validated already
        /// </summary>
        public void CreateInstructorAccount(string Email, string Password, string FirstName, string LastName, string Title, string SubjectArea, string Organization,int PaymentMode,int Plan,string country)
        {
            DateTime expiration = DateTime.UtcNow;
            switch (PaymentMode)
            {
                case 1:
                    expiration.AddDays(30);
                    break;
                case 2:
                    expiration.AddDays(365);
                    break;
                default:
                    throw new Exception("Payment unknown exception"); // create custom exception for this one --noli
            }

            db.ExecuteProcedure(string.Format("spRegisterInstructor '{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7},{8},'{9}','{10}'"
                , Email, Password, FirstName, LastName, Title, SubjectArea, Organization, PaymentMode, Plan,expiration,country));
        }

        public SqlDataReader ReadAccountByEmail(string Email)
        {
            return db.ReadQuery(string.Format("spGetAccountByEmail '{0}'", Email));
        }

        public SqlDataReader ReadAccountByID(int AccountID)
        {
            return db.ReadQuery(string.Format("spGetAccountByID {0}", AccountID));
        }

        public SqlDataReader ReadStudent(int AccountID)
        {
            return db.ReadQuery(string.Format("spGetStudentById {0}", AccountID));
        }

        public SqlDataReader ReadInstructor(int AccountID)
        {
            return db.ReadQuery(string.Format("spGetInstructorById {0}", AccountID));
        }

        public SqlDataReader GetAccountIDFromStudID(int ID)
        {
            return db.ReadQuery(string.Format("spGetAccountIDFromStudID {0}", ID));
        }

        public SqlDataReader ReadRole(string Email)
        {
            return db.ReadQuery(string.Format("spGetAccountRoleByEmail '{0}'", Email));
        }

        public void ClearCookieData(int id)
        {
            db.ExecuteProcedure(string.Format("spDeleteUserCookie {0}", id));
        }

        //spEditAccountFlag id, flag
        public void EditAccountFlagInDB(int id, int flag)
        {
            db.ExecuteProcedure(string.Format("spEditAccountFlag {0},{1}", id
                , flag));
        }


        public void EditStudentAccount(int id,string FirstName, string LastName, string School, string country)
        {
            db.ExecuteProcedure(string.Format("spEditStudent {0},'{1}','{2}','{3}','{4}'", id
                , FirstName, LastName, School, country));
        }

        public void EditAccountPassword(int accountID, string password)
        {
            db.ExecuteProcedure(string.Format("spEditAccountPassword {0},'{1}'", accountID,
                password));
        }

        public void EditInstructorAccount(int accountID, string firstName, string lastName, string organization, string country)
        {
            db.ExecuteProcedure(string.Format("spEditInstructor {0},'{1}','{2}','{3}','{4}'", accountID
                , firstName, lastName, organization, country));
        }
    }

    
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public static class DatabaseConnectionString
    {
        //public static string Default { get { return System.Configuration.ConfigurationManager.ConnectionStrings["whiteboardDBv2"].ConnectionString.ToString(); } }
        public static string Default { get { return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString.ToString(); } }
    }
    public class Database
    {
        string ConnectionString;
        SqlConnection DatabaseConnection;

        //public Database(string ConnectionString)
        //{
        //    this.ConnectionString = ConnectionString;
        //    DatabaseConnection = new SqlConnection(ConnectionString);
        //}

        public Database()
        {
            this.ConnectionString = DatabaseConnectionString.Default;
            DatabaseConnection = new SqlConnection(ConnectionString);
        }


        public bool ExecuteProcedure(string Procedure)
        {
            try
            {
                OpenConnection();
                SqlCommand Command = new SqlCommand(Procedure, DatabaseConnection);
                Command.ExecuteNonQuery();
                CloseConnection();
                return true;
            }
            catch(SqlException se)
            {
                CloseConnection();
                throw se;
            }
        }

        public SqlDataReader ReadQuery(string Procedure)
        {
            OpenConnection();
            SqlDataReader HandledReader = new SqlCommand(Procedure, DatabaseConnection).ExecuteReader();
 
            return HandledReader;
        }

       

        private void OpenConnection()
        {
            CloseConnection();
            DatabaseConnection.Open();
        }

        public void CloseConnection()
        {
            try
            {
                DatabaseConnection.Close();
            }
            catch(InvalidOperationException IOE)
            {
                var m = IOE.Message;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLayer;
using WebHelper;
using System.ComponentModel.DataAnnotations;

namespace WhiteBoard.Controllers
{
    public class AdminController : Controller, IAuthenticator
    {
        public ActionResult IsAuthenticated()
        {
            //should be logged in with cookie, and role of 0 which is admin
            if (!(TokenModel.AccountID(WebSession.COOKIE_Token) > 0 && TokenModel.Role(WebSession.COOKIE_Token) == 0))
            {
                return RedirectToAction("Forbidden", "Error", null);
            }
            else return null;
        }

        // GET: Admin
        public ActionResult Index(AdminModel model)
        {
            model.Read(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));
            return IsAuthenticated() ?? View(model);
        }

        public ActionResult StringsManagement_HomePage(ConfigurationStrings_HomeModel model)
        {
            //int editor = WebHelper.WebSession.AccountID;
            int editor = TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token);
            if (editor <= 0)
            {
                editor = -1;
            }
            model.CurrentEditor = editor;
            ViewBag.Updated = (ModelState.IsValid);
            if (ModelState.IsValid && Request.HttpMethod == "POST")
            {
                return RedirectToAction("Index");
            }
            else { }

            var FA = BusinessLayer.StaticValues.GetAllFontAwesomeStrings();

            ViewBag.FontAwesomeList = FA;
            return IsAuthenticated() ?? View(model);
        }

        public ActionResult ColorsManagement_HomePage(ConfigurationColorModel_Home model)
        {
            //int editor = WebHelper.WebSession.AccountID;
            int editor = TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token);
            if (editor <= 0)
            {
                editor = -1;
            }
            model.CurrentEditor = editor;
            ViewBag.Updated = (ModelState.IsValid);
            if (ModelState.IsValid && Request.HttpMethod == "POST")
            {
                return RedirectToAction("Index");
            }
            else { }

            return IsAuthenticated() ?? View(model);
        }

        public ActionResult StringsManagement_Banner(COnfigurationStrings_Banner model)
        {
            //int editor = WebHelper.WebSession.AccountID;
            int editor = TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token);
            if (editor <= 0)
            {
                editor = -1;
            }
            model.CurrentEditor = editor;
            ViewBag.Updated = (ModelState.IsValid);
            if (ModelState.IsValid && Request.HttpMethod == "POST")
            {
                return RedirectToAction("Index");
            }
            else { }

            return IsAuthenticated() ?? View(model);
        }

        public ActionResult Logs()
        {
            return IsAuthenticated() ?? View();
        }

        public ActionResult ViewAllUsers()
        {
            AdminModel CurrentAdmin = new AdminModel();
            CurrentAdmin.Read(TokenModel.AccountID(WebSession.COOKIE_Token));

            List<AccountModel> model = CurrentAdmin.GetAllAccounts();
            

            return IsAuthenticated() ?? View(model);
        }

        public ActionResult BanUser(string AccountID)
        {
            Session["PassedID"] = Convert.ToInt32(AccountID);
            AccountModel model = new AccountModel();
            model.ReadByID(Convert.ToInt32(AccountID));
            //ViewBag.Passed2 = Convert.ToInt32(this.Request.QueryString["id"]);

            return IsAuthenticated() ?? View(model);
        }

        [HttpPost]
        public ActionResult BanUser()
        {
            return IsAuthenticated() ?? this.SetAccountFlag(Convert.ToInt32(Session["PassedID"]), 2,
                string.Format("Successfully banned -> {0}", Convert.ToInt32(Session["PassedID"])));
            //on successfull ban message
            //ViewBag.PassedId = Convert.ToInt32(id);
            //ViewBag.Passed2 = Convert.ToInt32(this.Request.QueryString["id"]);
            //if (Convert.ToInt32(Session["PassedID"]) == 0)
            //{
            //    Session["ShowDanger"] = "Something went wrong! (id error)";
            //    return RedirectToAction("ViewAllUsers");
            //}else
            //{
            //    AdminModel CurrentAdmin = new AdminModel();
            //    CurrentAdmin.Read(TokenModel.AccountID(WebSession.COOKIE_Token));

            //    try
            //    {
            //        CurrentAdmin.EditAccountFlag(Convert.ToInt32(Session["PassedID"]), 2);
            //        Session["ShowSuccess"] = "Sucessfully banned! " + Convert.ToInt32(Session["PassedID"]);                 
            //    }
            //    catch (Exception e)
            //    {
            //        Session["ShowDanger"] = "Something went wrong! -> " + e.Message;
            //        return RedirectToAction("ViewAllUsers");
            //    }
            //}

            //return IsAuthenticated() ?? RedirectToAction("ViewAllUsers");
        }

        public ActionResult Activate(string AccountID)
        {
            Session["PassedID"] = Convert.ToInt32(AccountID);
            AccountModel model = new AccountModel();
            model.ReadByID(Convert.ToInt32(AccountID));
            //ViewBag.Passed2 = Convert.ToInt32(this.Request.QueryString["id"]);

            return IsAuthenticated() ?? View(model);
        }

        [HttpPost]
        public ActionResult Activate()
        {
            return IsAuthenticated() ?? this.SetAccountFlag(Convert.ToInt32(Session["PassedID"]), 0,
                string.Format("Successfully Activated -> {0}", Convert.ToInt32(Session["PassedID"]))
                );
            //on successfull ban message
            //ViewBag.PassedId = Convert.ToInt32(id);
            //ViewBag.Passed2 = Convert.ToInt32(this.Request.QueryString["id"]);
            //if (Convert.ToInt32(Session["PassedID"]) == 0)
            //{
            //    Session["ShowDanger"] = "Something went wrong! (id error)";
            //    return RedirectToAction("ViewAllUsers");
            //}
            //else
            //{
            //    AdminModel CurrentAdmin = new AdminModel();
            //    CurrentAdmin.Read(TokenModel.AccountID(WebSession.COOKIE_Token));

            //    try
            //    {
            //        CurrentAdmin.EditAccountFlag(Convert.ToInt32(Session["PassedID"]), 0);
            //        Session["ShowSuccess"] = "Sucessfully Activated! " + Convert.ToInt32(Session["PassedID"]);
                    
            //    }
            //    catch (Exception e)
            //    {
            //        Session["ShowDanger"] = "Something went wrong! ->" + e.Message;
            //        return RedirectToAction("ViewAllUsers");
            //    }
            //}

            //return IsAuthenticated() ?? RedirectToAction("ViewAllUsers");
        }

        public ActionResult ColorChanger()
        {
            return IsAuthenticated() ?? View();
        }

        public ActionResult StringChanger()
        {
            return IsAuthenticated() ?? View();
        }

        private ActionResult SetAccountFlag(int id,int flag,string SuccessMessage)
        {
            if (id == 0)
            {
                Session["ShowDanger"] = "Something went wrong! (id error)";
                return IsAuthenticated() ?? RedirectToAction("ViewAllUsers");
            }
            else
            {
                AdminModel CurrentAdmin = new AdminModel();
                CurrentAdmin.Read(TokenModel.AccountID(WebSession.COOKIE_Token));

                try
                {
                    CurrentAdmin.EditAccountFlag(id, flag);
                    Session["ShowSuccess"] = SuccessMessage;
                }
                catch (Exception e)
                {
                    Session["ShowDanger"] = "Something went wrong! -> " + e.Message;
                    return IsAuthenticated() ?? RedirectToAction("ViewAllUsers");
                }
            }

            return IsAuthenticated() ?? RedirectToAction("ViewAllUsers");
        }
    }
}
﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WhiteBoard.Controllers
{
    public class StudentController : Controller, IAuthenticator
    {
        public ActionResult IsAuthenticated()
        {
            //cookie role 2 is student
            if (!(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token) > 0 && TokenModel.Role(WebHelper.WebSession.COOKIE_Token) == 2))
            {
                return (ActionResult)RedirectToAction("Forbidden", "Error", null);
            }
            else return null;
        }

        // GET: Student
        public ActionResult Index(StudentModel model)
        {
            try
            {
                model.Read(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));

                if (model.Flag != 0)
                {
                    Session["AccountFlagged"] = model.Flag.ToString();
                    return RedirectToAction("Flagged", "Login", null);
                }

                return IsAuthenticated() ?? View(model);
            }
            catch (AccountNotFoundException)
            {
                return RedirectToAction("Forbidden", "Error");
            }
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            if (TokenModel.VerifyToken(WebHelper.WebSession.COOKIE_Token))
                return RedirectToAction("Index", "Student", null);
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(StudentModel model)
        {
            if (!ModelState.IsValid)
            {
                model.DestroyCardInfo();
                return View(model);
            }
            else
            {
                model.Password = WebHelper.PasswordSalter.GetSalted(model.Password);
                model.Create();
                //do something on model payment here before desturction
                model.DestroyCardInfo();
                return RedirectToAction("Index", "Home"); //successful registration
            }
        }

        public ActionResult BrowseCourse(CourseModel model)
        {
            if (Request.QueryString["CategoryID"] != null)
            {
                int CategoryID = Convert.ToInt32(Request.QueryString["CategoryID"].ToString());
                model.GetCourseByCategory(CategoryID);
            }
            else
            {
                model.ReadAllCourse();
            }
            model.ReadAllCategories();
            return IsAuthenticated() ?? View(model);
        }

        public ActionResult FavouriteCourses(CourseModel model)
        {
            int StudID = BusinessLayer.TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token);
            model.spGetFavoriteCourses(StudID);
            model.ReadAllCategories();
            return View("BrowseCourse",model);
        }

        public ActionResult Courses(CourseModel model)
        {
            int StudID = BusinessLayer.TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token);
            model.GetCourseSubscriptionsOfStudent(StudID);
            foreach(var get in model.MyCourses)
            {
                model.ReadCourseByID(get.CourseID.ToString());
            }
            model.ReadAllCategories();
            return View("BrowseCourse", model);
        }

        public ActionResult DeleteFavorite(CourseModel model)
        {
            int StudID = BusinessLayer.TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token);
            int TagID = Convert.ToInt32(Request.QueryString["TagID"].ToString());
            model.RemoveFromFavourite(StudID, TagID);
            return Redirect("/Student/EditFavourites");
        }

        public ActionResult AddFavorite(CourseModel model)
        {
            int StudID = BusinessLayer.TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token);
            int TagID = Convert.ToInt32(Request.QueryString["TagID"].ToString());
            int ID = model.spGetStudentById(StudID);
            model.AddFavorite(TagID,ID);
            return Redirect("/Student/EditFavourites");
        }

        public ActionResult EditFavourites(CourseModel model)
        {
            int StudID = BusinessLayer.TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token);
            model.spGetFavoriteCourses(StudID);
            model.CheckFavoritebyStudID(StudID);
            model.ReadAllCategories();
            return View(model);
        }

        public ActionResult Enroll(CourseModel model)
        {
            string COURSEID = Request.QueryString["CourseID"].ToString();
            model.ReadCourseByID(COURSEID);
            //   model.ReadCourseSubscriptionByStudID(WebHelper.WebSession.AccountID,Convert.ToInt32(COURSEID));
            model.ReadCourseSubscriptionByStudID(BusinessLayer.TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token), Convert.ToInt32(COURSEID));
            return IsAuthenticated() ?? View(model);
        }

        public ActionResult EnrollStudent(CourseModel model)
        {
            //string StudID = Request.QueryString["StudID"].ToString();
            int StudID = BusinessLayer.TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token);
            string CourseID = Request.QueryString["CourseID"].ToString();
            int ID;
            //  string Today = Request.QueryString["today"].ToString(); 
            string DateToday = (DateTime.Now).ToString();
            ID = model.spGetStudentById(Convert.ToInt32(StudID));

            model.spInsertIntoCourseSubscription(ID, Convert.ToInt32(CourseID));

            return IsAuthenticated() ?? Redirect("/Student/Enroll?CourseID=" + CourseID);
        }

        public ActionResult EditInfo()
        {
            StudentCrudModel model = new StudentCrudModel();
            model.ReadCurrentData(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));

            return IsAuthenticated() ?? View(model);
        }
        [HttpPost]
        public ActionResult EditInfo(StudentCrudModel model)
        {

            if (ModelState.IsValid)
            {
                model.SaveCurrentData(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));
                Session["ShowSuccess"] = "Account Info has been edited";
                return RedirectToAction("Index");
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                Session["ShowDanger"] = "Edit account fail";
            }

            return IsAuthenticated() ?? View(model);
        }

        public ActionResult DeleteAccount()
        {
            StudentModel sm = new StudentModel();
            sm.Read(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));
            return IsAuthenticated() ?? View(sm);
        }

        [HttpPost]
        public ActionResult DeleteAccount(StudentModel model)
        {
            model.Read(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));
            model.Delete();

            Session["ShowSuccess"] = "Account Has Been Deleted";
            return IsAuthenticated() ?? RedirectToAction("Index");
        }

        public ActionResult ChangePassword()
        {
            return IsAuthenticated() ?? View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                AccountModel CurrAcc = new AccountModel();
                model.Password = WebHelper.PasswordSalter.GetSalted(model.Password);
                //model.ConfirmPassword = WebHelper.PasswordSalter.GetSalted(model.Password);

                CurrAcc.Read(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));
                if (WebHelper.PasswordSalter.VerifySalt(model.CurrentPassword, CurrAcc.Password))
                {
                    model.SaveCurrentData(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));
                    Session["ShowSuccess"] = "Password Changed";
                }

                Session["ShowSuccess"] = null;
            }else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                Session["ShowDanger"] = "Password not changed";
            }


            return IsAuthenticated() ?? RedirectToAction("Index");
        }

    }
}
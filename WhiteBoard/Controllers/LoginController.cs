﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WhiteBoard.Controllers
{
    public class LoginController : Controller, IAuthenticator
    {
        // GET: Login
        public ActionResult Index()
        {
            if(TokenModel.VerifyToken(WebHelper.WebSession.COOKIE_Token))
            {
                switch(TokenModel.Role(WebHelper.WebSession.COOKIE_Token))
                {
                    case 0: //admin
                        return RedirectToAction("Index", "Admin", null);
                    case 1: // instructor
                        return RedirectToAction("Index", "Instructor", null);
                    case 2: // student
                        return RedirectToAction("Index", "Student", null);
                }
                
            }
            return View();
        }

        [HttpPost]
        public ActionResult Index(AccountModel model)
        {
            AccountModel dbModel = new AccountModel();
            dbModel.Read(model.Email);

            bool LoginSuccess = WebHelper.PasswordSalter.VerifySalt(model.Password, dbModel.Password);
            if (LoginSuccess)
            {

                string Token = WebHelper.Hash.SHA256(DateTime.UtcNow.ToString() + model.AccountID.ToString());
                WebHelper.WebSession.COOKIE_Token = Token;
                TokenModel.SetToken(dbModel.AccountID, dbModel.Role,Token);
                WebHelper.WebSession.Email = dbModel.Email;
                WebHelper.WebSession.AccountID = dbModel.AccountID;
                WebHelper.WebSession.UserName = dbModel.Email;

                if (dbModel.Flag != 0)
                {
                    Session["AccountFlagged"] = dbModel.Flag.ToString();
                    return RedirectToAction("Flagged", "Login", null);
                }

                BusinessLayer.WebTriggers.LoggedInEvent(dbModel.AccountID);

                switch (dbModel.Role)
                {
                    case 0:
                        return RedirectToAction("Index", "Admin");
                    case 1:
                        return RedirectToAction("Index", "Instructor");
                    case 2:
                        return RedirectToAction("Index", "Student");
                    default:
                        return View();
                }
            }
            Session["ShowDanger"] = "Error Logging in";
            return View();
        }

        public ActionResult Logout()
        {
            TokenModel.Clear(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));
            WebHelper.WebSession.COOKIE_Clear();
            WebHelper.WebSession.SessionClear();
            

            return RedirectToAction("Index", "Home", null);
        }

      
        public ActionResult Flagged()
        {
  
            ViewBag.FlagMessage = WebHelper.FlaggedAccountsHelper.GetFlagMessage(Convert.ToInt32(Session["AccountFlagged"]));
            if (ViewBag.FlagMessage == "err")//if flag is not found, just go to home page
            {
                return RedirectToAction("Index", "Error");//redirect to error maybe
            }
            TokenModel.Clear(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));
            WebHelper.WebSession.COOKIE_Clear();
            WebHelper.WebSession.SessionClear();
            return View();
        }

        public ActionResult IsAuthenticated()
        {
            if (!(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token) > 0 && TokenModel.Role(WebHelper.WebSession.COOKIE_Token) == 0))
            {
                return RedirectToAction("Forbidden", "Error", null);
            }
            else return null;
        }
    }
}
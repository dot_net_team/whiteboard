﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WhiteBoard.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title2 = "something";
            switch (TokenModel.Role(WebHelper.WebSession.COOKIE_Token))
            {
                case 0:
                    return RedirectToAction("Index", "Admin");
                case 1:
                    return RedirectToAction("Index", "Instructor");
                case 2:
                    return RedirectToAction("Index", "Student");
                default:
                    return View();
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
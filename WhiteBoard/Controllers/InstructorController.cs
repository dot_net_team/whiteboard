﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace WhiteBoard.Controllers
{
    public class InstructorController : Controller, IAuthenticator
    {
        // GET: Instructor
        public ActionResult Index(InstructorModel model)
        {
            try
            {
                model.Read(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));

                if (model.Flag != 0)
                {
                    Session["AccountFlagged"] = model.Flag.ToString();
                    return RedirectToAction("Flagged", "Login", null);
                }

                return IsAuthenticated() ?? View(model);
            }
            catch (AccountNotFoundException)
            {
                return RedirectToAction("Forbidden", "Error");
            }
        }

        public ActionResult Course(CourseModel model)
        {
            model.InstructorID = model.GetInstructorIDbyAccID(model.ID);
            model.ReadCourseByInstructorID(model.InstructorID);
            return IsAuthenticated() ?? View(model);

        }

        public ActionResult QuestionBank(QuestionModel model,string ID)
        {
          
            model.ReadQuestionbank(Convert.ToInt32(ID));
            return View(model);
        }
        public ActionResult AddQuestion(QuestionModel model, string ID)
        {

            model.ReadQuestionInQB(Convert.ToInt32(ID));
            model.QuestionBankID = Convert.ToInt32(ID);
            return View(model);
        }

       

        public ActionResult Quiz(QuizModel model,string id)
        {
            model.ReadLessons();
            return View(model) ;
        }
        [HttpPost]
        public ActionResult Quiz(string ID, string numItems,string passingGrade,string listbox,string QuizTitle)
        {
            QuizModel model = new QuizModel();
            model.Addquiz(QuizTitle,Convert.ToInt32(listbox),Convert.ToInt32(ID), Convert.ToInt32(numItems), Convert.ToInt32(passingGrade));
            return RedirectToAction("Index");
        }
      
        [HttpPost]
        public ActionResult QuestionBank(QuestionModel model, string LessonTitle,string description, string ID)
        {
            
                model.Create(Convert.ToInt32(ID), LessonTitle, description);
                return RedirectToAction("QuestionBank", "Instructor");
        }

       
        [HttpPost]
        public ActionResult AddQuestion(QuestionModel model,string ID, string QuestionType,string Question, string Answer,string ChoiceA, string ChoiceB, string ChoiceC, string ChoiceD)
        {
             
                         model.add(Convert.ToInt32(ID), QuestionType, Question, Answer, ChoiceA, ChoiceB, ChoiceC, ChoiceD);
                        return RedirectToAction("AddQuestion", "Instructor");
             
        }


        public ActionResult Lessons(CourseModel model)
        {

            model.ReadCourseByID(model.CourseID.ToString());
            model.ReadLessonByCourseID(model.CourseID.ToString());

            return View(model);
        }

        public ActionResult EditCourse()
        {

            if (TokenModel.VerifyToken(WebHelper.WebSession.COOKIE_Token))
                return RedirectToAction("Course", "Instructor");
            return IsAuthenticated() ?? View("Lessons");
        }

        [HttpPost]
        public ActionResult DeleteCourse(CourseModel model)
        {
            model.Delete();
            return IsAuthenticated() ?? RedirectToAction("Index", "Instructor");
        }

        [HttpPost]
        public ActionResult EditCourse(CourseModel model)
        {

            var filename = "";
            var ext = "";
            var courseImage = "";
            foreach (string upload in Request.Files)
            {
                filename = Path.GetRandomFileName().Replace(".", "");
                ext = Path.GetExtension(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Server.MapPath("~/images/") + filename + ext);
                courseImage = filename + ext;
            }

            model.ImageLocation = courseImage;
            model.InstructorID = model.GetInstructorIDbyCourseID(model.CourseID.ToString());
            model.Update();
            return IsAuthenticated() ?? RedirectToAction("Index", "Instructor");
        }

        [HttpPost]
        public ActionResult UploadFile(CourseModel model)
        {
            var filename = "";
            var ext = "";
            var locFile = "";
            foreach (string upload in Request.Files)
            {
                filename = Path.GetRandomFileName().Replace(".", "");
                ext = Path.GetExtension(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Server.MapPath("~/files/") + filename + ext);
                locFile = filename + ext;
            }

            model.FileLoc = locFile;
            model.UploadFile();
            return IsAuthenticated() ?? RedirectToAction("LessonContent", "Instructor", new { LessonID = model.LessonID });
        }

        [HttpPost]
        public ActionResult DeleteFile(CourseModel model)
        {
            model.ReadFileByID();

            int id = model.listFiles[0].LessonID;
            model.DeleteFile();
            return IsAuthenticated() ?? RedirectToAction("LessonContent", "Instructor", new { LessonID = id });

        }

        [HttpPost]
        public ActionResult DeleteLesson(CourseModel model)
        {
            model.ReadLessonByID(model.LessonID.ToString());
            int id = Convert.ToInt32(model.listLesson[0].LCourseID);
            model.DeleteLesson();
            return IsAuthenticated() ?? RedirectToAction("Lessons", "Instructor", new { CourseID = id });
        }

        public ActionResult StudentProgress(ProgressModel model)
        {
            model.Read();
            return View(model);
        }

        public ActionResult CreateLesson()
        {
            if (TokenModel.VerifyToken(WebHelper.WebSession.COOKIE_Token))
                return View("Lessons");
            return View("Lessons");
        }

        [HttpPost]
        public ActionResult CreateLesson(CourseModel model)
        {
            model.CreateLesson();
            return RedirectToAction("Lessons", "Instructor", new { courseid = model.LCourseID });
        }

        public ActionResult LessonContent(CourseModel model)
        {
            model.ReadLessonByID(model.LessonID.ToString());
            model.ReadFilesByLessonID();
            return IsAuthenticated() ?? View(model);
        }

        [HttpPost]
        public ActionResult EditLesson(CourseModel model)
        {
            model.UpdateLesson();
            return RedirectToAction("LessonContent", "Instructor", new { LessonID = model.LessonID });

        }
        public ActionResult CreateCourse()
        {
            if (TokenModel.VerifyToken(WebHelper.WebSession.COOKIE_Token))
                return RedirectToAction("Index", "Instructor");
            return IsAuthenticated() ?? View("Course");

        }

        [HttpPost]
        public ActionResult CreateCourse(CourseModel model)
        {
            var filename = "";
            var ext = "";
            var courseImage = "";
            foreach (string upload in Request.Files)
            {
                filename = Path.GetRandomFileName().Replace(".", "");
                ext = Path.GetExtension(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Server.MapPath("~/images/") + filename + ext);
                courseImage = filename + ext;
            }

            model.ImageLocation = courseImage;
            model.InstructorID = model.GetInstructorIDbyAccID(model.ID);
            model.Create();
            return IsAuthenticated() ?? RedirectToAction("Index", "Instructor");

        }

      


        public ActionResult BrowseCourse(CourseModel model)
        {
            model.ReadAllCourse();
            return IsAuthenticated() ?? View(model);
        }

        public ActionResult IsAuthenticated()
        {
            //cookie role 1 is instructor
            if (!(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token) > 0 && TokenModel.Role(WebHelper.WebSession.COOKIE_Token) == 1))
            {
                return RedirectToAction("Forbidden", "Error", null);
            }
            else return null;
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            if (TokenModel.VerifyToken(WebHelper.WebSession.COOKIE_Token))
                return RedirectToAction("Index", "Instructor", null);
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(InstructorModel model)
        {
            if (!ModelState.IsValid)
            {
                model.DestroyCardInfo();
                return View(model);
            }
            else
            {
                try
                {
                    //salt the password at the front end instead of back end
                    model.Password = WebHelper.PasswordSalter.GetSalted(model.Password);
                    model.Create();
                }
                catch (Exception se)
                {
                    return RedirectToAction("Index", "Error", se);
                }

                //do something on model payment here before desturction
                model.DestroyCardInfo();
                return RedirectToAction("Index", "Instructor"); //successful registration
            }
        }



        ///////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////

        public ActionResult EditInfo()
        {
            InstructorCRUDModel model = new InstructorCRUDModel();
            model.ReadCurrentData(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));

            return IsAuthenticated() ?? View(model);
        }
        [HttpPost]
        public ActionResult EditInfo(InstructorCRUDModel model)
        {

            if (ModelState.IsValid)
            {
                model.SaveCurrentData(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));
                Session["ShowSuccess"] = "Account Info has been edited";
                return RedirectToAction("Index");
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                Session["ShowDanger"] = "Edit account fail";
            }

            return IsAuthenticated() ?? View(model);
        }

        public ActionResult DeleteAccount()
        {

            InstructorModel sm = new InstructorModel();
            sm.Read(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));
            return IsAuthenticated() ?? View(sm);
        }

        [HttpPost]
        public ActionResult DeleteAccount(InstructorModel model)
        {
            model.Read(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));
            model.Delete();

            Session["ShowSuccess"] = "Account Has Been Deleted";
            return IsAuthenticated() ?? RedirectToAction("Index");
        }

        public ActionResult ChangePassword()
        {
            return IsAuthenticated() ?? View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                AccountModel CurrAcc = new AccountModel();
                model.Password = WebHelper.PasswordSalter.GetSalted(model.Password);
                //model.ConfirmPassword = WebHelper.PasswordSalter.GetSalted(model.Password);

                CurrAcc.Read(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));
                if (WebHelper.PasswordSalter.VerifySalt(model.CurrentPassword, CurrAcc.Password))
                {
                    model.SaveCurrentData(TokenModel.AccountID(WebHelper.WebSession.COOKIE_Token));
                    Session["ShowSuccess"] = "Password Changed";
                }

                Session["ShowSuccess"] = null;
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                Session["ShowDanger"] = "Password not changed";
            }


            return IsAuthenticated() ?? RedirectToAction("Index");
        }


    }
}
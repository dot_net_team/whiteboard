﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WhiteBoard.Controllers
{
    public class ErrorController : Controller
    {
        //GET: Error
        //public ActionResult Index(Exception e)
        //{
        //    ViewBag.Exception = e;
        //    return View(e);
        //}
        public ActionResult Index()
        {
            return View();
        }
        //400
        public ViewResult BadRequest()
        {
            Response.StatusCode = 400;
            return View();
        }

        //403
        public ViewResult Forbidden()
        {
            Response.StatusCode = 403;
            return View();
        }

        //404
        public ViewResult NotFound()
        {
            Response.StatusCode = 404;
            return View();
        }

        //408
        public ViewResult RTO()
        {
            Response.StatusCode = 408;
            return View();
        }

        //Custom not accesible
        public ViewResult InAccessible()
        {
            return View();
        }
    }
}
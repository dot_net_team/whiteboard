﻿
/*
 * Triggers
 * (always use utc time)

use whiteboardDBv2
 */



CREATE TABLE Logs(
 [LOG_ID] INT IDENTITY(1,1) PRIMARY KEY,
 [USER_ID] INT NOT NULL, --set account id as foreign key
 COMMAND VARCHAR(50),
 SQLCOMMAND VARCHAR(MAX), --this is text instead of varchar becase some queries can be really long like a salted password
 [DESCRIPTION] VARCHAR(MAX),
 DATE_EXECUTED DATETIME2
 )
 GO

CREATE PROCEDURE spWriteLog
 @id int, @command varchar(50), @sqlcom VARCHAR(MAX), @descrip VARCHAR(MAX)
 AS
 BEGIN
 PRINT 'Writing Logs'
 PRINT @id
	INSERT INTO Logs([USER_ID],COMMAND,SQLCOMMAND,[DESCRIPTION],DATE_EXECUTED) VALUES(@id ,@command,@sqlcom,@descrip,GETUTCDATE())
END
GO

 CREATE PROCEDURE spReadLog
 @Offset INT,@Next INT
 AS
 BEGIN
	SELECT * FROM Logs ORDER BY LOG_ID DESC OFFSET @Offset  ROWS  FETCH NEXT @Next  ROWS ONLY 
END
GO

/***************************** create tirggers ****************************/
/**************************************************************************/
CREATE TRIGGER trRegisterNewAccount
ON Account
AFTER INSERT
AS
BEGIN
	DECLARE --@id VARCHAR,
	@Email VARCHAR(255),
	@Password VARCHAR(128),
	@Role VARCHAR(5)

	--SET @id = cast(@@IDENTITY AS VARCHAR)
	SELECT @Email = Email FROM inserted
	SELECT @Password = [Password] FROM inserted
	SELECT @Role = [Role] FROM inserted
	
	DECLARE @query VARCHAR(MAX), @sqltype VARCHAR(50), @descr VARCHAR(MAX)
	SET @sqltype = 'INSERT'
	SET @descr = 'Created a New Account with Account using email: ' + @Email
	SET @query = 'INSERT INTO Account(Email,[Password],[Role],[Flag]) VALUES('+@Email+','+@Password+','+@Role+',0 )'
	
	--print @id
	EXECUTE spWriteLog @@IDENTITY,@sqltype,@query,@descr
END
GO 

CREATE TRIGGER trNewAccountDetails
ON AccountDetails
FOR INSERT
AS
BEGIN
	DECLARE @id		INT,
	@FirstName		VARCHAR,
	@LastName		VARCHAR,
	@Country		VARCHAR,
	@Email			VARCHAR,
	@DateCreated	DATETIME2,
	@LastLogin		DATETIME2

	SELECT @id			= AccountID FROM inserted
	SELECT @FirstName	= FirstName FROM inserted
	SELECT @LastName	= LastName  FROM inserted
	SELECT @Country		= Country   FROM inserted
	SELECT @DateCreated =DateCreated FROM inserted
	SELECT @LastLogin	=LastLogin	FROM inserted
	
	DECLARE @query VARCHAR(MAX), @sqltype VARCHAR(50), @descr VARCHAR(MAX)
	SET @sqltype = 'INSERT'
	SET @descr = 'Created a Account details for: ' + CONVERT(VARCHAR(23), @id, 126)
	SET @query = 'INSERT INTO AccountDetails(AccountID,FirstName,LastName,DateCreated,LastLogin,Country)
				VALUES('+CONVERT(VARCHAR(23), @id, 126)+','+@FirstName+','+@LastName+','+CONVERT(VARCHAR(23), @DateCreated, 126)+','+CONVERT(VARCHAR(23), @LastLogin, 126)+','+@Country+')'


	EXECUTE spWriteLog @id,@sqltype,@query,@descr
END
GO 


--INSERT INTO Instructor VALUES(@AccountID,@Title,@SubjectArea,@Organization)
CREATE TRIGGER trNewInstructor
ON Instructor
FOR INSERT
AS
BEGIN
	DECLARE @id INT,
	@Title				VARCHAR,
	@SubjectArea		VARCHAR,
	@Organization		VARCHAR

	SELECT @id					= AccountID			FROM inserted
	SELECT @Title		    = Title				FROM inserted
	SELECT @SubjectArea     = SubjectArea		FROM inserted
	SELECT @Organization    = Organization      FROM inserted

	DECLARE @query VARCHAR(MAX), @sqltype VARCHAR(50), @descr VARCHAR(MAX)
	SET @sqltype = 'INSERT'
	SET @descr = 'Created an Instructor ID for Account: ' + CONVERT(VARCHAR(23), @id, 126)
	SET @query = 'INSERT INTO Instructor VALUES('+CONVERT(VARCHAR(23), @id, 126)+','+@Title+','+@SubjectArea+','+@Organization+')'


	EXECUTE spWriteLog @id,@sqltype,@query,@descr
END
GO 

--INSERT INTO InstructorSubscription VALUES(SCOPE_IDENTITY(),GETUTCDATE(),@Expiration,1,@PaymentMode,@Plan)
CREATE TRIGGER trNewSubscription
ON InstructorSubscription
FOR INSERT
AS
BEGIN
	DECLARE @Instid INT,
	@id INT,
	@Exp				DATETIME,
	@PaymentMode		INT,
	@Plan				INT

	SELECT @InstId				= InstructorID				FROM inserted
	SELECT @Exp					= SubscriptionExpiration	FROM inserted
	SELECT @PaymentMode			= PaymentMode				FROM inserted
	SELECT @Plan				= [Plan]					FROM inserted
	
	SET @id = (SELECT Account.ID FROM Account JOIN Instructor ON Instructor.AccountID = Account.ID WHERE Instructor.ID=@InstId)

	DECLARE @query VARCHAR(MAX), @sqltype VARCHAR(50), @descr VARCHAR(MAX)
	SET @sqltype = 'INSERT'
	SET @descr = 'Created a new subscription for instructor: ' + CONVERT(VARCHAR(23), @id, 126)
	SET @query = 'INSERT INTO InstructorSubscription VALUES('+CONVERT(VARCHAR(23), @id, 126)+',GETUTCDATE(),'+@Exp+',1,'+@PaymentMode+','+@Plan+')'


	EXECUTE spWriteLog @id,@sqltype,@query,@descr
END
GO

--INSERT INTO Student(AccountID,School)VALUES(@AccountID,@School)
CREATE TRIGGER trNewStudent
ON Student
FOR INSERT
AS
BEGIN
	DECLARE @id INT,
	@School		VARCHAR

	SELECT @id					= AccountID			FROM inserted
	SELECT @School			    = School				FROM inserted

	DECLARE @query VARCHAR(MAX), @sqltype VARCHAR(50), @descr VARCHAR(MAX)
	SET @sqltype = 'INSERT'
	SET @descr = 'Created a Student ID for Account: ' +CONVERT(VARCHAR(23), @id, 126)
	SET @query = 'INSERT INTO Student(AccountID,School)VALUES('+CONVERT(VARCHAR(23), @id, 126)+','+@School+')'


	EXECUTE spWriteLog @id,@sqltype,@query,@descr
END
GO 

--INSERT INTO Strings(Title,EditorID,[Data],LastEdit) VALUES(@Title,@id,@txt,GETUTCDATE())
CREATE TRIGGER trNewConfigString
ON Strings
FOR INSERT
AS
BEGIN
	DECLARE @id INT,
	@Title	VARCHAR(254),
	@Data VARCHAR(254)

	SELECT @id				= EditorID		FROM inserted
	SELECT @Title		    = Title			FROM inserted

	DECLARE @query VARCHAR(MAX), @sqltype VARCHAR(50), @descr VARCHAR(MAX)
	SET @sqltype = 'INSERT'
	SET @descr =  'Editor ' + CONVERT(VARCHAR(23), @id, 126) + ' has created a new config string for -> ' + @Title
	SET @query = 'INSERT INTO Strings(Title,EditorID,[Data],LastEdit) VALUES('+@Title+','+CONVERT(VARCHAR(23), @id, 126)+',*Cannot Show Text*,GETUTCDATE())'


	EXECUTE spWriteLog @id,@sqltype,@query,@descr
END
GO 

--UPDATE Student SET Student.School=@School WHERE Student.AccountID=@Id
--UPDATE AccountDetails SET FirstName=@FirstName,LastName=@LastName,Country=@Country WHERE AccountID=@Id
CREATE TRIGGER trUpdateStudentInfo
ON Student
AFTER UPDATE
AS
BEGIN
	DECLARE @id INT,
	@School	VARCHAR(254)

	SELECT @id					= AccountID		FROM inserted
	SELECT @School			    = School		FROM inserted

	DECLARE @query VARCHAR(MAX), @sqltype VARCHAR(50), @descr VARCHAR(MAX)
	SET @sqltype = 'UPDATE'
	SET @descr =  'Student ' + CONVERT(VARCHAR(23), @id, 126) + ' has updated his school info to -> ' + @School	
	SET @query = 'UPDATE Student SET Student.School='+@School+' WHERE Student.AccountID='+ CONVERT(VARCHAR(23), @id, 126)


	EXECUTE spWriteLog @id,@sqltype,@query,@descr
END
GO 


--spReadLog
--EXECUTE spRegisterAccount 'tesas12345t@test.com','pass',2,'fn','ln',1,null
--INSERT INTO Account([Role],Email,[Password],[Flag]) VALUES(3,'email23','@Password',0)
--INSERT INTO AccountDetails(AccountID,FirstName,LastName,DateCreated,LastLogin,Country) VALUES(1000000,'ff','ff',null,null,'philiipi')
----go
--spRegisterStudent 'tes1234asdasd56t@test.com','1604','test','test','test',1
--INSERT INTO Student(AccountID,School)VALUES(1000013,'huwaw')

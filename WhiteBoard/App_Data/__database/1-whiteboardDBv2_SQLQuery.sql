/*
I've used U.K. standards for max character lenghts in varchar
because:http://stackoverflow.com/questions/20958/list-of-standard-lengths-for-database-fields
link:http://webarchive.nationalarchives.gov.uk/+/http://www.cabinetoffice.gov.uk/media/254290/GDS%20Catalogue%20Vol%202.pdf

STORE ALL DATETIME AS UTC!!


notes: when using local database in app_data make sure to create a database named: 'whiteboard_local_db' to avoid conflicts 
		then run the queries as usual* using visual studio, otherwise you would have to modify your webconfig to your respective paths.

		
		*DO NOT execute 'use master' and 'CREATE DATABASE whiteboardDBv2' when using local db file
		*it is already the .mdf so no need to create another as it will be create in the local sql server not on the app_data folder
		
		please be guided accordingly thanks,
				by: noli
use master
drop database whiteboardDBv2
CREATE DATABASE whiteboardDBv2
USE whiteboardDBv2
*/


/* USER ROLES */
CREATE TABLE [Roles]
(
	[RoleId] INT NOT NULL,
	[RoleName] [VARCHAR](10) NOT NULL,
	[RoleDescription] [varchar](255) NOT NULL,
CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
    [RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, 
  IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
DELETE FROM Roles;
INSERT INTO Roles VALUES(0,'Admin','Has All the capabilities');
INSERT INTO Roles VALUES(1,'Instructor','Supplies the courses');
INSERT INTO Roles VALUES(2,'Student','Subscribes to the courses');

CREATE TABLE Account(
	ID INT IDENTITY(1000000,1) PRIMARY KEY,
	Email VARCHAR(255) UNIQUE,
	[Password] VARCHAR(128),
	[Role] INT NOT NULL,
	[Flag] int NOT NULL --0 active, 1 inactive, 2 banned, 3 deleted,
)
CREATE TABLE AccountDetails(
	AccountID INT FOREIGN KEY REFERENCES Account(ID) NOT NULL,
	FirstName VARCHAR(35),
	LastName VARCHAR(35),
	DateCreated DATETIME,
	LastLogin DATETIME,
	Country VARCHAR(35)
)

CREATE TABLE Student(
	ID INT IDENTITY(2000000,1) PRIMARY KEY,
	AccountID INT FOREIGN KEY REFERENCES Account(ID),
	School VARCHAR(255)
)

CREATE TABLE Instructor(
	ID INT IDENTITY(3000000,1) PRIMARY KEY,
	AccountID INT FOREIGN KEY REFERENCES Account(ID) NOT NULL,
	Title VARCHAR(255),
	SubjectArea VARCHAR(255),
	Organization VARCHAR(255)
)

CREATE TABLE Course(
	ID INT IDENTITY(1000,1) PRIMARY KEY,
	InstructorID INT FOREIGN KEY REFERENCES Instructor(ID) NOT NULL,
	Title VARCHAR(255),
	[Description] TEXT,
	ImageLocation VARCHAR(255),
	CategoryID int,
	CoursePrice decimal(19,4)
)

CREATE TABLE CourseSubscription(
	ID INT IDENTITY(1000,1) PRIMARY KEY,
	StudentID INT FOREIGN KEY REFERENCES Student(ID) NOT NULL,
	CourseID INT FOREIGN KEY REFERENCES Course(ID) NOT NULL,
	DateAdded DATETIME,
	Expiration DATETIME
)

CREATE TABLE SubscriptionPlanList(
	ID INT PRIMARY KEY,
	SubscriptionPlanName VARCHAR(35),
	SubscriptionPrice INT
)

CREATE TABLE PaymentModeList(
	ID INT PRIMARY KEY,
	PaymentModePlanName VARCHAR(35)
)

CREATE TABLE InstructorSubscription(
	ID INT IDENTITY(1000,1) PRIMARY KEY,
	InstructorID INT FOREIGN KEY REFERENCES Instructor(ID) NOT NULL,
	SubscriptionDate DATETIME,
	SubscriptionExpiration DATETIME,
	Active BIT,
	PaymentMode INT FOREIGN KEY REFERENCES PaymentModeList(ID) NOT NULL ,
	[Plan] INT FOREIGN KEY REFERENCES SubscriptionPlanList(ID) NOT NULL
)

CREATE TABLE COOKIE(
	Token VARCHAR(64) PRIMARY KEY,
	AccountID INT FOREIGN KEY REFERENCES Account(ID) NOT NULL,
	AccountRole INT NOT NULL,
)

CREATE TABLE Categories
(
	TagID int identity(1000,1) primary key,
	CategoryName varchar (64)
)

CREATE TABLE Favorites
(
	ID int identity(1000,1) primary key,
	TagID int foreign key references Categories(TagID),
	StudentID INT FOREIGN KEY REFERENCES Student(ID) NOT NULL
)

INSERT INTO Categories Values
('Social Science'),
('Mathematics'),
('Computer Science')
select * from Categories
select * from Favorites
select * from course

-- nilipat ko to sa query 4 kasi kailangan may naka enroll na student bago ma lagay
--
--Alter table Course add CategoryID int
--update Course set CategoryID=1000 where ID= 1001
--update Course set CategoryID=1000 where ID= 1003
--update Course set CategoryID=1001 where ID= 1002
--update Course set CategoryID=1001 where ID= 1004
--select * from Course
--insert into Favorites values (1001,2000001)

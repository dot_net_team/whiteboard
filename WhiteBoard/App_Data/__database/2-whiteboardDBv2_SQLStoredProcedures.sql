/*
	 stored procedures for whiteboardDBv2
	 
use master
USE whiteboardDBv2
 */


/* (private) Register Account Stored Procedure */
/* ##############@@@@@@@@@@  REGISTER ACCOUNT  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spRegisterAccount
@Email VARCHAR(255),@Password VARCHAR(255),@Role INT,
@FirstName VARCHAR(35),@LastName VARCHAR(35), @Country VARCHAR(35),
@Identity INT OUTPUT
AS
BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			INSERT INTO Account([Role],Email,[Password],[Flag]) VALUES(@Role,@Email,@Password,0)
			SELECT @Identity = SCOPE_IDENTITY()
			INSERT INTO AccountDetails(AccountID,FirstName,LastName,DateCreated,LastLogin,Country)
				VALUES(@Identity,@FirstName,@LastName,GETUTCDATE(),GETUTCDATE(),@Country)
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE
			   @ErMessage NVARCHAR(2048),
			   @ErSeverity INT,
			   @ErState INT
			SELECT
				@ErSeverity = ERROR_SEVERITY(),
				@ErState = ERROR_STATE(),
				@ErMessage = ERROR_MESSAGE()
			RAISERROR (@ErMessage,@ErSeverity,@ErState )
			ROLLBACK TRANSACTION
		END CATCH
END
GO
/* Register Instructor Stored Procedure */
/* ##############@@@@@@@@@@  REG INSTRUCTOR  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spRegisterInstructor
@Email VARCHAR(255),@Password VARCHAR(255),
@FirstName VARCHAR(35),@LastName VARCHAR(35),
@Title VARCHAR(255),@SubjectArea VARCHAR(255),@Organization VARCHAR(255),
@PaymentMode INT, @Plan INT, @Expiration DATETIME, @Country VARCHAR(35)
AS
BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			DECLARE @AccountID INT
			EXECUTE spRegisterAccount @Email,@Password,1,@FirstName,@LastName,@Country,@AccountID Output
			INSERT INTO Instructor VALUES(@AccountID,@Title,@SubjectArea,@Organization)
			INSERT INTO InstructorSubscription VALUES(SCOPE_IDENTITY(),GETUTCDATE(),
				@Expiration,1,@PaymentMode,@Plan)
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE
			   @ErMessage NVARCHAR(2048),
			   @ErSeverity INT,
			   @ErState INT
			SELECT
				@ErSeverity = ERROR_SEVERITY(),
				@ErState = ERROR_STATE(),
				@ErMessage = ERROR_MESSAGE()
			RAISERROR (@ErMessage,@ErSeverity,@ErState )
			ROLLBACK TRANSACTION
		END CATCH
END
GO
/* ##############@@@@@@@@@@  REG SRUDENT  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spRegisterStudent
@Email VARCHAR(255),@Password VARCHAR(255),
@FirstName VARCHAR(35),@LastName VARCHAR(35),
@School VARCHAR(255),@Country VARCHAR(35)
AS
BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			DECLARE @AccountID INT
			EXECUTE spRegisterAccount @Email,@Password,2,@FirstName,@LastName,@Country,@AccountID Output
			INSERT INTO Student(AccountID,School)VALUES(@AccountID,@School)
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE
			   @ErMessage NVARCHAR(2048),
			   @ErSeverity INT,
			   @ErState INT
			SELECT
				@ErSeverity = ERROR_SEVERITY(),
				@ErState = ERROR_STATE(),
				@ErMessage = ERROR_MESSAGE()
			RAISERROR (@ErMessage,@ErSeverity,@ErState )
			ROLLBACK TRANSACTION
		END CATCH
END
GO
/* ##############@@@@@@@@@@  Save Cookie  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spSaveCookie
@Token VARCHAR(64),@AccountID INT, @AccountRole INT
AS
BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			INSERT INTO COOKIE VALUES(@Token,@AccountID,@AccountRole)
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE
			   @ErMessage NVARCHAR(2048),
			   @ErSeverity INT,
			   @ErState INT
			SELECT
				@ErSeverity = ERROR_SEVERITY(),
				@ErState = ERROR_STATE(),
				@ErMessage = ERROR_MESSAGE()
			RAISERROR (@ErMessage,@ErSeverity,@ErState )
			ROLLBACK TRANSACTION
		END CATCH
END
GO

/* ##############@@@@@@@@@@  ChangeFlag  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spEditAccountFlag
@Id int,@flag int
AS
BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			UPDATE Account SET [Flag]=@flag WHERE ID=@Id
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE
			   @ErMessage NVARCHAR(2048),
			   @ErSeverity INT,
			   @ErState INT
			SELECT
				@ErSeverity = ERROR_SEVERITY(),
				@ErState = ERROR_STATE(),
				@ErMessage = ERROR_MESSAGE()
			RAISERROR (@ErMessage,@ErSeverity,@ErState )
			ROLLBACK TRANSACTION
		END CATCH
END
GO
/* ##############@@@@@@@@@@  EDIT SRUDENT  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spEditStudent
@Id int,@FirstName VARCHAR(35),@LastName VARCHAR(35),
@School VARCHAR(255),@Country VARCHAR(35)
AS
BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			UPDATE Student SET Student.School=@School WHERE Student.AccountID=@Id
			UPDATE AccountDetails SET FirstName=@FirstName,LastName=@LastName,Country=@Country WHERE AccountID=@Id
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE
			   @ErMessage NVARCHAR(2048),
			   @ErSeverity INT,
			   @ErState INT
			SELECT
				@ErSeverity = ERROR_SEVERITY(),
				@ErState = ERROR_STATE(),
				@ErMessage = ERROR_MESSAGE()
			RAISERROR (@ErMessage,@ErSeverity,@ErState )
			ROLLBACK TRANSACTION
		END CATCH
END
GO

/* ##############@@@@@@@@@@  Change Password  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spEditAccountPassword
@Id int,@password VARCHAR(254)
AS
BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			UPDATE Account SET [Password]=@password WHERE ID=@id
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE
			   @ErMessage NVARCHAR(2048),
			   @ErSeverity INT,
			   @ErState INT
			SELECT
				@ErSeverity = ERROR_SEVERITY(),
				@ErState = ERROR_STATE(),
				@ErMessage = ERROR_MESSAGE()
			RAISERROR (@ErMessage,@ErSeverity,@ErState )
			ROLLBACK TRANSACTION
		END CATCH
END
GO

/* ##############@@@@@@@@@@  EDIT Instructor  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spEditInstructor
@Id int,@FirstName VARCHAR(35),@LastName VARCHAR(35),
@Organization VARCHAR(255),@Country VARCHAR(35)
AS
BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			UPDATE Instructor SET Instructor.Organization=@Organization WHERE Instructor.AccountID=@Id
			UPDATE AccountDetails SET FirstName=@FirstName,LastName=@LastName,Country=@Country WHERE AccountID=@Id
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE
			   @ErMessage NVARCHAR(2048),
			   @ErSeverity INT,
			   @ErState INT
			SELECT
				@ErSeverity = ERROR_SEVERITY(),
				@ErState = ERROR_STATE(),
				@ErMessage = ERROR_MESSAGE()
			RAISERROR (@ErMessage,@ErSeverity,@ErState )
			ROLLBACK TRANSACTION
		END CATCH
END
GO
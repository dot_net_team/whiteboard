﻿/*
	 configuration management queries for whiteboardDBv2
	 
	 for strings

use master
USE whiteboardDBv2 
 */

CREATE TABLE Strings(
	Title VARCHAR(254) PRIMARY KEY NOT NULL,
	EditorID INT DEFAULT -10,
	[Data] TEXT,
	LastEdit DATETIME2
)
GO

CREATE PROCEDURE spGetConfString 
@Title VARCHAR(254)
AS
BEGIN
	IF EXISTS (SELECT [Data] FROM Strings WHERE Title=@Title)
		BEGIN 
			--SELECT * FROM Strings 
			SELECT * FROM Strings WHERE Title=@Title
		END
	ELSE
		BEGIN 
			INSERT INTO Strings VALUES(@Title,-4,'NULL',GETUTCDATE())
			SELECT * FROM Strings WHERE Title=@Title
		END
END
GO

CREATE PROCEDURE spGetFaString 
@Title VARCHAR(254)
AS
BEGIN
		BEGIN 
			--SELECT * FROM Strings 
			SELECT Title,[Data] FROM Strings WHERE Title=@Title
		END

END
GO

CREATE PROCEDURE spGetAllBanners
AS
BEGIN
		BEGIN 
			--SELECT * FROM Strings 
			SELECT Title,[Data] FROM Strings WHERE Title LIKE 'ba-banner%'
		END
END
GO

CREATE PROCEDURE spGetAllFontAwesome 
AS
BEGIN
		BEGIN 
			--SELECT * FROM Strings 
			SELECT Title,[Data] FROM Strings WHERE Title LIKE 'fa-%'
		END
END
GO

CREATE PROCEDURE spGetAllTypeFace
AS
BEGIN
		BEGIN 
			--SELECT * FROM Strings 
			SELECT Title,[Data] FROM Strings WHERE Title LIKE 'font-%'
		END
END
GO

CREATE PROCEDURE spEditConfString
@id INT,
@Title VARCHAR(254),
@txt TEXT
AS
BEGIN
	DECLARE @RowCount1 INTEGER
		UPDATE Strings SET [Data]=@txt,LastEdit=GETUTCDATE() WHERE Title=@Title
	SELECT @RowCount1 = @@ROWCOUNT
	IF( @RowCount1 = 0)
		BEGIN
			INSERT INTO Strings(Title,EditorID,[Data],LastEdit) VALUES(@Title,@id,@txt,GETUTCDATE())
		END	
END
GO

/*
	THIS insert does not matter anymore, it is auto created when not found in the catch method of the stored prodedures
	BUT STILL run this to avoid so many artifacts when running for the first time
*/

spEditConfString -5,'HomePage_Top_Header_String','Learning Made Easy' 
GO				 
spEditConfString -5,'HomePage_Top_Header_SubString','Start Learning Now' 
GO				
spEditConfString -5,'HomePage_Head_WebsiteName','Whiteboard' 
GO				
spEditConfString -5,'HomePage_Head_Tab1','Features' 
GO				
spEditConfString -5,'HomePage_Head_Tab1_detail','Lorem ipsum' 
GO				
spEditConfString -5,'HomePage_Head_Tab2','Courses' 
GO				
spEditConfString -5,'HomePage_Head_Tab2_detail','Lorem ipsum' 
GO				
spEditConfString -5,'HomePage_Head_Tab3','Plans and Pricing' 
GO				
spEditConfString -5,'HomePage_Head_Tab3_detail','Lorem ipsum' 
GO				
spEditConfString -5,'HomePage_Head_Tab_Login','Login' 
GO				
spEditConfString -5,'HomePage_Head_Tab_Logout','Logout' 
GO				
spEditConfString -5,'HomePage_Tab1_1','Learn anything' 
GO				
spEditConfString -5,'HomePage_Tab1_2','Learn anything' 
GO				
spEditConfString -5,'HomePage_Tab1_3','Learn anything' 
GO				
spEditConfString -5,'HomePage_Tab1_1_1','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nec vero sum nescius esse utilitatem in historia, non modo voluptatem' 
GO				
spEditConfString -5,'HomePage_Tab1_1_2','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nec vero sum nescius esse utilitatem in historia, non modo voluptatemg' 
GO				
spEditConfString -5,'HomePage_Tab1_1_3','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nec vero sum nescius esse utilitatem in historia, non modo voluptatem' 
GO				
spEditConfString -5,'HomePage_Copyright','&copy; 2016 - Whiteboard'
GO				
				
				
spEditConfString -5,'HomePage_Plan1_Name','Bachelor'
GO				
spEditConfString -5,'HomePage_Plan1_Monthly','$9.00'
GO				
spEditConfString -5,'HomePage_Plan1_NumberOfLesson','4'
GO				
spEditConfString -5,'HomePage_Plan1_NumberOfQuestionBank','5'
GO				
spEditConfString -5,'HomePage_Plan1_TransactionFee','15%'
GO				
spEditConfString -5,'HomePage_Plan1_NumberOfCourse','5'
GO				
spEditConfString -5,'HomePage_Plan2_Name','Doctor'
GO				
spEditConfString -5,'HomePage_Plan2_Monthly','$49.00'
GO				
spEditConfString -5,'HomePage_Plan2_NumberOfCourse','Unlimited'
GO				
spEditConfString -5,'HomePage_Plan2_NumberOfLesson','Unlimited'
GO				
spEditConfString -5,'HomePage_Plan2_NumberOfQuestionBank','Unlimited'
GO				
spEditConfString -5,'HomePage_Plan2_ExtraFeature1','Upload Pictures and Videos'
GO				
spEditConfString -5,'HomePage_Plan2_TransactionFee','15%'
				
GO				
spEditConfString -5,'HomePage_Plan3_Name','Master'
GO				
spEditConfString -5,'HomePage_Plan3_Monthly','$19.00'
GO				
spEditConfString -5,'HomePage_Plan3_NumberOfCourse','10'
GO				
spEditConfString -5,'HomePage_Plan3_NumberOfLesson','10'
GO				
spEditConfString -5,'HomePage_Plan3_NumberOfQuestionBank','10'
GO				
spEditConfString -5,'HomePage_Plan3_TransactionFee','10%'
GO				
spEditConfString -5,'HomePage_Head_PageTitle', 'WhiteBoard - Learning Made Easy'
GO
spEditConfString -5, 'HomePage_Plan1_Monthly' , '9.00'
GO
spEditConfString -5, 'HomePage_Plan2_Monthly' , '19.00'
GO
spEditConfString -5, 'HomePage_Plan3_Monthly' , '49.00'
GO


spEditConfString -5,'fa-adn'                                 ,'&#xf170;'
Go
spEditConfString -5,'fa-align-center'                        ,'&#xf037;'
Go
spEditConfString -5,'fa-align-justify'                       ,'&#xf039;'
Go
spEditConfString -5,'fa-align-left'                          ,'&#xf036;'
Go
spEditConfString -5,'fa-align-right'                         ,'&#xf038;'
Go
spEditConfString -5,'fa-amazon'                              ,'&#xf270;'
Go
spEditConfString -5,'fa-ambulance'                           ,'&#xf0f9;'
Go
spEditConfString -5,'fa-american-sign-language-interpreting' ,'&#xf2a3;'
Go
spEditConfString -5,'fa-anchor'                              ,'&#xf13d;'
Go
spEditConfString -5,'fa-android'                             ,'&#xf17b;'
Go
spEditConfString -5,'fa-angellist'                           ,'&#xf209;'
Go
spEditConfString -5,'fa-angle-double-down'                   ,'&#xf103;'
Go
spEditConfString -5,'fa-angle-double-left'                   ,'&#xf100;'
Go
spEditConfString -5,'fa-angle-double-right'                  ,'&#xf101;'
Go
spEditConfString -5,'fa-angle-double-up'                     ,'&#xf102;'
Go
spEditConfString -5,'fa-angle-down'                          ,'&#xf107;'
Go
spEditConfString -5,'fa-angle-left'                          ,'&#xf104;'
Go
spEditConfString -5,'fa-angle-right'                         ,'&#xf105;'
Go
spEditConfString -5,'fa-angle-up'                            ,'&#xf106;'
Go
spEditConfString -5,'fa-apple'                               ,'&#xf179;'
Go
spEditConfString -5,'fa-archive'                             ,'&#xf187;'
Go
spEditConfString -5,'fa-area-chart'                          ,'&#xf1fe;'
Go
spEditConfString -5,'fa-arrow-circle-down'                   ,'&#xf0ab;'
Go
spEditConfString -5,'fa-arrow-circle-left'                   ,'&#xf0a8;'
Go
spEditConfString -5,'fa-arrow-circle-o-down'                 ,'&#xf01a;'
Go
spEditConfString -5,'fa-arrow-circle-o-left'                 ,'&#xf190;'
Go
spEditConfString -5,'fa-arrow-circle-o-right'                ,'&#xf18e;'
Go
spEditConfString -5,'fa-arrow-circle-o-up'                   ,'&#xf01b;'
Go
spEditConfString -5,'fa-arrow-circle-right'                  ,'&#xf0a9;'
Go
spEditConfString -5,'fa-arrow-circle-up'                     ,'&#xf0aa;'
Go
spEditConfString -5,'fa-arrow-down'                          ,'&#xf063;'
Go
spEditConfString -5,'fa-arrow-left'                          ,'&#xf060;'
Go
spEditConfString -5,'fa-arrow-right'                         ,'&#xf061;'
Go
spEditConfString -5,'fa-arrow-up'                            ,'&#xf062;'
Go
spEditConfString -5,'fa-arrows'                              ,'&#xf047;'
Go
spEditConfString -5,'fa-arrows-alt'                          ,'&#xf0b2;'
Go
spEditConfString -5,'fa-arrows-h'                            ,'&#xf07e;'
Go
spEditConfString -5,'fa-arrows-v'                            ,'&#xf07d;'
Go
spEditConfString -5,'fa-asl-interpreting'                    ,'&#xf2a3;'
Go
spEditConfString -5,'fa-assistive-listening-systems'         ,'&#xf2a2;'
Go
spEditConfString -5,'fa-asterisk'                            ,'&#xf069;'
Go
spEditConfString -5,'fa-at'                                  ,'&#xf1fa;'
Go
spEditConfString -5,'fa-audio-description'                   ,'&#xf29e;'
Go
spEditConfString -5,'fa-automobile'                          ,'&#xf1b9;'
Go
spEditConfString -5,'fa-backward'                            ,'&#xf04a;'
Go
spEditConfString -5,'fa-balance-scale'                       ,'&#xf24e;'
Go
spEditConfString -5,'fa-ban'                                 ,'&#xf05e;'
Go
spEditConfString -5,'fa-bank'                                ,'&#xf19c;'
Go
spEditConfString -5,'fa-bar-chart'                           ,'&#xf080;'
Go
spEditConfString -5,'fa-bar-chart-o'                         ,'&#xf080;'
Go
spEditConfString -5,'fa-barcode'                             ,'&#xf02a;'
Go
spEditConfString -5,'fa-bars'                                ,'&#xf0c9;'
Go
spEditConfString -5,'fa-battery-0'                           ,'&#xf244;'
Go
spEditConfString -5,'fa-battery-1'                           ,'&#xf243;'
Go
spEditConfString -5,'fa-battery-2'                           ,'&#xf242;'
Go
spEditConfString -5,'fa-battery-3'                           ,'&#xf241;'
Go
spEditConfString -5,'fa-battery-4'                           ,'&#xf240;'
Go
spEditConfString -5,'fa-battery-empty'                       ,'&#xf244;'
Go
spEditConfString -5,'fa-battery-full'                        ,'&#xf240;'
Go
spEditConfString -5,'fa-battery-half'                        ,'&#xf242;'
Go
spEditConfString -5,'fa-battery-quarter'                     ,'&#xf243;'
Go
spEditConfString -5,'fa-battery-three-quarters'              ,'&#xf241;'
Go
spEditConfString -5,'fa-bed'                                 ,'&#xf236;'
Go
spEditConfString -5,'fa-beer'                                ,'&#xf0fc;'
Go
spEditConfString -5,'fa-behance'                             ,'&#xf1b4;'
Go
spEditConfString -5,'fa-behance-square'                      ,'&#xf1b5;'
Go
spEditConfString -5,'fa-bell'                                ,'&#xf0f3;'
Go
spEditConfString -5,'fa-bell-o'                              ,'&#xf0a2;'
Go
spEditConfString -5,'fa-bell-slash'                          ,'&#xf1f6;'
Go
spEditConfString -5,'fa-bell-slash-o'                        ,'&#xf1f7;'
Go
spEditConfString -5,'fa-bicycle'                             ,'&#xf206;'
Go
spEditConfString -5,'fa-binoculars'                          ,'&#xf1e5;'
Go
spEditConfString -5,'fa-birthday-cake'                       ,'&#xf1fd;'
Go
spEditConfString -5,'fa-bitbucket'                           ,'&#xf171;'
Go
spEditConfString -5,'fa-bitbucket-square'                    ,'&#xf172;'
Go
spEditConfString -5,'fa-bitcoin'                             ,'&#xf15a;'
Go
spEditConfString -5,'fa-black-tie'                           ,'&#xf27e;'
Go
spEditConfString -5,'fa-blind'                               ,'&#xf29d;'
Go
spEditConfString -5,'fa-bluetooth'                           ,'&#xf293;'
Go
spEditConfString -5,'fa-bluetooth-b'                         ,'&#xf294;'
Go
spEditConfString -5,'fa-bold'                                ,'&#xf032;'
Go
spEditConfString -5,'fa-bolt'                                ,'&#xf0e7;'
Go
spEditConfString -5,'fa-bomb'                                ,'&#xf1e2;'
Go
spEditConfString -5,'fa-book'                                ,'&#xf02d;'
Go
spEditConfString -5,'fa-bookmark'                            ,'&#xf02e;'
Go
spEditConfString -5,'fa-bookmark-o'                          ,'&#xf097;'
Go
spEditConfString -5,'fa-braille'                             ,'&#xf2a1;'
Go
spEditConfString -5,'fa-briefcase'                           ,'&#xf0b1;'
Go
spEditConfString -5,'fa-btc'                                 ,'&#xf15a;'
Go
spEditConfString -5,'fa-bug'                                 ,'&#xf188;'
Go
spEditConfString -5,'fa-building'                            ,'&#xf1ad;'
Go
spEditConfString -5,'fa-building-o'                          ,'&#xf0f7;'
Go
spEditConfString -5,'fa-bullhorn'                            ,'&#xf0a1;'
Go
spEditConfString -5,'fa-bullseye'                            ,'&#xf140;'
Go
spEditConfString -5,'fa-bus'                                 ,'&#xf207;'
Go
spEditConfString -5,'fa-buysellads'                          ,'&#xf20d;'
Go
spEditConfString -5,'fa-cab'                                 ,'&#xf1ba;'
Go
spEditConfString -5,'fa-calculator'                          ,'&#xf1ec;'
Go
spEditConfString -5,'fa-calendar'                            ,'&#xf073;'
Go
spEditConfString -5,'fa-calendar-check-o'                    ,'&#xf274;'
Go
spEditConfString -5,'fa-calendar-minus-o'                    ,'&#xf272;'
Go
spEditConfString -5,'fa-calendar-o'                          ,'&#xf133;'
Go
spEditConfString -5,'fa-calendar-plus-o'                     ,'&#xf271;'
Go
spEditConfString -5,'fa-calendar-times-o'                    ,'&#xf273;'
Go
spEditConfString -5,'fa-camera'                              ,'&#xf030;'
Go
spEditConfString -5,'fa-camera-retro'                        ,'&#xf083;'
Go
spEditConfString -5,'fa-car'                                 ,'&#xf1b9;'
Go
spEditConfString -5,'fa-caret-down'                          ,'&#xf0d7;'
Go
spEditConfString -5,'fa-caret-left'                          ,'&#xf0d9;'
Go
spEditConfString -5,'fa-caret-right'                         ,'&#xf0da;'
Go
spEditConfString -5,'fa-caret-square-o-down'                 ,'&#xf150;'
Go
spEditConfString -5,'fa-caret-square-o-left'                 ,'&#xf191;'
Go
spEditConfString -5,'fa-caret-square-o-right'                ,'&#xf152;'
Go
spEditConfString -5,'fa-caret-square-o-up'                   ,'&#xf151;'
Go
spEditConfString -5,'fa-caret-up'                            ,'&#xf0d8;'
Go
spEditConfString -5,'fa-cart-arrow-down'                     ,'&#xf218;'
Go
spEditConfString -5,'fa-cart-plus'                           ,'&#xf217;'
Go
spEditConfString -5,'fa-cc'                                  ,'&#xf20a;'
Go
spEditConfString -5,'fa-cc-amex'                             ,'&#xf1f3;'
Go
spEditConfString -5,'fa-cc-diners-club'                      ,'&#xf24c;'
Go
spEditConfString -5,'fa-cc-discover'                         ,'&#xf1f2;'
Go
spEditConfString -5,'fa-cc-jcb'                              ,'&#xf24b;'
Go
spEditConfString -5,'fa-cc-mastercard'                       ,'&#xf1f1;'
Go
spEditConfString -5,'fa-cc-paypal'                           ,'&#xf1f4;'
Go
spEditConfString -5,'fa-cc-stripe'                           ,'&#xf1f5;'
Go
spEditConfString -5,'fa-cc-visa'                             ,'&#xf1f0;'
Go
spEditConfString -5,'fa-certificate'                         ,'&#xf0a3;'
Go
spEditConfString -5,'fa-chain'                               ,'&#xf0c1;'
Go
spEditConfString -5,'fa-chain-broken'                        ,'&#xf127;'
Go
spEditConfString -5,'fa-check'                               ,'&#xf00c;'
Go
spEditConfString -5,'fa-check-circle'                        ,'&#xf058;'
Go
spEditConfString -5,'fa-check-circle-o'                      ,'&#xf05d;'
Go
spEditConfString -5,'fa-check-square'                        ,'&#xf14a;'
Go
spEditConfString -5,'fa-check-square-o'                      ,'&#xf046;'
Go
spEditConfString -5,'fa-chevron-circle-down'                 ,'&#xf13a;'
Go
spEditConfString -5,'fa-chevron-circle-left'                 ,'&#xf137;'
Go
spEditConfString -5,'fa-chevron-circle-right'                ,'&#xf138;'
Go
spEditConfString -5,'fa-chevron-circle-up'                   ,'&#xf139;'
Go
spEditConfString -5,'fa-chevron-down'                        ,'&#xf078;'
Go
spEditConfString -5,'fa-chevron-left'                        ,'&#xf053;'
Go
spEditConfString -5,'fa-chevron-right'                       ,'&#xf054;'
Go
spEditConfString -5,'fa-chevron-up'                          ,'&#xf077;'
Go
spEditConfString -5,'fa-child'                               ,'&#xf1ae;'
Go
spEditConfString -5,'fa-chrome'                              ,'&#xf268;'
Go
spEditConfString -5,'fa-circle'                              ,'&#xf111;'
Go
spEditConfString -5,'fa-circle-o'                            ,'&#xf10c;'
Go
spEditConfString -5,'fa-circle-o-notch'                      ,'&#xf1ce;'
Go
spEditConfString -5,'fa-circle-thin'                         ,'&#xf1db;'
Go
spEditConfString -5,'fa-clipboard'                           ,'&#xf0ea;'
Go
spEditConfString -5,'fa-clock-o'                             ,'&#xf017;'
Go
spEditConfString -5,'fa-clone'                               ,'&#xf24d;'
Go
spEditConfString -5,'fa-close'                               ,'&#xf00d;'
Go
spEditConfString -5,'fa-cloud'                               ,'&#xf0c2;'
Go
spEditConfString -5,'fa-cloud-download'                      ,'&#xf0ed;'
Go
spEditConfString -5,'fa-cloud-upload'                        ,'&#xf0ee;'
Go
spEditConfString -5,'fa-cny'                                 ,'&#xf157;'
Go
spEditConfString -5,'fa-code'                                ,'&#xf121;'
Go
spEditConfString -5,'fa-code-fork'                           ,'&#xf126;'
Go
spEditConfString -5,'fa-codepen'                             ,'&#xf1cb;'
Go
spEditConfString -5,'fa-codiepie'                            ,'&#xf284;'
Go
spEditConfString -5,'fa-coffee'                              ,'&#xf0f4;'
Go
spEditConfString -5,'fa-cog'                                 ,'&#xf013;'
Go
spEditConfString -5,'fa-cogs'                                ,'&#xf085;'
Go
spEditConfString -5,'fa-columns'                             ,'&#xf0db;'
Go
spEditConfString -5,'fa-comment'                             ,'&#xf075;'
Go
spEditConfString -5,'fa-comment-o'                           ,'&#xf0e5;'
Go
spEditConfString -5,'fa-commenting'                          ,'&#xf27a;'
Go
spEditConfString -5,'fa-commenting-o'                        ,'&#xf27b;'
Go
spEditConfString -5,'fa-comments'                            ,'&#xf086;'
Go
spEditConfString -5,'fa-comments-o'                          ,'&#xf0e6;'
Go
spEditConfString -5,'fa-compass'                             ,'&#xf14e;'
Go
spEditConfString -5,'fa-compress'                            ,'&#xf066;'
Go
spEditConfString -5,'fa-connectdevelop'                      ,'&#xf20e;'
Go
spEditConfString -5,'fa-contao'                              ,'&#xf26d;'
Go
spEditConfString -5,'fa-copy'                                ,'&#xf0c5;'
Go
spEditConfString -5,'fa-copyright'                           ,'&#xf1f9;'
Go
spEditConfString -5,'fa-creative-commons'                    ,'&#xf25e;'
Go
spEditConfString -5,'fa-credit-card'                         ,'&#xf09d;'
Go
spEditConfString -5,'fa-credit-card-alt'                     ,'&#xf283;'
Go
spEditConfString -5,'fa-crop'                                ,'&#xf125;'
Go
spEditConfString -5,'fa-crosshairs'                          ,'&#xf05b;'
Go
spEditConfString -5,'fa-css3'                                ,'&#xf13c;'
Go
spEditConfString -5,'fa-cube'                                ,'&#xf1b2;'
Go
spEditConfString -5,'fa-cubes'                               ,'&#xf1b3;'
Go
spEditConfString -5,'fa-cut'                                 ,'&#xf0c4;'
Go
spEditConfString -5,'fa-cutlery'                             ,'&#xf0f5;'
Go
spEditConfString -5,'fa-dashboard'                           ,'&#xf0e4;'
Go
spEditConfString -5,'fa-dashcube'                            ,'&#xf210;'
Go
spEditConfString -5,'fa-database'                            ,'&#xf1c0;'
Go
spEditConfString -5,'fa-deaf'                                ,'&#xf2a4;'
Go
spEditConfString -5,'fa-deafness'                            ,'&#xf2a4;'
Go
spEditConfString -5,'fa-dedent'                              ,'&#xf03b;'
Go
spEditConfString -5,'fa-delicious'                           ,'&#xf1a5;'
Go
spEditConfString -5,'fa-desktop'                             ,'&#xf108;'
Go
spEditConfString -5,'fa-deviantart'                          ,'&#xf1bd;'
Go
spEditConfString -5,'fa-diamond'                             ,'&#xf219;'
Go
spEditConfString -5,'fa-digg'                                ,'&#xf1a6;'
Go
spEditConfString -5,'fa-dollar'                              ,'&#xf155;'
Go
spEditConfString -5,'fa-dot-circle-o'                        ,'&#xf192;'
Go
spEditConfString -5,'fa-download'                            ,'&#xf019;'
Go
spEditConfString -5,'fa-dribbble'                            ,'&#xf17d;'
Go
spEditConfString -5,'fa-dropbox'                             ,'&#xf16b;'
Go
spEditConfString -5,'fa-drupal'                              ,'&#xf1a9;'
Go
spEditConfString -5,'fa-edge'                                ,'&#xf282;'
Go
spEditConfString -5,'fa-edit'                                ,'&#xf044;'
Go
spEditConfString -5,'fa-eject'                               ,'&#xf052;'
Go
spEditConfString -5,'fa-ellipsis-h'                          ,'&#xf141;'
Go
spEditConfString -5,'fa-ellipsis-v'                          ,'&#xf142;'
Go
spEditConfString -5,'fa-empire'                              ,'&#xf1d1;'
Go
spEditConfString -5,'fa-envelope'                            ,'&#xf0e0;'
Go
spEditConfString -5,'fa-envelope-o'                          ,'&#xf003;'
Go
spEditConfString -5,'fa-envelope-square'                     ,'&#xf199;'
Go
spEditConfString -5,'fa-envira'                              ,'&#xf299;'
Go
spEditConfString -5,'fa-eraser'                              ,'&#xf12d;'
Go
spEditConfString -5,'fa-eur'                                 ,'&#xf153;'
Go
spEditConfString -5,'fa-euro'                                ,'&#xf153;'
Go
spEditConfString -5,'fa-exchange'                            ,'&#xf0ec;'
Go
spEditConfString -5,'fa-exclamation'                         ,'&#xf12a;'
Go
spEditConfString -5,'fa-exclamation-circle'                  ,'&#xf06a;'
Go
spEditConfString -5,'fa-exclamation-triangle'                ,'&#xf071;'
Go
spEditConfString -5,'fa-expand'                              ,'&#xf065;'
Go
spEditConfString -5,'fa-expeditedssl'                        ,'&#xf23e;'
Go
spEditConfString -5,'fa-external-link'                       ,'&#xf08e;'
Go
spEditConfString -5,'fa-external-link-square'                ,'&#xf14c;'
Go
spEditConfString -5,'fa-eye'                                 ,'&#xf06e;'
Go
spEditConfString -5,'fa-eye-slash'                           ,'&#xf070;'
Go
spEditConfString -5,'fa-eyedropper'                          ,'&#xf1fb;'
Go
spEditConfString -5,'fa-fa'                                  ,'&#xf2b4;'
Go
spEditConfString -5,'fa-facebook'                            ,'&#xf09a;'
Go
spEditConfString -5,'fa-facebook-f'                          ,'&#xf09a;'
Go
spEditConfString -5,'fa-facebook-official'                   ,'&#xf230;'
Go
spEditConfString -5,'fa-facebook-square'                     ,'&#xf082;'
Go
spEditConfString -5,'fa-fast-backward'                       ,'&#xf049;'
Go
spEditConfString -5,'fa-fast-forward'                        ,'&#xf050;'
Go
spEditConfString -5,'fa-fax'                                 ,'&#xf1ac;'
Go
spEditConfString -5,'fa-feed'                                ,'&#xf09e;'
Go
spEditConfString -5,'fa-female'                              ,'&#xf182;'
Go
spEditConfString -5,'fa-fighter-jet'                         ,'&#xf0fb;'
Go
spEditConfString -5,'fa-file'                                ,'&#xf15b;'
Go
spEditConfString -5,'fa-file-archive-o'                      ,'&#xf1c6;'
Go
spEditConfString -5,'fa-file-audio-o'                        ,'&#xf1c7;'
Go
spEditConfString -5,'fa-file-code-o'                         ,'&#xf1c9;'
Go
spEditConfString -5,'fa-file-excel-o'                        ,'&#xf1c3;'
Go
spEditConfString -5,'fa-file-image-o'                        ,'&#xf1c5;'
Go
spEditConfString -5,'fa-file-movie-o'                        ,'&#xf1c8;'
Go
spEditConfString -5,'fa-file-o'                              ,'&#xf016;'
Go
spEditConfString -5,'fa-file-pdf-o'                          ,'&#xf1c1;'
Go
spEditConfString -5,'fa-file-photo-o'                        ,'&#xf1c5;'
Go
spEditConfString -5,'fa-file-picture-o'                      ,'&#xf1c5;'
Go
spEditConfString -5,'fa-file-powerpoint-o'                   ,'&#xf1c4;'
Go
spEditConfString -5,'fa-file-sound-o'                        ,'&#xf1c7;'
Go
spEditConfString -5,'fa-file-text'                           ,'&#xf15c;'
Go
spEditConfString -5,'fa-file-text-o'                         ,'&#xf0f6;'
Go
spEditConfString -5,'fa-file-video-o'                        ,'&#xf1c8;'
Go
spEditConfString -5,'fa-file-word-o'                         ,'&#xf1c2;'
Go
spEditConfString -5,'fa-file-zip-o'                          ,'&#xf1c6;'
Go
spEditConfString -5,'fa-files-o'                             ,'&#xf0c5;'
Go
spEditConfString -5,'fa-film'                                ,'&#xf008;'
Go
spEditConfString -5,'fa-filter'                              ,'&#xf0b0;'
Go
spEditConfString -5,'fa-fire'                                ,'&#xf06d;'
Go
spEditConfString -5,'fa-fire-extinguisher'                   ,'&#xf134;'
Go
spEditConfString -5,'fa-firefox'                             ,'&#xf269;'
Go
spEditConfString -5,'fa-first-order'                         ,'&#xf2b0;'
Go
spEditConfString -5,'fa-flag'                                ,'&#xf024;'
Go
spEditConfString -5,'fa-flag-checkered'                      ,'&#xf11e;'
Go
spEditConfString -5,'fa-flag-o'                              ,'&#xf11d;'
Go
spEditConfString -5,'fa-flash'                               ,'&#xf0e7;'
Go
spEditConfString -5,'fa-flask'                               ,'&#xf0c3;'
Go
spEditConfString -5,'fa-flickr'                              ,'&#xf16e;'
Go
spEditConfString -5,'fa-floppy-o'                            ,'&#xf0c7;'
Go
spEditConfString -5,'fa-folder'                              ,'&#xf07b;'
Go
spEditConfString -5,'fa-folder-o'                            ,'&#xf114;'
Go
spEditConfString -5,'fa-folder-open'                         ,'&#xf07c;'
Go
spEditConfString -5,'fa-folder-open-o'                       ,'&#xf115;'
Go
spEditConfString -5,'fa-font'                                ,'&#xf031;'
Go
spEditConfString -5,'fa-font-awesome'                        ,'&#xf2b4;'
Go
spEditConfString -5,'fa-fonticons'                           ,'&#xf280;'
Go
spEditConfString -5,'fa-fort-awesome'                        ,'&#xf286;'
Go
spEditConfString -5,'fa-forumbee'                            ,'&#xf211;'
Go
spEditConfString -5,'fa-forward'                             ,'&#xf04e;'
Go
spEditConfString -5,'fa-foursquare'                          ,'&#xf180;'
Go
spEditConfString -5,'fa-frown-o'                             ,'&#xf119;'
Go
spEditConfString -5,'fa-futbol-o'                            ,'&#xf1e3;'
Go
spEditConfString -5,'fa-gamepad'                             ,'&#xf11b;'
Go
spEditConfString -5,'fa-gavel'                               ,'&#xf0e3;'
Go
spEditConfString -5,'fa-gbp'                                 ,'&#xf154;'
Go
spEditConfString -5,'fa-ge'                                  ,'&#xf1d1;'
Go
spEditConfString -5,'fa-gear'                                ,'&#xf013;'
Go
spEditConfString -5,'fa-gears'                               ,'&#xf085;'
Go
spEditConfString -5,'fa-genderless'                          ,'&#xf22d;'
Go
spEditConfString -5,'fa-get-pocket'                          ,'&#xf265;'
Go
spEditConfString -5,'fa-gg'                                  ,'&#xf260;'
Go
spEditConfString -5,'fa-gg-circle'                           ,'&#xf261;'
Go
spEditConfString -5,'fa-gift'                                ,'&#xf06b;'
Go
spEditConfString -5,'fa-git'                                 ,'&#xf1d3;'
Go
spEditConfString -5,'fa-git-square'                          ,'&#xf1d2;'
Go
spEditConfString -5,'fa-github'                              ,'&#xf09b;'
Go
spEditConfString -5,'fa-github-alt'                          ,'&#xf113;'
Go
spEditConfString -5,'fa-github-square'                       ,'&#xf092;'
Go
spEditConfString -5,'fa-gitlab'                              ,'&#xf296;'
Go
spEditConfString -5,'fa-gittip'                              ,'&#xf184;'
Go
spEditConfString -5,'fa-glass'                               ,'&#xf000;'
Go
spEditConfString -5,'fa-glide'                               ,'&#xf2a5;'
Go
spEditConfString -5,'fa-glide-g'                             ,'&#xf2a6;'
Go
spEditConfString -5,'fa-globe'                               ,'&#xf0ac;'
Go
spEditConfString -5,'fa-google'                              ,'&#xf1a0;'
Go
spEditConfString -5,'fa-google-plus'                         ,'&#xf0d5;'
Go
spEditConfString -5,'fa-google-plus-circle'                  ,'&#xf2b3;'
Go
spEditConfString -5,'fa-google-plus-official'                ,'&#xf2b3;'
Go
spEditConfString -5,'fa-google-plus-square'                  ,'&#xf0d4;'
Go
spEditConfString -5,'fa-google-wallet'                       ,'&#xf1ee;'
Go
spEditConfString -5,'fa-graduation-cap'                      ,'&#xf19d;'
Go
spEditConfString -5,'fa-gratipay'                            ,'&#xf184;'
Go
spEditConfString -5,'fa-group'                               ,'&#xf0c0;'
Go
spEditConfString -5,'fa-h-square'                            ,'&#xf0fd;'
Go
spEditConfString -5,'fa-hacker-news'                         ,'&#xf1d4;'
Go
spEditConfString -5,'fa-hand-grab-o'                         ,'&#xf255;'
Go
spEditConfString -5,'fa-hand-lizard-o'                       ,'&#xf258;'
Go
spEditConfString -5,'fa-hand-o-down'                         ,'&#xf0a7;'
Go
spEditConfString -5,'fa-hand-o-left'                         ,'&#xf0a5;'
Go
spEditConfString -5,'fa-hand-o-right'                        ,'&#xf0a4;'
Go
spEditConfString -5,'fa-hand-o-up'                           ,'&#xf0a6;'
Go
spEditConfString -5,'fa-hand-paper-o'                        ,'&#xf256;'
Go
spEditConfString -5,'fa-hand-peace-o'                        ,'&#xf25b;'
Go
spEditConfString -5,'fa-hand-pointer-o'                      ,'&#xf25a;'
Go
spEditConfString -5,'fa-hand-rock-o'                         ,'&#xf255;'
Go
spEditConfString -5,'fa-hand-scissors-o'                     ,'&#xf257;'
Go
spEditConfString -5,'fa-hand-spock-o'                        ,'&#xf259;'
Go
spEditConfString -5,'fa-hand-stop-o'                         ,'&#xf256;'
Go
spEditConfString -5,'fa-hard-of-hearing'                     ,'&#xf2a4;'
Go
spEditConfString -5,'fa-hashtag'                             ,'&#xf292;'
Go
spEditConfString -5,'fa-hdd-o'                               ,'&#xf0a0;'
Go
spEditConfString -5,'fa-header'                              ,'&#xf1dc;'
Go
spEditConfString -5,'fa-headphones'                          ,'&#xf025;'
Go
spEditConfString -5,'fa-heart'                               ,'&#xf004;'
Go
spEditConfString -5,'fa-heart-o'                             ,'&#xf08a;'
Go
spEditConfString -5,'fa-heartbeat'                           ,'&#xf21e;'
Go
spEditConfString -5,'fa-history'                             ,'&#xf1da;'
Go
spEditConfString -5,'fa-home'                                ,'&#xf015;'
Go
spEditConfString -5,'fa-hospital-o'                          ,'&#xf0f8;'
Go
spEditConfString -5,'fa-hotel'                               ,'&#xf236;'
Go
spEditConfString -5,'fa-hourglass'                           ,'&#xf254;'
Go
spEditConfString -5,'fa-hourglass-1'                         ,'&#xf251;'
Go
spEditConfString -5,'fa-hourglass-2'                         ,'&#xf252;'
Go
spEditConfString -5,'fa-hourglass-3'                         ,'&#xf253;'
Go
spEditConfString -5,'fa-hourglass-end'                       ,'&#xf253;'
Go
spEditConfString -5,'fa-hourglass-half'                      ,'&#xf252;'
Go
spEditConfString -5,'fa-hourglass-o'                         ,'&#xf250;'
Go
spEditConfString -5,'fa-hourglass-start'                     ,'&#xf251;'
Go
spEditConfString -5,'fa-houzz'                               ,'&#xf27c;'
Go
spEditConfString -5,'fa-html5'                               ,'&#xf13b;'
Go
spEditConfString -5,'fa-i-cursor'                            ,'&#xf246;'
Go
spEditConfString -5,'fa-ils'                                 ,'&#xf20b;'
Go
spEditConfString -5,'fa-image'                               ,'&#xf03e;'
Go
spEditConfString -5,'fa-inbox'                               ,'&#xf01c;'
Go
spEditConfString -5,'fa-indent'                              ,'&#xf03c;'
Go
spEditConfString -5,'fa-industry'                            ,'&#xf275;'
Go
spEditConfString -5,'fa-info'                                ,'&#xf129;'
Go
spEditConfString -5,'fa-info-circle'                         ,'&#xf05a;'
Go
spEditConfString -5,'fa-inr'                                 ,'&#xf156;'
Go
spEditConfString -5,'fa-instagram'                           ,'&#xf16d;'
Go
spEditConfString -5,'fa-institution'                         ,'&#xf19c;'
Go
spEditConfString -5,'fa-internet-explorer'                   ,'&#xf26b;'
Go
spEditConfString -5,'fa-intersex'                            ,'&#xf224;'
Go
spEditConfString -5,'fa-ioxhost'                             ,'&#xf208;'
Go
spEditConfString -5,'fa-italic'                              ,'&#xf033;'
Go
spEditConfString -5,'fa-joomla'                              ,'&#xf1aa;'
Go
spEditConfString -5,'fa-jpy'                                 ,'&#xf157;'
Go
spEditConfString -5,'fa-jsfiddle'                            ,'&#xf1cc;'
Go
spEditConfString -5,'fa-key'                                 ,'&#xf084;'
Go
spEditConfString -5,'fa-keyboard-o'                          ,'&#xf11c;'
Go
spEditConfString -5,'fa-krw'                                 ,'&#xf159;'
Go
spEditConfString -5,'fa-language'                            ,'&#xf1ab;'
Go
spEditConfString -5,'fa-laptop'                              ,'&#xf109;'
Go
spEditConfString -5,'fa-lastfm'                              ,'&#xf202;'
Go
spEditConfString -5,'fa-lastfm-square'                       ,'&#xf203;'
Go
spEditConfString -5,'fa-leaf'                                ,'&#xf06c;'
Go
spEditConfString -5,'fa-leanpub'                             ,'&#xf212;'
Go
spEditConfString -5,'fa-legal'                               ,'&#xf0e3;'
Go
spEditConfString -5,'fa-lemon-o'                             ,'&#xf094;'
Go
spEditConfString -5,'fa-level-down'                          ,'&#xf149;'
Go
spEditConfString -5,'fa-level-up'                            ,'&#xf148;'
Go
spEditConfString -5,'fa-life-bouy'                           ,'&#xf1cd;'
Go
spEditConfString -5,'fa-life-buoy'                           ,'&#xf1cd;'
Go
spEditConfString -5,'fa-life-ring'                           ,'&#xf1cd;'
Go
spEditConfString -5,'fa-life-saver'                          ,'&#xf1cd;'
Go
spEditConfString -5,'fa-lightbulb-o'                         ,'&#xf0eb;'
Go
spEditConfString -5,'fa-line-chart'                          ,'&#xf201;'
Go
spEditConfString -5,'fa-link'                                ,'&#xf0c1;'
Go
spEditConfString -5,'fa-linkedin'                            ,'&#xf0e1;'
Go
spEditConfString -5,'fa-linkedin-square'                     ,'&#xf08c;'
Go
spEditConfString -5,'fa-linux'                               ,'&#xf17c;'
Go
spEditConfString -5,'fa-list'                                ,'&#xf03a;'
Go
spEditConfString -5,'fa-list-alt'                            ,'&#xf022;'
Go
spEditConfString -5,'fa-list-ol'                             ,'&#xf0cb;'
Go
spEditConfString -5,'fa-list-ul'                             ,'&#xf0ca;'
Go
spEditConfString -5,'fa-location-arrow'                      ,'&#xf124;'
Go
spEditConfString -5,'fa-lock'                                ,'&#xf023;'
Go
spEditConfString -5,'fa-long-arrow-down'                     ,'&#xf175;'
Go
spEditConfString -5,'fa-long-arrow-left'                     ,'&#xf177;'
Go
spEditConfString -5,'fa-long-arrow-right'                    ,'&#xf178;'
Go
spEditConfString -5,'fa-long-arrow-up'                       ,'&#xf176;'
Go
spEditConfString -5,'fa-low-vision'                          ,'&#xf2a8;'
Go
spEditConfString -5,'fa-magic'                               ,'&#xf0d0;'
Go
spEditConfString -5,'fa-magnet'                              ,'&#xf076;'
Go
spEditConfString -5,'fa-mail-forward'                        ,'&#xf064;'
Go
spEditConfString -5,'fa-mail-reply'                          ,'&#xf112;'
Go
spEditConfString -5,'fa-mail-reply-all'                      ,'&#xf122;'
Go
spEditConfString -5,'fa-male'                                ,'&#xf183;'
Go
spEditConfString -5,'fa-map'                                 ,'&#xf279;'
Go
spEditConfString -5,'fa-map-marker'                          ,'&#xf041;'
Go
spEditConfString -5,'fa-map-o'                               ,'&#xf278;'
Go
spEditConfString -5,'fa-map-pin'                             ,'&#xf276;'
Go
spEditConfString -5,'fa-map-signs'                           ,'&#xf277;'
Go
spEditConfString -5,'fa-mars'                                ,'&#xf222;'
Go
spEditConfString -5,'fa-mars-double'                         ,'&#xf227;'
Go
spEditConfString -5,'fa-mars-stroke'                         ,'&#xf229;'
Go
spEditConfString -5,'fa-mars-stroke-h'                       ,'&#xf22b;'
Go
spEditConfString -5,'fa-mars-stroke-v'                       ,'&#xf22a;'
Go
spEditConfString -5,'fa-maxcdn'                              ,'&#xf136;'
Go
spEditConfString -5,'fa-meanpath'                            ,'&#xf20c;'
Go
spEditConfString -5,'fa-medium'                              ,'&#xf23a;'
Go
spEditConfString -5,'fa-medkit'                              ,'&#xf0fa;'
Go
spEditConfString -5,'fa-meh-o'                               ,'&#xf11a;'
Go
spEditConfString -5,'fa-mercury'                             ,'&#xf223;'
Go
spEditConfString -5,'fa-microphone'                          ,'&#xf130;'
Go
spEditConfString -5,'fa-microphone-slash'                    ,'&#xf131;'
Go
spEditConfString -5,'fa-minus'                               ,'&#xf068;'
Go
spEditConfString -5,'fa-minus-circle'                        ,'&#xf056;'
Go
spEditConfString -5,'fa-minus-square'                        ,'&#xf146;'
Go
spEditConfString -5,'fa-minus-square-o'                      ,'&#xf147;'
Go
spEditConfString -5,'fa-mixcloud'                            ,'&#xf289;'
Go
spEditConfString -5,'fa-mobile'                              ,'&#xf10b;'
Go
spEditConfString -5,'fa-mobile-phone'                        ,'&#xf10b;'
Go
spEditConfString -5,'fa-modx'                                ,'&#xf285;'
Go
spEditConfString -5,'fa-money'                               ,'&#xf0d6;'
Go
spEditConfString -5,'fa-moon-o'                              ,'&#xf186;'
Go
spEditConfString -5,'fa-mortar-board'                        ,'&#xf19d;'
Go
spEditConfString -5,'fa-motorcycle'                          ,'&#xf21c;'
Go
spEditConfString -5,'fa-mouse-pointer'                       ,'&#xf245;'
Go
spEditConfString -5,'fa-music'                               ,'&#xf001;'
Go
spEditConfString -5,'fa-navicon'                             ,'&#xf0c9;'
Go
spEditConfString -5,'fa-neuter'                              ,'&#xf22c;'
Go
spEditConfString -5,'fa-newspaper-o'                         ,'&#xf1ea;'
Go
spEditConfString -5,'fa-object-group'                        ,'&#xf247;'
Go
spEditConfString -5,'fa-object-ungroup'                      ,'&#xf248;'
Go
spEditConfString -5,'fa-odnoklassniki'                       ,'&#xf263;'
Go
spEditConfString -5,'fa-odnoklassniki-square'                ,'&#xf264;'
Go
spEditConfString -5,'fa-opencart'                            ,'&#xf23d;'
Go
spEditConfString -5,'fa-openid'                              ,'&#xf19b;'
Go
spEditConfString -5,'fa-opera'                               ,'&#xf26a;'
Go
spEditConfString -5,'fa-optin-monster'                       ,'&#xf23c;'
Go
spEditConfString -5,'fa-outdent'                             ,'&#xf03b;'
Go
spEditConfString -5,'fa-pagelines'                           ,'&#xf18c;'
Go
spEditConfString -5,'fa-paint-brush'                         ,'&#xf1fc;'
Go
spEditConfString -5,'fa-paper-plane'                         ,'&#xf1d8;'
Go
spEditConfString -5,'fa-paper-plane-o'                       ,'&#xf1d9;'
Go
spEditConfString -5,'fa-paperclip'                           ,'&#xf0c6;'
Go
spEditConfString -5,'fa-paragraph'                           ,'&#xf1dd;'
Go
spEditConfString -5,'fa-paste'                               ,'&#xf0ea;'
Go
spEditConfString -5,'fa-pause'                               ,'&#xf04c;'
Go
spEditConfString -5,'fa-pause-circle'                        ,'&#xf28b;'
Go
spEditConfString -5,'fa-pause-circle-o'                      ,'&#xf28c;'
Go
spEditConfString -5,'fa-paw'                                 ,'&#xf1b0;'
Go
spEditConfString -5,'fa-paypal'                              ,'&#xf1ed;'
Go
spEditConfString -5,'fa-pencil'                              ,'&#xf040;'
Go
spEditConfString -5,'fa-pencil-square'                       ,'&#xf14b;'
Go
spEditConfString -5,'fa-pencil-square-o'                     ,'&#xf044;'
Go
spEditConfString -5,'fa-percent'                             ,'&#xf295;'
Go
spEditConfString -5,'fa-phone'                               ,'&#xf095;'
Go
spEditConfString -5,'fa-phone-square'                        ,'&#xf098;'
Go
spEditConfString -5,'fa-photo'                               ,'&#xf03e;'
Go
spEditConfString -5,'fa-picture-o'                           ,'&#xf03e;'
Go
spEditConfString -5,'fa-pie-chart'                           ,'&#xf200;'
Go
spEditConfString -5,'fa-pied-piper'                          ,'&#xf2ae;'
Go
spEditConfString -5,'fa-pied-piper-alt'                      ,'&#xf1a8;'
Go
spEditConfString -5,'fa-pied-piper-pp'                       ,'&#xf1a7;'
Go
spEditConfString -5,'fa-pinterest'                           ,'&#xf0d2;'
Go
spEditConfString -5,'fa-pinterest-p'                         ,'&#xf231;'
Go
spEditConfString -5,'fa-pinterest-square'                    ,'&#xf0d3;'
Go
spEditConfString -5,'fa-plane'                               ,'&#xf072;'
Go
spEditConfString -5,'fa-play'                                ,'&#xf04b;'
Go
spEditConfString -5,'fa-play-circle'                         ,'&#xf144;'
Go
spEditConfString -5,'fa-play-circle-o'                       ,'&#xf01d;'
Go
spEditConfString -5,'fa-plug'                                ,'&#xf1e6;'
Go
spEditConfString -5,'fa-plus'                                ,'&#xf067;'
Go
spEditConfString -5,'fa-plus-circle'                         ,'&#xf055;'
Go
spEditConfString -5,'fa-plus-square'                         ,'&#xf0fe;'
Go
spEditConfString -5,'fa-plus-square-o'                       ,'&#xf196;'
Go
spEditConfString -5,'fa-power-off'                           ,'&#xf011;'
Go
spEditConfString -5,'fa-print'                               ,'&#xf02f;'
Go
spEditConfString -5,'fa-product-hunt'                        ,'&#xf288;'
Go
spEditConfString -5,'fa-puzzle-piece'                        ,'&#xf12e;'
Go
spEditConfString -5,'fa-qq'                                  ,'&#xf1d6;'
Go
spEditConfString -5,'fa-qrcode'                              ,'&#xf029;'
Go
spEditConfString -5,'fa-question'                            ,'&#xf128;'
Go
spEditConfString -5,'fa-question-circle'                     ,'&#xf059;'
Go
spEditConfString -5,'fa-question-circle-o'                   ,'&#xf29c;'
Go
spEditConfString -5,'fa-quote-left'                          ,'&#xf10d;'
Go
spEditConfString -5,'fa-quote-right'                         ,'&#xf10e;'
Go
spEditConfString -5,'fa-ra'                                  ,'&#xf1d0;'
Go
spEditConfString -5,'fa-random'                              ,'&#xf074;'
Go
spEditConfString -5,'fa-rebel'                               ,'&#xf1d0;'
Go
spEditConfString -5,'fa-recycle'                             ,'&#xf1b8;'
Go
spEditConfString -5,'fa-reddit'                              ,'&#xf1a1;'
Go
spEditConfString -5,'fa-reddit-alien'                        ,'&#xf281;'
Go
spEditConfString -5,'fa-reddit-square'                       ,'&#xf1a2;'
Go
spEditConfString -5,'fa-refresh'                             ,'&#xf021;'
Go
spEditConfString -5,'fa-registered'                          ,'&#xf25d;'
Go
spEditConfString -5,'fa-remove'                              ,'&#xf00d;'
Go
spEditConfString -5,'fa-renren'                              ,'&#xf18b;'
Go
spEditConfString -5,'fa-reorder'                             ,'&#xf0c9;'
Go
spEditConfString -5,'fa-repeat'                              ,'&#xf01e;'
Go
spEditConfString -5,'fa-reply'                               ,'&#xf112;'
Go
spEditConfString -5,'fa-reply-all'                           ,'&#xf122;'
Go
spEditConfString -5,'fa-resistance'                          ,'&#xf1d0;'
Go
spEditConfString -5,'fa-retweet'                             ,'&#xf079;'
Go
spEditConfString -5,'fa-rmb'                                 ,'&#xf157;'
Go
spEditConfString -5,'fa-road'                                ,'&#xf018;'
Go
spEditConfString -5,'fa-rocket'                              ,'&#xf135;'
Go
spEditConfString -5,'fa-rotate-left'                         ,'&#xf0e2;'
Go
spEditConfString -5,'fa-rotate-right'                        ,'&#xf01e;'
Go
spEditConfString -5,'fa-rouble'                              ,'&#xf158;'
Go
spEditConfString -5,'fa-rss'                                 ,'&#xf09e;'
Go
spEditConfString -5,'fa-rss-square'                          ,'&#xf143;'
Go
spEditConfString -5,'fa-rub'                                 ,'&#xf158;'
Go
spEditConfString -5,'fa-ruble'                               ,'&#xf158;'
Go
spEditConfString -5,'fa-rupee'                               ,'&#xf156;'
Go
spEditConfString -5,'fa-safari'                              ,'&#xf267;'
Go
spEditConfString -5,'fa-save'                                ,'&#xf0c7;'
Go
spEditConfString -5,'fa-scissors'                            ,'&#xf0c4;'
Go
spEditConfString -5,'fa-scribd'                              ,'&#xf28a;'
Go
spEditConfString -5,'fa-search'                              ,'&#xf002;'
Go
spEditConfString -5,'fa-search-minus'                        ,'&#xf010;'
Go
spEditConfString -5,'fa-search-plus'                         ,'&#xf00e;'
Go
spEditConfString -5,'fa-sellsy'                              ,'&#xf213;'
Go
spEditConfString -5,'fa-send'                                ,'&#xf1d8;'
Go
spEditConfString -5,'fa-send-o'                              ,'&#xf1d9;'
Go
spEditConfString -5,'fa-server'                              ,'&#xf233;'
Go
spEditConfString -5,'fa-share'                               ,'&#xf064;'
Go
spEditConfString -5,'fa-share-alt'                           ,'&#xf1e0;'
Go
spEditConfString -5,'fa-share-alt-square'                    ,'&#xf1e1;'
Go
spEditConfString -5,'fa-share-square'                        ,'&#xf14d;'
Go
spEditConfString -5,'fa-share-square-o'                      ,'&#xf045;'
Go
spEditConfString -5,'fa-shekel'                              ,'&#xf20b;'
Go
spEditConfString -5,'fa-sheqel'                              ,'&#xf20b;'
Go
spEditConfString -5,'fa-shield'                              ,'&#xf132;'
Go
spEditConfString -5,'fa-ship'                                ,'&#xf21a;'
Go
spEditConfString -5,'fa-shirtsinbulk'                        ,'&#xf214;'
Go
spEditConfString -5,'fa-shopping-bag'                        ,'&#xf290;'
Go
spEditConfString -5,'fa-shopping-basket'                     ,'&#xf291;'
Go
spEditConfString -5,'fa-shopping-cart'                       ,'&#xf07a;'
Go
spEditConfString -5,'fa-sign-in'                             ,'&#xf090;'
Go
spEditConfString -5,'fa-sign-language'                       ,'&#xf2a7;'
Go
spEditConfString -5,'fa-sign-out'                            ,'&#xf08b;'
Go
spEditConfString -5,'fa-signal'                              ,'&#xf012;'
Go
spEditConfString -5,'fa-signing'                             ,'&#xf2a7;'
Go
spEditConfString -5,'fa-simplybuilt'                         ,'&#xf215;'
Go
spEditConfString -5,'fa-sitemap'                             ,'&#xf0e8;'
Go
spEditConfString -5,'fa-skyatlas'                            ,'&#xf216;'
Go
spEditConfString -5,'fa-skype'                               ,'&#xf17e;'
Go
spEditConfString -5,'fa-slack'                               ,'&#xf198;'
Go
spEditConfString -5,'fa-sliders'                             ,'&#xf1de;'
Go
spEditConfString -5,'fa-slideshare'                          ,'&#xf1e7;'
Go
spEditConfString -5,'fa-smile-o'                             ,'&#xf118;'
Go
spEditConfString -5,'fa-snapchat'                            ,'&#xf2ab;'
Go
spEditConfString -5,'fa-snapchat-ghost'                      ,'&#xf2ac;'
Go
spEditConfString -5,'fa-snapchat-square'                     ,'&#xf2ad;'
Go
spEditConfString -5,'fa-soccer-ball-o'                       ,'&#xf1e3;'
Go
spEditConfString -5,'fa-sort'                                ,'&#xf0dc;'
Go
spEditConfString -5,'fa-sort-alpha-asc'                      ,'&#xf15d;'
Go
spEditConfString -5,'fa-sort-alpha-desc'                     ,'&#xf15e;'
Go
spEditConfString -5,'fa-sort-amount-asc'                     ,'&#xf160;'
Go
spEditConfString -5,'fa-sort-amount-desc'                    ,'&#xf161;'
Go
spEditConfString -5,'fa-sort-asc'                            ,'&#xf0de;'
Go
spEditConfString -5,'fa-sort-desc'                           ,'&#xf0dd;'
Go
spEditConfString -5,'fa-sort-down'                           ,'&#xf0dd;'
Go
spEditConfString -5,'fa-sort-numeric-asc'                    ,'&#xf162;'
Go
spEditConfString -5,'fa-sort-numeric-desc'                   ,'&#xf163;'
Go
spEditConfString -5,'fa-sort-up'                             ,'&#xf0de;'
Go
spEditConfString -5,'fa-soundcloud'                          ,'&#xf1be;'
Go
spEditConfString -5,'fa-space-shuttle'                       ,'&#xf197;'
Go
spEditConfString -5,'fa-spinner'                             ,'&#xf110;'
Go
spEditConfString -5,'fa-spoon'                               ,'&#xf1b1;'
Go
spEditConfString -5,'fa-spotify'                             ,'&#xf1bc;'
Go
spEditConfString -5,'fa-square'                              ,'&#xf0c8;'
Go
spEditConfString -5,'fa-square-o'                            ,'&#xf096;'
Go
spEditConfString -5,'fa-stack-exchange'                      ,'&#xf18d;'
Go
spEditConfString -5,'fa-stack-overflow'                      ,'&#xf16c;'
Go
spEditConfString -5,'fa-star'                                ,'&#xf005;'
Go
spEditConfString -5,'fa-star-half'                           ,'&#xf089;'
Go
spEditConfString -5,'fa-star-half-empty'                     ,'&#xf123;'
Go
spEditConfString -5,'fa-star-half-full'                      ,'&#xf123;'
Go
spEditConfString -5,'fa-star-half-o'                         ,'&#xf123;'
Go
spEditConfString -5,'fa-star-o'                              ,'&#xf006;'
Go
spEditConfString -5,'fa-steam'                               ,'&#xf1b6;'
Go
spEditConfString -5,'fa-steam-square'                        ,'&#xf1b7;'
Go
spEditConfString -5,'fa-step-backward'                       ,'&#xf048;'
Go
spEditConfString -5,'fa-step-forward'                        ,'&#xf051;'
Go
spEditConfString -5,'fa-stethoscope'                         ,'&#xf0f1;'
Go
spEditConfString -5,'fa-sticky-note'                         ,'&#xf249;'
Go
spEditConfString -5,'fa-sticky-note-o'                       ,'&#xf24a;'
Go
spEditConfString -5,'fa-stop'                                ,'&#xf04d;'
Go
spEditConfString -5,'fa-stop-circle'                         ,'&#xf28d;'
Go
spEditConfString -5,'fa-stop-circle-o'                       ,'&#xf28e;'
Go
spEditConfString -5,'fa-street-view'                         ,'&#xf21d;'
Go
spEditConfString -5,'fa-strikethrough'                       ,'&#xf0cc;'
Go
spEditConfString -5,'fa-stumbleupon'                         ,'&#xf1a4;'
Go
spEditConfString -5,'fa-stumbleupon-circle'                  ,'&#xf1a3;'
Go
spEditConfString -5,'fa-subscript'                           ,'&#xf12c;'
Go
spEditConfString -5,'fa-subway'                              ,'&#xf239;'
Go
spEditConfString -5,'fa-suitcase'                            ,'&#xf0f2;'
Go
spEditConfString -5,'fa-sun-o'                               ,'&#xf185;'
Go
spEditConfString -5,'fa-superscript'                         ,'&#xf12b;'
Go
spEditConfString -5,'fa-support'                             ,'&#xf1cd;'
Go
spEditConfString -5,'fa-table'                               ,'&#xf0ce;'
Go
spEditConfString -5,'fa-tablet'                              ,'&#xf10a;'
Go
spEditConfString -5,'fa-tachometer'                          ,'&#xf0e4;'
Go
spEditConfString -5,'fa-tag'                                 ,'&#xf02b;'
Go
spEditConfString -5,'fa-tags'                                ,'&#xf02c;'
Go
spEditConfString -5,'fa-tasks'                               ,'&#xf0ae;'
Go
spEditConfString -5,'fa-taxi'                                ,'&#xf1ba;'
Go
spEditConfString -5,'fa-television'                          ,'&#xf26c;'
Go
spEditConfString -5,'fa-tencent-weibo'                       ,'&#xf1d5;'
Go
spEditConfString -5,'fa-terminal'                            ,'&#xf120;'
Go
spEditConfString -5,'fa-text-height'                         ,'&#xf034;'
Go
spEditConfString -5,'fa-text-width'                          ,'&#xf035;'
Go
spEditConfString -5,'fa-th'                                  ,'&#xf00a;'
Go
spEditConfString -5,'fa-th-large'                            ,'&#xf009;'
Go
spEditConfString -5,'fa-th-list'                             ,'&#xf00b;'
Go
spEditConfString -5,'fa-themeisle'                           ,'&#xf2b2;'
Go
spEditConfString -5,'fa-thumb-tack'                          ,'&#xf08d;'
Go
spEditConfString -5,'fa-thumbs-down'                         ,'&#xf165;'
Go
spEditConfString -5,'fa-thumbs-o-down'                       ,'&#xf088;'
Go
spEditConfString -5,'fa-thumbs-o-up'                         ,'&#xf087;'
Go
spEditConfString -5,'fa-thumbs-up'                           ,'&#xf164;'
Go
spEditConfString -5,'fa-ticket'                              ,'&#xf145;'
Go
spEditConfString -5,'fa-times'                               ,'&#xf00d;'
Go
spEditConfString -5,'fa-times-circle'                        ,'&#xf057;'
Go
spEditConfString -5,'fa-times-circle-o'                      ,'&#xf05c;'
Go
spEditConfString -5,'fa-tint'                                ,'&#xf043;'
Go
spEditConfString -5,'fa-toggle-down'                         ,'&#xf150;'
Go
spEditConfString -5,'fa-toggle-left'                         ,'&#xf191;'
Go
spEditConfString -5,'fa-toggle-off'                          ,'&#xf204;'
Go
spEditConfString -5,'fa-toggle-on'                           ,'&#xf205;'
Go
spEditConfString -5,'fa-toggle-right'                        ,'&#xf152;'
Go
spEditConfString -5,'fa-toggle-up'                           ,'&#xf151;'
Go
spEditConfString -5,'fa-trademark'                           ,'&#xf25c;'
Go
spEditConfString -5,'fa-train'                               ,'&#xf238;'
Go
spEditConfString -5,'fa-transgender'                         ,'&#xf224;'
Go
spEditConfString -5,'fa-transgender-alt'                     ,'&#xf225;'
Go
spEditConfString -5,'fa-trash'                               ,'&#xf1f8;'
Go
spEditConfString -5,'fa-trash-o'                             ,'&#xf014;'
Go
spEditConfString -5,'fa-tree'                                ,'&#xf1bb;'
Go
spEditConfString -5,'fa-trello'                              ,'&#xf181;'
Go
spEditConfString -5,'fa-tripadvisor'                         ,'&#xf262;'
Go
spEditConfString -5,'fa-trophy'                              ,'&#xf091;'
Go
spEditConfString -5,'fa-truck'                               ,'&#xf0d1;'
Go
spEditConfString -5,'fa-try'                                 ,'&#xf195;'
Go
spEditConfString -5,'fa-tty'                                 ,'&#xf1e4;'
Go
spEditConfString -5,'fa-tumblr'                              ,'&#xf173;'
Go
spEditConfString -5,'fa-tumblr-square'                       ,'&#xf174;'
Go
spEditConfString -5,'fa-turkish-lira'                        ,'&#xf195;'
Go
spEditConfString -5,'fa-tv'                                  ,'&#xf26c;'
Go
spEditConfString -5,'fa-twitch'                              ,'&#xf1e8;'
Go
spEditConfString -5,'fa-twitter'                             ,'&#xf099;'
Go
spEditConfString -5,'fa-twitter-square'                      ,'&#xf081;'
Go
spEditConfString -5,'fa-umbrella'                            ,'&#xf0e9;'
Go
spEditConfString -5,'fa-underline'                           ,'&#xf0cd;'
Go
spEditConfString -5,'fa-undo'                                ,'&#xf0e2;'
Go
spEditConfString -5,'fa-universal-access'                    ,'&#xf29a;'
Go
spEditConfString -5,'fa-university'                          ,'&#xf19c;'
Go
spEditConfString -5,'fa-unlink'                              ,'&#xf127;'
Go
spEditConfString -5,'fa-unlock'                              ,'&#xf09c;'
Go
spEditConfString -5,'fa-unlock-alt'                          ,'&#xf13e;'
Go
spEditConfString -5,'fa-unsorted'                            ,'&#xf0dc;'
Go
spEditConfString -5,'fa-upload'                              ,'&#xf093;'
Go
spEditConfString -5,'fa-usb'                                 ,'&#xf287;'
Go
spEditConfString -5,'fa-usd'                                 ,'&#xf155;'
Go
spEditConfString -5,'fa-user'                                ,'&#xf007;'
Go
spEditConfString -5,'fa-user-md'                             ,'&#xf0f0;'
Go
spEditConfString -5,'fa-user-plus'                           ,'&#xf234;'
Go
spEditConfString -5,'fa-user-secret'                         ,'&#xf21b;'
Go
spEditConfString -5,'fa-user-times'                          ,'&#xf235;'
Go
spEditConfString -5,'fa-users'                               ,'&#xf0c0;'
Go
spEditConfString -5,'fa-venus'                               ,'&#xf221;'
Go
spEditConfString -5,'fa-venus-double'                        ,'&#xf226;'
Go
spEditConfString -5,'fa-venus-mars'                          ,'&#xf228;'
Go
spEditConfString -5,'fa-viacoin'                             ,'&#xf237;'
Go
spEditConfString -5,'fa-viadeo'                              ,'&#xf2a9;'
Go
spEditConfString -5,'fa-viadeo-square'                       ,'&#xf2aa;'
Go
spEditConfString -5,'fa-video-camera'                        ,'&#xf03d;'
Go
spEditConfString -5,'fa-vimeo'                               ,'&#xf27d;'
Go
spEditConfString -5,'fa-vimeo-square'                        ,'&#xf194;'
Go
spEditConfString -5,'fa-vine'                                ,'&#xf1ca;'
Go
spEditConfString -5,'fa-vk'                                  ,'&#xf189;'
Go
spEditConfString -5,'fa-volume-control-phone'                ,'&#xf2a0;'
Go
spEditConfString -5,'fa-volume-down'                         ,'&#xf027;'
Go
spEditConfString -5,'fa-volume-off'                          ,'&#xf026;'
Go
spEditConfString -5,'fa-volume-up'                           ,'&#xf028;'
Go
spEditConfString -5,'fa-warning'                             ,'&#xf071;'
Go
spEditConfString -5,'fa-wechat'                              ,'&#xf1d7;'
Go
spEditConfString -5,'fa-weibo'                               ,'&#xf18a;'
Go
spEditConfString -5,'fa-weixin'                              ,'&#xf1d7;'
Go
spEditConfString -5,'fa-whatsapp'                            ,'&#xf232;'
Go
spEditConfString -5,'fa-wheelchair'                          ,'&#xf193;'
Go
spEditConfString -5,'fa-wheelchair-alt'                      ,'&#xf29b;'
Go
spEditConfString -5,'fa-wifi'                                ,'&#xf1eb;'
Go
spEditConfString -5,'fa-wikipedia-w'                         ,'&#xf266;'
Go
spEditConfString -5,'fa-windows'                             ,'&#xf17a;'
Go
spEditConfString -5,'fa-won'                                 ,'&#xf159;'
Go
spEditConfString -5,'fa-wordpress'                           ,'&#xf19a;'
Go
spEditConfString -5,'fa-wpbeginner'                          ,'&#xf297;'
Go
spEditConfString -5,'fa-wpforms'                             ,'&#xf298;'
Go
spEditConfString -5,'fa-wrench'                              ,'&#xf0ad;'
Go
spEditConfString -5,'fa-xing'                                ,'&#xf168;'
Go
spEditConfString -5,'fa-xing-square'                         ,'&#xf169;'
Go
spEditConfString -5,'fa-y-combinator'                        ,'&#xf23b;'
Go
spEditConfString -5,'fa-y-combinator-square'                 ,'&#xf1d4;'
Go
spEditConfString -5,'fa-yahoo'                               ,'&#xf19e;'
Go
spEditConfString -5,'fa-yc'                                  ,'&#xf23b;'
Go
spEditConfString -5,'fa-yc-square'                           ,'&#xf1d4;'
Go
spEditConfString -5,'fa-yelp'                                ,'&#xf1e9;'
Go
spEditConfString -5,'fa-yen'                                 ,'&#xf157;'
Go
spEditConfString -5,'fa-yoast'                               ,'&#xf2b1;'
Go
spEditConfString -5,'fa-youtube'                             ,'&#xf167;'
Go
spEditConfString -5,'fa-youtube-play'                        ,'&#xf16a;'
Go
spEditConfString -5,'fa-youtube-square'                      ,'&#xf166;'
Go



--FONTS

spEditConfString -5, 'font-Arial' , 'Arial'
GO
spEditConfString -5, 'font-Arial Black' , 'Arial Black'
GO
spEditConfString -5, 'font-Arial Narrow' , 'Arial Narrow'
GO
spEditConfString -5, 'font-Arial Rounded MT Bold' , 'Arial Rounded MT Bold'
GO
spEditConfString -5, 'font-Avant Garde' , 'Avant Garde'
GO
spEditConfString -5, 'font-Calibri' , 'Calibri'
GO
spEditConfString -5, 'font-Candara' , 'Candara'
GO
spEditConfString -5, 'font-Century Gothic' , 'Century Gothic'
GO
spEditConfString -5, 'font-Franklin Gothic Medium' , 'Franklin Gothic Medium'
GO
spEditConfString -5, 'font-Futura' , 'Futura'
GO
spEditConfString -5, 'font-Geneva' , 'Geneva'
GO
spEditConfString -5, 'font-Gill Sans' , 'Gill Sans'
GO
spEditConfString -5, 'font-Helvetica' , 'Helvetica'
GO
spEditConfString -5, 'font-Impact' , 'Impact'
GO
spEditConfString -5, 'font-Lucida Grande' , 'Lucida Grande'
GO
spEditConfString -5, 'font-Optima' , 'Optima'
GO
spEditConfString -5, 'font-Segoe UI' , 'Segoe UI'
GO
spEditConfString -5, 'font-Tahoma' , 'Tahoma'
GO
spEditConfString -5, 'font-Trebuchet MS' , 'Trebuchet MS'
GO
spEditConfString -5, 'font-Verdana' , 'Verdana'
GO
spEditConfString -5, 'font-Serif' , 'Serif'
GO
spEditConfString -5, 'font-Big Caslon' , 'Big Caslon'
GO
spEditConfString -5, 'font-Bodoni MT' , 'Bodoni MT'
GO
spEditConfString -5, 'font-Book Antiqua' , 'Book Antiqua'
GO
spEditConfString -5, 'font-Calisto MT' , 'Calisto MT'
GO
spEditConfString -5, 'font-Cambria' , 'Cambria'
GO
spEditConfString -5, 'font-Didot' , 'Didot'
GO
spEditConfString -5, 'font-Garamond' , 'Garamond'
GO
spEditConfString -5, 'font-Georgia' , 'Georgia'
GO
spEditConfString -5, 'font-Goudy Old Style' , 'Goudy Old Style'
GO
spEditConfString -5, 'font-Hoefler Text' , 'Hoefler Text'
GO
spEditConfString -5, 'font-Lucida Bright' , 'Lucida Bright'
GO
spEditConfString -5, 'font-Palatino' , 'Palatino'
GO
spEditConfString -5, 'font-Perpetua' , 'Perpetua'
GO
spEditConfString -5, 'font-Rockwell' , 'Rockwell'
GO
spEditConfString -5, 'font-Rockwell Extra Bold' , 'Rockwell Extra Bold'
GO
spEditConfString -5, 'font-Baskerville' , 'Baskerville'
GO
spEditConfString -5, 'font-Times New Roman' , 'Times New Roman'
GO
spEditConfString -5, 'font-Monospaced' , 'Monospaced'
GO
spEditConfString -5, 'font-Consolas' , 'Consolas'
GO
spEditConfString -5, 'font-Courier New' , 'Courier New'
GO
spEditConfString -5, 'font-Lucida Console' , 'Lucida Console'
GO
spEditConfString -5, 'font-Lucida Sans Typewriter' , 'Lucida Sans Typewriter'
GO
spEditConfString -5, 'font-Monaco' , 'Monaco'
GO
spEditConfString -5, 'font-Andale Mono' , 'Andale Mono'
GO
spEditConfString -5, 'font-Fantasy' , 'Fantasy'
GO
spEditConfString -5, 'font-Copperplate' , 'Copperplate'
GO
spEditConfString -5, 'font-Papyrus' , 'Papyrus'
GO
spEditConfString -5, 'font-Script' , 'Script'
GO
spEditConfString -5, 'font-Brush Script MT' , 'Brush Script MT'
GO












--ALTERNATIVE FONT AWESOME

spEditConfString -5,'fa-500px'                               ,''
GO
spEditConfString -5,'fa-adjust'                              ,''
GO
spEditConfString -5,'fa-adn'                                 ,''
GO
spEditConfString -5,'fa-align-center'                        ,''
GO
spEditConfString -5,'fa-align-justify'                       ,''
GO
spEditConfString -5,'fa-align-left'                          ,''
GO
spEditConfString -5,'fa-align-right'                         ,''
GO
spEditConfString -5,'fa-amazon'                              ,''
GO
spEditConfString -5,'fa-ambulance'                           ,''
GO
spEditConfString -5,'fa-american-sign-language-interpreting' ,''
GO
spEditConfString -5,'fa-anchor'                              ,''
GO
spEditConfString -5,'fa-android'                             ,''
GO
spEditConfString -5,'fa-angellist'                           ,''
GO
spEditConfString -5,'fa-angle-double-down'                   ,''
GO
spEditConfString -5,'fa-angle-double-left'                   ,''
GO
spEditConfString -5,'fa-angle-double-right'                  ,''
GO
spEditConfString -5,'fa-angle-double-up'                     ,''
GO
spEditConfString -5,'fa-angle-down'                          ,''
GO
spEditConfString -5,'fa-angle-left'                          ,''
GO
spEditConfString -5,'fa-angle-right'                         ,''
GO
spEditConfString -5,'fa-angle-up'                            ,''
GO
spEditConfString -5,'fa-apple'                               ,''
GO
spEditConfString -5,'fa-archive'                             ,''
GO
spEditConfString -5,'fa-area-chart'                          ,''
GO
spEditConfString -5,'fa-arrow-circle-down'                   ,''
GO
spEditConfString -5,'fa-arrow-circle-left'                   ,''
GO
spEditConfString -5,'fa-arrow-circle-o-down'                 ,''
GO
spEditConfString -5,'fa-arrow-circle-o-left'                 ,''
GO
spEditConfString -5,'fa-arrow-circle-o-right'                ,''
GO
spEditConfString -5,'fa-arrow-circle-o-up'                   ,''
GO
spEditConfString -5,'fa-arrow-circle-right'                  ,''
GO
spEditConfString -5,'fa-arrow-circle-up'                     ,''
GO
spEditConfString -5,'fa-arrow-down'                          ,''
GO
spEditConfString -5,'fa-arrow-left'                          ,''
GO
spEditConfString -5,'fa-arrow-right'                         ,''
GO
spEditConfString -5,'fa-arrow-up'                            ,''
GO
spEditConfString -5,'fa-arrows'                              ,''
GO
spEditConfString -5,'fa-arrows-alt'                          ,''
GO
spEditConfString -5,'fa-arrows-h'                            ,''
GO
spEditConfString -5,'fa-arrows-v'                            ,''
GO
spEditConfString -5,'fa-asl-interpreting'                    ,''
GO
spEditConfString -5,'fa-assistive-listening-systems'         ,''
GO
spEditConfString -5,'fa-asterisk'                            ,''
GO
spEditConfString -5,'fa-at'                                  ,''
GO
spEditConfString -5,'fa-audio-description'                   ,''
GO
spEditConfString -5,'fa-automobile'                          ,''
GO
spEditConfString -5,'fa-backward'                            ,''
GO
spEditConfString -5,'fa-balance-scale'                       ,''
GO
spEditConfString -5,'fa-ban'                                 ,''
GO
spEditConfString -5,'fa-bank'                                ,''
GO
spEditConfString -5,'fa-bar-chart'                           ,''
GO
spEditConfString -5,'fa-bar-chart-o'                         ,''
GO
spEditConfString -5,'fa-barcode'                             ,''
GO
spEditConfString -5,'fa-bars'                                ,''
GO
spEditConfString -5,'fa-battery-0'                           ,''
GO
spEditConfString -5,'fa-battery-1'                           ,''
GO
spEditConfString -5,'fa-battery-2'                           ,''
GO
spEditConfString -5,'fa-battery-3'                           ,''
GO
spEditConfString -5,'fa-battery-4'                           ,''
GO
spEditConfString -5,'fa-battery-empty'                       ,''
GO
spEditConfString -5,'fa-battery-full'                        ,''
GO
spEditConfString -5,'fa-battery-half'                        ,''
GO
spEditConfString -5,'fa-battery-quarter'                     ,''
GO
spEditConfString -5,'fa-battery-three-quarters'              ,''
GO
spEditConfString -5,'fa-bed'                                 ,''
GO
spEditConfString -5,'fa-beer'                                ,''
GO
spEditConfString -5,'fa-behance'                             ,''
GO
spEditConfString -5,'fa-behance-square'                      ,''
GO
spEditConfString -5,'fa-bell'                                ,''
GO
spEditConfString -5,'fa-bell-o'                              ,''
GO
spEditConfString -5,'fa-bell-slash'                          ,''
GO
spEditConfString -5,'fa-bell-slash-o'                        ,''
GO
spEditConfString -5,'fa-bicycle'                             ,''
GO
spEditConfString -5,'fa-binoculars'                          ,''
GO
spEditConfString -5,'fa-birthday-cake'                       ,''
GO
spEditConfString -5,'fa-bitbucket'                           ,''
GO
spEditConfString -5,'fa-bitbucket-square'                    ,''
GO
spEditConfString -5,'fa-bitcoin'                             ,''
GO
spEditConfString -5,'fa-black-tie'                           ,''
GO
spEditConfString -5,'fa-blind'                               ,''
GO
spEditConfString -5,'fa-bluetooth'                           ,''
GO
spEditConfString -5,'fa-bluetooth-b'                         ,''
GO
spEditConfString -5,'fa-bold'                                ,''
GO
spEditConfString -5,'fa-bolt'                                ,''
GO
spEditConfString -5,'fa-bomb'                                ,''
GO
spEditConfString -5,'fa-book'                                ,''
GO
spEditConfString -5,'fa-bookmark'                            ,''
GO
spEditConfString -5,'fa-bookmark-o'                          ,''
GO
spEditConfString -5,'fa-braille'                             ,''
GO
spEditConfString -5,'fa-briefcase'                           ,''
GO
spEditConfString -5,'fa-btc'                                 ,''
GO
spEditConfString -5,'fa-bug'                                 ,''
GO
spEditConfString -5,'fa-building'                            ,''
GO
spEditConfString -5,'fa-building-o'                          ,''
GO
spEditConfString -5,'fa-bullhorn'                            ,''
GO
spEditConfString -5,'fa-bullseye'                            ,''
GO
spEditConfString -5,'fa-bus'                                 ,''
GO
spEditConfString -5,'fa-buysellads'                          ,''
GO
spEditConfString -5,'fa-cab'                                 ,''
GO
spEditConfString -5,'fa-calculator'                          ,''
GO
spEditConfString -5,'fa-calendar'                            ,''
GO
spEditConfString -5,'fa-calendar-check-o'                    ,''
GO
spEditConfString -5,'fa-calendar-minus-o'                    ,''
GO
spEditConfString -5,'fa-calendar-o'                          ,''
GO
spEditConfString -5,'fa-calendar-plus-o'                     ,''
GO
spEditConfString -5,'fa-calendar-times-o'                    ,''
GO
spEditConfString -5,'fa-camera'                              ,''
GO
spEditConfString -5,'fa-camera-retro'                        ,''
GO
spEditConfString -5,'fa-car'                                 ,''
GO
spEditConfString -5,'fa-caret-down'                          ,''
GO
spEditConfString -5,'fa-caret-left'                          ,''
GO
spEditConfString -5,'fa-caret-right'                         ,''
GO
spEditConfString -5,'fa-caret-square-o-down'                 ,''
GO
spEditConfString -5,'fa-caret-square-o-left'                 ,''
GO
spEditConfString -5,'fa-caret-square-o-right'                ,''
GO
spEditConfString -5,'fa-caret-square-o-up'                   ,''
GO
spEditConfString -5,'fa-caret-up'                            ,''
GO
spEditConfString -5,'fa-cart-arrow-down'                     ,''
GO
spEditConfString -5,'fa-cart-plus'                           ,''
GO
spEditConfString -5,'fa-cc'                                  ,''
GO
spEditConfString -5,'fa-cc-amex'                             ,''
GO
spEditConfString -5,'fa-cc-diners-club'                      ,''
GO
spEditConfString -5,'fa-cc-discover'                         ,''
GO
spEditConfString -5,'fa-cc-jcb'                              ,''
GO
spEditConfString -5,'fa-cc-mastercard'                       ,''
GO
spEditConfString -5,'fa-cc-paypal'                           ,''
GO
spEditConfString -5,'fa-cc-stripe'                           ,''
GO
spEditConfString -5,'fa-cc-visa'                             ,''
GO
spEditConfString -5,'fa-certificate'                         ,''
GO
spEditConfString -5,'fa-chain'                               ,''
GO
spEditConfString -5,'fa-chain-broken'                        ,''
GO
spEditConfString -5,'fa-check'                               ,''
GO
spEditConfString -5,'fa-check-circle'                        ,''
GO
spEditConfString -5,'fa-check-circle-o'                      ,''
GO
spEditConfString -5,'fa-check-square'                        ,''
GO
spEditConfString -5,'fa-check-square-o'                      ,''
GO
spEditConfString -5,'fa-chevron-circle-down'                 ,''
GO
spEditConfString -5,'fa-chevron-circle-left'                 ,''
GO
spEditConfString -5,'fa-chevron-circle-right'                ,''
GO
spEditConfString -5,'fa-chevron-circle-up'                   ,''
GO
spEditConfString -5,'fa-chevron-down'                        ,''
GO
spEditConfString -5,'fa-chevron-left'                        ,''
GO
spEditConfString -5,'fa-chevron-right'                       ,''
GO
spEditConfString -5,'fa-chevron-up'                          ,''
GO
spEditConfString -5,'fa-child'                               ,''
GO
spEditConfString -5,'fa-chrome'                              ,''
GO
spEditConfString -5,'fa-circle'                              ,''
GO
spEditConfString -5,'fa-circle-o'                            ,''
GO
spEditConfString -5,'fa-circle-o-notch'                      ,''
GO
spEditConfString -5,'fa-circle-thin'                         ,''
GO
spEditConfString -5,'fa-clipboard'                           ,''
GO
spEditConfString -5,'fa-clock-o'                             ,''
GO
spEditConfString -5,'fa-clone'                               ,''
GO
spEditConfString -5,'fa-close'                               ,''
GO
spEditConfString -5,'fa-cloud'                               ,''
GO
spEditConfString -5,'fa-cloud-download'                      ,''
GO
spEditConfString -5,'fa-cloud-upload'                        ,''
GO
spEditConfString -5,'fa-cny'                                 ,''
GO
spEditConfString -5,'fa-code'                                ,''
GO
spEditConfString -5,'fa-code-fork'                           ,''
GO
spEditConfString -5,'fa-codepen'                             ,''
GO
spEditConfString -5,'fa-codiepie'                            ,''
GO
spEditConfString -5,'fa-coffee'                              ,''
GO
spEditConfString -5,'fa-cog'                                 ,''
GO
spEditConfString -5,'fa-cogs'                                ,''
GO
spEditConfString -5,'fa-columns'                             ,''
GO
spEditConfString -5,'fa-comment'                             ,''
GO
spEditConfString -5,'fa-comment-o'                           ,''
GO
spEditConfString -5,'fa-commenting'                          ,''
GO
spEditConfString -5,'fa-commenting-o'                        ,''
GO
spEditConfString -5,'fa-comments'                            ,''
GO
spEditConfString -5,'fa-comments-o'                          ,''
GO
spEditConfString -5,'fa-compass'                             ,''
GO
spEditConfString -5,'fa-compress'                            ,''
GO
spEditConfString -5,'fa-connectdevelop'                      ,''
GO
spEditConfString -5,'fa-contao'                              ,''
GO
spEditConfString -5,'fa-copy'                                ,''
GO
spEditConfString -5,'fa-copyright'                           ,''
GO
spEditConfString -5,'fa-creative-commons'                    ,''
GO
spEditConfString -5,'fa-credit-card'                         ,''
GO
spEditConfString -5,'fa-credit-card-alt'                     ,''
GO
spEditConfString -5,'fa-crop'                                ,''
GO
spEditConfString -5,'fa-crosshairs'                          ,''
GO
spEditConfString -5,'fa-css3'                                ,''
GO
spEditConfString -5,'fa-cube'                                ,''
GO
spEditConfString -5,'fa-cubes'                               ,''
GO
spEditConfString -5,'fa-cut'                                 ,''
GO
spEditConfString -5,'fa-cutlery'                             ,''
GO
spEditConfString -5,'fa-dashboard'                           ,''
GO
spEditConfString -5,'fa-dashcube'                            ,''
GO
spEditConfString -5,'fa-database'                            ,''
GO
spEditConfString -5,'fa-deaf'                                ,''
GO
spEditConfString -5,'fa-deafness'                            ,''
GO
spEditConfString -5,'fa-dedent'                              ,''
GO
spEditConfString -5,'fa-delicious'                           ,''
GO
spEditConfString -5,'fa-desktop'                             ,''
GO
spEditConfString -5,'fa-deviantart'                          ,''
GO
spEditConfString -5,'fa-diamond'                             ,''
GO
spEditConfString -5,'fa-digg'                                ,''
GO
spEditConfString -5,'fa-dollar'                              ,''
GO
spEditConfString -5,'fa-dot-circle-o'                        ,''
GO
spEditConfString -5,'fa-download'                            ,''
GO
spEditConfString -5,'fa-dribbble'                            ,''
GO
spEditConfString -5,'fa-dropbox'                             ,''
GO
spEditConfString -5,'fa-drupal'                              ,''
GO
spEditConfString -5,'fa-edge'                                ,''
GO
spEditConfString -5,'fa-edit'                                ,''
GO
spEditConfString -5,'fa-eject'                               ,''
GO
spEditConfString -5,'fa-ellipsis-h'                          ,''
GO
spEditConfString -5,'fa-ellipsis-v'                          ,''
GO
spEditConfString -5,'fa-empire'                              ,''
GO
spEditConfString -5,'fa-envelope'                            ,''
GO
spEditConfString -5,'fa-envelope-o'                          ,''
GO
spEditConfString -5,'fa-envelope-square'                     ,''
GO
spEditConfString -5,'fa-envira'                              ,''
GO
spEditConfString -5,'fa-eraser'                              ,''
GO
spEditConfString -5,'fa-eur'                                 ,''
GO
spEditConfString -5,'fa-euro'                                ,''
GO
spEditConfString -5,'fa-exchange'                            ,''
GO
spEditConfString -5,'fa-exclamation'                         ,''
GO
spEditConfString -5,'fa-exclamation-circle'                  ,''
GO
spEditConfString -5,'fa-exclamation-triangle'                ,''
GO
spEditConfString -5,'fa-expand'                              ,''
GO
spEditConfString -5,'fa-expeditedssl'                        ,''
GO
spEditConfString -5,'fa-external-link'                       ,''
GO
spEditConfString -5,'fa-external-link-square'                ,''
GO
spEditConfString -5,'fa-eye'                                 ,''
GO
spEditConfString -5,'fa-eye-slash'                           ,''
GO
spEditConfString -5,'fa-eyedropper'                          ,''
GO
spEditConfString -5,'fa-fa'                                  ,''
GO
spEditConfString -5,'fa-facebook'                            ,''
GO
spEditConfString -5,'fa-facebook-f'                          ,''
GO
spEditConfString -5,'fa-facebook-official'                   ,''
GO
spEditConfString -5,'fa-facebook-square'                     ,''
GO
spEditConfString -5,'fa-fast-backward'                       ,''
GO
spEditConfString -5,'fa-fast-forward'                        ,''
GO
spEditConfString -5,'fa-fax'                                 ,''
GO
spEditConfString -5,'fa-feed'                                ,''
GO
spEditConfString -5,'fa-female'                              ,''
GO
spEditConfString -5,'fa-fighter-jet'                         ,''
GO
spEditConfString -5,'fa-file'                                ,''
GO
spEditConfString -5,'fa-file-archive-o'                      ,''
GO
spEditConfString -5,'fa-file-audio-o'                        ,''
GO
spEditConfString -5,'fa-file-code-o'                         ,''
GO
spEditConfString -5,'fa-file-excel-o'                        ,''
GO
spEditConfString -5,'fa-file-image-o'                        ,''
GO
spEditConfString -5,'fa-file-movie-o'                        ,''
GO
spEditConfString -5,'fa-file-o'                              ,''
GO
spEditConfString -5,'fa-file-pdf-o'                          ,''
GO
spEditConfString -5,'fa-file-photo-o'                        ,''
GO
spEditConfString -5,'fa-file-picture-o'                      ,''
GO
spEditConfString -5,'fa-file-powerpoint-o'                   ,''
GO
spEditConfString -5,'fa-file-sound-o'                        ,''
GO
spEditConfString -5,'fa-file-text'                           ,''
GO
spEditConfString -5,'fa-file-text-o'                         ,''
GO
spEditConfString -5,'fa-file-video-o'                        ,''
GO
spEditConfString -5,'fa-file-word-o'                         ,''
GO
spEditConfString -5,'fa-file-zip-o'                          ,''
GO
spEditConfString -5,'fa-files-o'                             ,''
GO
spEditConfString -5,'fa-film'                                ,''
GO
spEditConfString -5,'fa-filter'                              ,''
GO
spEditConfString -5,'fa-fire'                                ,''
GO
spEditConfString -5,'fa-fire-extinguisher'                   ,''
GO
spEditConfString -5,'fa-firefox'                             ,''
GO
spEditConfString -5,'fa-first-order'                         ,''
GO
spEditConfString -5,'fa-flag'                                ,''
GO
spEditConfString -5,'fa-flag-checkered'                      ,''
GO
spEditConfString -5,'fa-flag-o'                              ,''
GO
spEditConfString -5,'fa-flash'                               ,''
GO
spEditConfString -5,'fa-flask'                               ,''
GO
spEditConfString -5,'fa-flickr'                              ,''
GO
spEditConfString -5,'fa-floppy-o'                            ,''
GO
spEditConfString -5,'fa-folder'                              ,''
GO
spEditConfString -5,'fa-folder-o'                            ,''
GO
spEditConfString -5,'fa-folder-open'                         ,''
GO
spEditConfString -5,'fa-folder-open-o'                       ,''
GO
spEditConfString -5,'fa-font'                                ,''
GO
spEditConfString -5,'fa-font-awesome'                        ,''
GO
spEditConfString -5,'fa-fonticons'                           ,''
GO
spEditConfString -5,'fa-fort-awesome'                        ,''
GO
spEditConfString -5,'fa-forumbee'                            ,''
GO
spEditConfString -5,'fa-forward'                             ,''
GO
spEditConfString -5,'fa-foursquare'                          ,''
GO
spEditConfString -5,'fa-frown-o'                             ,''
GO
spEditConfString -5,'fa-futbol-o'                            ,''
GO
spEditConfString -5,'fa-gamepad'                             ,''
GO
spEditConfString -5,'fa-gavel'                               ,''
GO
spEditConfString -5,'fa-gbp'                                 ,''
GO
spEditConfString -5,'fa-ge'                                  ,''
GO
spEditConfString -5,'fa-gear'                                ,''
GO
spEditConfString -5,'fa-gears'                               ,''
GO
spEditConfString -5,'fa-genderless'                          ,''
GO
spEditConfString -5,'fa-get-pocket'                          ,''
GO
spEditConfString -5,'fa-gg'                                  ,''
GO
spEditConfString -5,'fa-gg-circle'                           ,''
GO
spEditConfString -5,'fa-gift'                                ,''
GO
spEditConfString -5,'fa-git'                                 ,''
GO
spEditConfString -5,'fa-git-square'                          ,''
GO
spEditConfString -5,'fa-github'                              ,''
GO
spEditConfString -5,'fa-github-alt'                          ,''
GO
spEditConfString -5,'fa-github-square'                       ,''
GO
spEditConfString -5,'fa-gitlab'                              ,''
GO
spEditConfString -5,'fa-gittip'                              ,''
GO
spEditConfString -5,'fa-glass'                               ,''
GO
spEditConfString -5,'fa-glide'                               ,''
GO
spEditConfString -5,'fa-glide-g'                             ,''
GO
spEditConfString -5,'fa-globe'                               ,''
GO
spEditConfString -5,'fa-google'                              ,''
GO
spEditConfString -5,'fa-google-plus'                         ,''
GO
spEditConfString -5,'fa-google-plus-circle'                  ,''
GO
spEditConfString -5,'fa-google-plus-official'                ,''
GO
spEditConfString -5,'fa-google-plus-square'                  ,''
GO
spEditConfString -5,'fa-google-wallet'                       ,''
GO
spEditConfString -5,'fa-graduation-cap'                      ,''
GO
spEditConfString -5,'fa-gratipay'                            ,''
GO
spEditConfString -5,'fa-group'                               ,''
GO
spEditConfString -5,'fa-h-square'                            ,''
GO
spEditConfString -5,'fa-hacker-news'                         ,''
GO
spEditConfString -5,'fa-hand-grab-o'                         ,''
GO
spEditConfString -5,'fa-hand-lizard-o'                       ,''
GO
spEditConfString -5,'fa-hand-o-down'                         ,''
GO
spEditConfString -5,'fa-hand-o-left'                         ,''
GO
spEditConfString -5,'fa-hand-o-right'                        ,''
GO
spEditConfString -5,'fa-hand-o-up'                           ,''
GO
spEditConfString -5,'fa-hand-paper-o'                        ,''
GO
spEditConfString -5,'fa-hand-peace-o'                        ,''
GO
spEditConfString -5,'fa-hand-pointer-o'                      ,''
GO
spEditConfString -5,'fa-hand-rock-o'                         ,''
GO
spEditConfString -5,'fa-hand-scissors-o'                     ,''
GO
spEditConfString -5,'fa-hand-spock-o'                        ,''
GO
spEditConfString -5,'fa-hand-stop-o'                         ,''
GO
spEditConfString -5,'fa-hard-of-hearing'                     ,''
GO
spEditConfString -5,'fa-hashtag'                             ,''
GO
spEditConfString -5,'fa-hdd-o'                               ,''
GO
spEditConfString -5,'fa-header'                              ,''
GO
spEditConfString -5,'fa-headphones'                          ,''
GO
spEditConfString -5,'fa-heart'                               ,''
GO
spEditConfString -5,'fa-heart-o'                             ,''
GO
spEditConfString -5,'fa-heartbeat'                           ,''
GO
spEditConfString -5,'fa-history'                             ,''
GO
spEditConfString -5,'fa-home'                                ,''
GO
spEditConfString -5,'fa-hospital-o'                          ,''
GO
spEditConfString -5,'fa-hotel'                               ,''
GO
spEditConfString -5,'fa-hourglass'                           ,''
GO
spEditConfString -5,'fa-hourglass-1'                         ,''
GO
spEditConfString -5,'fa-hourglass-2'                         ,''
GO
spEditConfString -5,'fa-hourglass-3'                         ,''
GO
spEditConfString -5,'fa-hourglass-end'                       ,''
GO
spEditConfString -5,'fa-hourglass-half'                      ,''
GO
spEditConfString -5,'fa-hourglass-o'                         ,''
GO
spEditConfString -5,'fa-hourglass-start'                     ,''
GO
spEditConfString -5,'fa-houzz'                               ,''
GO
spEditConfString -5,'fa-html5'                               ,''
GO
spEditConfString -5,'fa-i-cursor'                            ,''
GO
spEditConfString -5,'fa-ils'                                 ,''
GO
spEditConfString -5,'fa-image'                               ,''
GO
spEditConfString -5,'fa-inbox'                               ,''
GO
spEditConfString -5,'fa-indent'                              ,''
GO
spEditConfString -5,'fa-industry'                            ,''
GO
spEditConfString -5,'fa-info'                                ,''
GO
spEditConfString -5,'fa-info-circle'                         ,''
GO
spEditConfString -5,'fa-inr'                                 ,''
GO
spEditConfString -5,'fa-instagram'                           ,''
GO
spEditConfString -5,'fa-institution'                         ,''
GO
spEditConfString -5,'fa-internet-explorer'                   ,''
GO
spEditConfString -5,'fa-intersex'                            ,''
GO
spEditConfString -5,'fa-ioxhost'                             ,''
GO
spEditConfString -5,'fa-italic'                              ,''
GO
spEditConfString -5,'fa-joomla'                              ,''
GO
spEditConfString -5,'fa-jpy'                                 ,''
GO
spEditConfString -5,'fa-jsfiddle'                            ,''
GO
spEditConfString -5,'fa-key'                                 ,''
GO
spEditConfString -5,'fa-keyboard-o'                          ,''
GO
spEditConfString -5,'fa-krw'                                 ,''
GO
spEditConfString -5,'fa-language'                            ,''
GO
spEditConfString -5,'fa-laptop'                              ,''
GO
spEditConfString -5,'fa-lastfm'                              ,''
GO
spEditConfString -5,'fa-lastfm-square'                       ,''
GO
spEditConfString -5,'fa-leaf'                                ,''
GO
spEditConfString -5,'fa-leanpub'                             ,''
GO
spEditConfString -5,'fa-legal'                               ,''
GO
spEditConfString -5,'fa-lemon-o'                             ,''
GO
spEditConfString -5,'fa-level-down'                          ,''
GO
spEditConfString -5,'fa-level-up'                            ,''
GO
spEditConfString -5,'fa-life-bouy'                           ,''
GO
spEditConfString -5,'fa-life-buoy'                           ,''
GO
spEditConfString -5,'fa-life-ring'                           ,''
GO
spEditConfString -5,'fa-life-saver'                          ,''
GO
spEditConfString -5,'fa-lightbulb-o'                         ,''
GO
spEditConfString -5,'fa-line-chart'                          ,''
GO
spEditConfString -5,'fa-link'                                ,''
GO
spEditConfString -5,'fa-linkedin'                            ,''
GO
spEditConfString -5,'fa-linkedin-square'                     ,''
GO
spEditConfString -5,'fa-linux'                               ,''
GO
spEditConfString -5,'fa-list'                                ,''
GO
spEditConfString -5,'fa-list-alt'                            ,''
GO
spEditConfString -5,'fa-list-ol'                             ,''
GO
spEditConfString -5,'fa-list-ul'                             ,''
GO
spEditConfString -5,'fa-location-arrow'                      ,''
GO
spEditConfString -5,'fa-lock'                                ,''
GO
spEditConfString -5,'fa-long-arrow-down'                     ,''
GO
spEditConfString -5,'fa-long-arrow-left'                     ,''
GO
spEditConfString -5,'fa-long-arrow-right'                    ,''
GO
spEditConfString -5,'fa-long-arrow-up'                       ,''
GO
spEditConfString -5,'fa-low-vision'                          ,''
GO
spEditConfString -5,'fa-magic'                               ,''
GO
spEditConfString -5,'fa-magnet'                              ,''
GO
spEditConfString -5,'fa-mail-forward'                        ,''
GO
spEditConfString -5,'fa-mail-reply'                          ,''
GO
spEditConfString -5,'fa-mail-reply-all'                      ,''
GO
spEditConfString -5,'fa-male'                                ,''
GO
spEditConfString -5,'fa-map'                                 ,''
GO
spEditConfString -5,'fa-map-marker'                          ,''
GO
spEditConfString -5,'fa-map-o'                               ,''
GO
spEditConfString -5,'fa-map-pin'                             ,''
GO
spEditConfString -5,'fa-map-signs'                           ,''
GO
spEditConfString -5,'fa-mars'                                ,''
GO
spEditConfString -5,'fa-mars-double'                         ,''
GO
spEditConfString -5,'fa-mars-stroke'                         ,''
GO
spEditConfString -5,'fa-mars-stroke-h'                       ,''
GO
spEditConfString -5,'fa-mars-stroke-v'                       ,''
GO
spEditConfString -5,'fa-maxcdn'                              ,''
GO
spEditConfString -5,'fa-meanpath'                            ,''
GO
spEditConfString -5,'fa-medium'                              ,''
GO
spEditConfString -5,'fa-medkit'                              ,''
GO
spEditConfString -5,'fa-meh-o'                               ,''
GO
spEditConfString -5,'fa-mercury'                             ,''
GO
spEditConfString -5,'fa-microphone'                          ,''
GO
spEditConfString -5,'fa-microphone-slash'                    ,''
GO
spEditConfString -5,'fa-minus'                               ,''
GO
spEditConfString -5,'fa-minus-circle'                        ,''
GO
spEditConfString -5,'fa-minus-square'                        ,''
GO
spEditConfString -5,'fa-minus-square-o'                      ,''
GO
spEditConfString -5,'fa-mixcloud'                            ,''
GO
spEditConfString -5,'fa-mobile'                              ,''
GO
spEditConfString -5,'fa-mobile-phone'                        ,''
GO
spEditConfString -5,'fa-modx'                                ,''
GO
spEditConfString -5,'fa-money'                               ,''
GO
spEditConfString -5,'fa-moon-o'                              ,''
GO
spEditConfString -5,'fa-mortar-board'                        ,''
GO
spEditConfString -5,'fa-motorcycle'                          ,''
GO
spEditConfString -5,'fa-mouse-pointer'                       ,''
GO
spEditConfString -5,'fa-music'                               ,''
GO
spEditConfString -5,'fa-navicon'                             ,''
GO
spEditConfString -5,'fa-neuter'                              ,''
GO
spEditConfString -5,'fa-newspaper-o'                         ,''
GO
spEditConfString -5,'fa-object-group'                        ,''
GO
spEditConfString -5,'fa-object-ungroup'                      ,''
GO
spEditConfString -5,'fa-odnoklassniki'                       ,''
GO
spEditConfString -5,'fa-odnoklassniki-square'                ,''
GO
spEditConfString -5,'fa-opencart'                            ,''
GO
spEditConfString -5,'fa-openid'                              ,''
GO
spEditConfString -5,'fa-opera'                               ,''
GO
spEditConfString -5,'fa-optin-monster'                       ,''
GO
spEditConfString -5,'fa-outdent'                             ,''
GO
spEditConfString -5,'fa-pagelines'                           ,''
GO
spEditConfString -5,'fa-paint-brush'                         ,''
GO
spEditConfString -5,'fa-paper-plane'                         ,''
GO
spEditConfString -5,'fa-paper-plane-o'                       ,''
GO
spEditConfString -5,'fa-paperclip'                           ,''
GO
spEditConfString -5,'fa-paragraph'                           ,''
GO
spEditConfString -5,'fa-paste'                               ,''
GO
spEditConfString -5,'fa-pause'                               ,''
GO
spEditConfString -5,'fa-pause-circle'                        ,''
GO
spEditConfString -5,'fa-pause-circle-o'                      ,''
GO
spEditConfString -5,'fa-paw'                                 ,''
GO
spEditConfString -5,'fa-paypal'                              ,''
GO
spEditConfString -5,'fa-pencil'                              ,''
GO
spEditConfString -5,'fa-pencil-square'                       ,''
GO
spEditConfString -5,'fa-pencil-square-o'                     ,''
GO
spEditConfString -5,'fa-percent'                             ,''
GO
spEditConfString -5,'fa-phone'                               ,''
GO
spEditConfString -5,'fa-phone-square'                        ,''
GO
spEditConfString -5,'fa-photo'                               ,''
GO
spEditConfString -5,'fa-picture-o'                           ,''
GO
spEditConfString -5,'fa-pie-chart'                           ,''
GO
spEditConfString -5,'fa-pied-piper'                          ,''
GO
spEditConfString -5,'fa-pied-piper-alt'                      ,''
GO
spEditConfString -5,'fa-pied-piper-pp'                       ,''
GO
spEditConfString -5,'fa-pinterest'                           ,''
GO
spEditConfString -5,'fa-pinterest-p'                         ,''
GO
spEditConfString -5,'fa-pinterest-square'                    ,''
GO
spEditConfString -5,'fa-plane'                               ,''
GO
spEditConfString -5,'fa-play'                                ,''
GO
spEditConfString -5,'fa-play-circle'                         ,''
GO
spEditConfString -5,'fa-play-circle-o'                       ,''
GO
spEditConfString -5,'fa-plug'                                ,''
GO
spEditConfString -5,'fa-plus'                                ,''
GO
spEditConfString -5,'fa-plus-circle'                         ,''
GO
spEditConfString -5,'fa-plus-square'                         ,''
GO
spEditConfString -5,'fa-plus-square-o'                       ,''
GO
spEditConfString -5,'fa-power-off'                           ,''
GO
spEditConfString -5,'fa-print'                               ,''
GO
spEditConfString -5,'fa-product-hunt'                        ,''
GO
spEditConfString -5,'fa-puzzle-piece'                        ,''
GO
spEditConfString -5,'fa-qq'                                  ,''
GO
spEditConfString -5,'fa-qrcode'                              ,''
GO
spEditConfString -5,'fa-question'                            ,''
GO
spEditConfString -5,'fa-question-circle'                     ,''
GO
spEditConfString -5,'fa-question-circle-o'                   ,''
GO
spEditConfString -5,'fa-quote-left'                          ,''
GO
spEditConfString -5,'fa-quote-right'                         ,''
GO
spEditConfString -5,'fa-ra'                                  ,''
GO
spEditConfString -5,'fa-random'                              ,''
GO
spEditConfString -5,'fa-rebel'                               ,''
GO
spEditConfString -5,'fa-recycle'                             ,''
GO
spEditConfString -5,'fa-reddit'                              ,''
GO
spEditConfString -5,'fa-reddit-alien'                        ,''
GO
spEditConfString -5,'fa-reddit-square'                       ,''
GO
spEditConfString -5,'fa-refresh'                             ,''
GO
spEditConfString -5,'fa-registered'                          ,''
GO
spEditConfString -5,'fa-remove'                              ,''
GO
spEditConfString -5,'fa-renren'                              ,''
GO
spEditConfString -5,'fa-reorder'                             ,''
GO
spEditConfString -5,'fa-repeat'                              ,''
GO
spEditConfString -5,'fa-reply'                               ,''
GO
spEditConfString -5,'fa-reply-all'                           ,''
GO
spEditConfString -5,'fa-resistance'                          ,''
GO
spEditConfString -5,'fa-retweet'                             ,''
GO
spEditConfString -5,'fa-rmb'                                 ,''
GO
spEditConfString -5,'fa-road'                                ,''
GO
spEditConfString -5,'fa-rocket'                              ,''
GO
spEditConfString -5,'fa-rotate-left'                         ,''
GO
spEditConfString -5,'fa-rotate-right'                        ,''
GO
spEditConfString -5,'fa-rouble'                              ,''
GO
spEditConfString -5,'fa-rss'                                 ,''
GO
spEditConfString -5,'fa-rss-square'                          ,''
GO
spEditConfString -5,'fa-rub'                                 ,''
GO
spEditConfString -5,'fa-ruble'                               ,''
GO
spEditConfString -5,'fa-rupee'                               ,''
GO
spEditConfString -5,'fa-safari'                              ,''
GO
spEditConfString -5,'fa-save'                                ,''
GO
spEditConfString -5,'fa-scissors'                            ,''
GO
spEditConfString -5,'fa-scribd'                              ,''
GO
spEditConfString -5,'fa-search'                              ,''
GO
spEditConfString -5,'fa-search-minus'                        ,''
GO
spEditConfString -5,'fa-search-plus'                         ,''
GO
spEditConfString -5,'fa-sellsy'                              ,''
GO
spEditConfString -5,'fa-send'                                ,''
GO
spEditConfString -5,'fa-send-o'                              ,''
GO
spEditConfString -5,'fa-server'                              ,''
GO
spEditConfString -5,'fa-share'                               ,''
GO
spEditConfString -5,'fa-share-alt'                           ,''
GO
spEditConfString -5,'fa-share-alt-square'                    ,''
GO
spEditConfString -5,'fa-share-square'                        ,''
GO
spEditConfString -5,'fa-share-square-o'                      ,''
GO
spEditConfString -5,'fa-shekel'                              ,''
GO
spEditConfString -5,'fa-sheqel'                              ,''
GO
spEditConfString -5,'fa-shield'                              ,''
GO
spEditConfString -5,'fa-ship'                                ,''
GO
spEditConfString -5,'fa-shirtsinbulk'                        ,''
GO
spEditConfString -5,'fa-shopping-bag'                        ,''
GO
spEditConfString -5,'fa-shopping-basket'                     ,''
GO
spEditConfString -5,'fa-shopping-cart'                       ,''
GO
spEditConfString -5,'fa-sign-in'                             ,''
GO
spEditConfString -5,'fa-sign-language'                       ,''
GO
spEditConfString -5,'fa-sign-out'                            ,''
GO
spEditConfString -5,'fa-signal'                              ,''
GO
spEditConfString -5,'fa-signing'                             ,''
GO
spEditConfString -5,'fa-simplybuilt'                         ,''
GO
spEditConfString -5,'fa-sitemap'                             ,''
GO
spEditConfString -5,'fa-skyatlas'                            ,''
GO
spEditConfString -5,'fa-skype'                               ,''
GO
spEditConfString -5,'fa-slack'                               ,''
GO
spEditConfString -5,'fa-sliders'                             ,''
GO
spEditConfString -5,'fa-slideshare'                          ,''
GO
spEditConfString -5,'fa-smile-o'                             ,''
GO
spEditConfString -5,'fa-snapchat'                            ,''
GO
spEditConfString -5,'fa-snapchat-ghost'                      ,''
GO
spEditConfString -5,'fa-snapchat-square'                     ,''
GO
spEditConfString -5,'fa-soccer-ball-o'                       ,''
GO
spEditConfString -5,'fa-sort'                                ,''
GO
spEditConfString -5,'fa-sort-alpha-asc'                      ,''
GO
spEditConfString -5,'fa-sort-alpha-desc'                     ,''
GO
spEditConfString -5,'fa-sort-amount-asc'                     ,''
GO
spEditConfString -5,'fa-sort-amount-desc'                    ,''
GO
spEditConfString -5,'fa-sort-asc'                            ,''
GO
spEditConfString -5,'fa-sort-desc'                           ,''
GO
spEditConfString -5,'fa-sort-down'                           ,''
GO
spEditConfString -5,'fa-sort-numeric-asc'                    ,''
GO
spEditConfString -5,'fa-sort-numeric-desc'                   ,''
GO
spEditConfString -5,'fa-sort-up'                             ,''
GO
spEditConfString -5,'fa-soundcloud'                          ,''
GO
spEditConfString -5,'fa-space-shuttle'                       ,''
GO
spEditConfString -5,'fa-spinner'                             ,''
GO
spEditConfString -5,'fa-spoon'                               ,''
GO
spEditConfString -5,'fa-spotify'                             ,''
GO
spEditConfString -5,'fa-square'                              ,''
GO
spEditConfString -5,'fa-square-o'                            ,''
GO
spEditConfString -5,'fa-stack-exchange'                      ,''
GO
spEditConfString -5,'fa-stack-overflow'                      ,''
GO
spEditConfString -5,'fa-star'                                ,''
GO
spEditConfString -5,'fa-star-half'                           ,''
GO
spEditConfString -5,'fa-star-half-empty'                     ,''
GO
spEditConfString -5,'fa-star-half-full'                      ,''
GO
spEditConfString -5,'fa-star-half-o'                         ,''
GO
spEditConfString -5,'fa-star-o'                              ,''
GO
spEditConfString -5,'fa-steam'                               ,''
GO
spEditConfString -5,'fa-steam-square'                        ,''
GO
spEditConfString -5,'fa-step-backward'                       ,''
GO
spEditConfString -5,'fa-step-forward'                        ,''
GO
spEditConfString -5,'fa-stethoscope'                         ,''
GO
spEditConfString -5,'fa-sticky-note'                         ,''
GO
spEditConfString -5,'fa-sticky-note-o'                       ,''
GO
spEditConfString -5,'fa-stop'                                ,''
GO
spEditConfString -5,'fa-stop-circle'                         ,''
GO
spEditConfString -5,'fa-stop-circle-o'                       ,''
GO
spEditConfString -5,'fa-street-view'                         ,''
GO
spEditConfString -5,'fa-strikethrough'                       ,''
GO
spEditConfString -5,'fa-stumbleupon'                         ,''
GO
spEditConfString -5,'fa-stumbleupon-circle'                  ,''
GO
spEditConfString -5,'fa-subscript'                           ,''
GO
spEditConfString -5,'fa-subway'                              ,''
GO
spEditConfString -5,'fa-suitcase'                            ,''
GO
spEditConfString -5,'fa-sun-o'                               ,''
GO
spEditConfString -5,'fa-superscript'                         ,''
GO
spEditConfString -5,'fa-support'                             ,''
GO
spEditConfString -5,'fa-table'                               ,''
GO
spEditConfString -5,'fa-tablet'                              ,''
GO
spEditConfString -5,'fa-tachometer'                          ,''
GO
spEditConfString -5,'fa-tag'                                 ,''
GO
spEditConfString -5,'fa-tags'                                ,''
GO
spEditConfString -5,'fa-tasks'                               ,''
GO
spEditConfString -5,'fa-taxi'                                ,''
GO
spEditConfString -5,'fa-television'                          ,''
GO
spEditConfString -5,'fa-tencent-weibo'                       ,''
GO
spEditConfString -5,'fa-terminal'                            ,''
GO
spEditConfString -5,'fa-text-height'                         ,''
GO
spEditConfString -5,'fa-text-width'                          ,''
GO
spEditConfString -5,'fa-th'                                  ,''
GO
spEditConfString -5,'fa-th-large'                            ,''
GO
spEditConfString -5,'fa-th-list'                             ,''
GO
spEditConfString -5,'fa-themeisle'                           ,''
GO
spEditConfString -5,'fa-thumb-tack'                          ,''
GO
spEditConfString -5,'fa-thumbs-down'                         ,''
GO
spEditConfString -5,'fa-thumbs-o-down'                       ,''
GO
spEditConfString -5,'fa-thumbs-o-up'                         ,''
GO
spEditConfString -5,'fa-thumbs-up'                           ,''
GO
spEditConfString -5,'fa-ticket'                              ,''
GO
spEditConfString -5,'fa-times'                               ,''
GO
spEditConfString -5,'fa-times-circle'                        ,''
GO
spEditConfString -5,'fa-times-circle-o'                      ,''
GO
spEditConfString -5,'fa-tint'                                ,''
GO
spEditConfString -5,'fa-toggle-down'                         ,''
GO
spEditConfString -5,'fa-toggle-left'                         ,''
GO
spEditConfString -5,'fa-toggle-off'                          ,''
GO
spEditConfString -5,'fa-toggle-on'                           ,''
GO
spEditConfString -5,'fa-toggle-right'                        ,''
GO
spEditConfString -5,'fa-toggle-up'                           ,''
GO
spEditConfString -5,'fa-trademark'                           ,''
GO
spEditConfString -5,'fa-train'                               ,''
GO
spEditConfString -5,'fa-transgender'                         ,''
GO
spEditConfString -5,'fa-transgender-alt'                     ,''
GO
spEditConfString -5,'fa-trash'                               ,''
GO
spEditConfString -5,'fa-trash-o'                             ,''
GO
spEditConfString -5,'fa-tree'                                ,''
GO
spEditConfString -5,'fa-trello'                              ,''
GO
spEditConfString -5,'fa-tripadvisor'                         ,''
GO
spEditConfString -5,'fa-trophy'                              ,''
GO
spEditConfString -5,'fa-truck'                               ,''
GO
spEditConfString -5,'fa-try'                                 ,''
GO
spEditConfString -5,'fa-tty'                                 ,''
GO
spEditConfString -5,'fa-tumblr'                              ,''
GO
spEditConfString -5,'fa-tumblr-square'                       ,''
GO
spEditConfString -5,'fa-turkish-lira'                        ,''
GO
spEditConfString -5,'fa-tv'                                  ,''
GO
spEditConfString -5,'fa-twitch'                              ,''
GO
spEditConfString -5,'fa-twitter'                             ,''
GO
spEditConfString -5,'fa-twitter-square'                      ,''
GO
spEditConfString -5,'fa-umbrella'                            ,''
GO
spEditConfString -5,'fa-underline'                           ,''
GO
spEditConfString -5,'fa-undo'                                ,''
GO
spEditConfString -5,'fa-universal-access'                    ,''
GO
spEditConfString -5,'fa-university'                          ,''
GO
spEditConfString -5,'fa-unlink'                              ,''
GO
spEditConfString -5,'fa-unlock'                              ,''
GO
spEditConfString -5,'fa-unlock-alt'                          ,''
GO
spEditConfString -5,'fa-unsorted'                            ,''
GO
spEditConfString -5,'fa-upload'                              ,''
GO
spEditConfString -5,'fa-usb'                                 ,''
GO
spEditConfString -5,'fa-usd'                                 ,''
GO
spEditConfString -5,'fa-user'                                ,''
GO
spEditConfString -5,'fa-user-md'                             ,''
GO
spEditConfString -5,'fa-user-plus'                           ,''
GO
spEditConfString -5,'fa-user-secret'                         ,''
GO
spEditConfString -5,'fa-user-times'                          ,''
GO
spEditConfString -5,'fa-users'                               ,''
GO
spEditConfString -5,'fa-venus'                               ,''
GO
spEditConfString -5,'fa-venus-double'                        ,''
GO
spEditConfString -5,'fa-venus-mars'                          ,''
GO
spEditConfString -5,'fa-viacoin'                             ,''
GO
spEditConfString -5,'fa-viadeo'                              ,''
GO
spEditConfString -5,'fa-viadeo-square'                       ,''
GO
spEditConfString -5,'fa-video-camera'                        ,''
GO
spEditConfString -5,'fa-vimeo'                               ,''
GO
spEditConfString -5,'fa-vimeo-square'                        ,''
GO
spEditConfString -5,'fa-vine'                                ,''
GO
spEditConfString -5,'fa-vk'                                  ,''
GO
spEditConfString -5,'fa-volume-control-phone'                ,''
GO
spEditConfString -5,'fa-volume-down'                         ,''
GO
spEditConfString -5,'fa-volume-off'                          ,''
GO
spEditConfString -5,'fa-volume-up'                           ,''
GO
spEditConfString -5,'fa-warning'                             ,''
GO
spEditConfString -5,'fa-wechat'                              ,''
GO
spEditConfString -5,'fa-weibo'                               ,''
GO
spEditConfString -5,'fa-weixin'                              ,''
GO
spEditConfString -5,'fa-whatsapp'                            ,''
GO
spEditConfString -5,'fa-wheelchair'                          ,''
GO
spEditConfString -5,'fa-wheelchair-alt'                      ,''
GO
spEditConfString -5,'fa-wifi'                                ,''
GO
spEditConfString -5,'fa-wikipedia-w'                         ,''
GO
spEditConfString -5,'fa-windows'                             ,''
GO
spEditConfString -5,'fa-won'                                 ,''
GO
spEditConfString -5,'fa-wordpress'                           ,''
GO
spEditConfString -5,'fa-wpbeginner'                          ,''
GO
spEditConfString -5,'fa-wpforms'                             ,''
GO
spEditConfString -5,'fa-wrench'                              ,''
GO
spEditConfString -5,'fa-xing'                                ,''
GO
spEditConfString -5,'fa-xing-square'                         ,''
GO
spEditConfString -5,'fa-y-combinator'                        ,''
GO
spEditConfString -5,'fa-y-combinator-square'                 ,''
GO
spEditConfString -5,'fa-yahoo'                               ,''
GO
spEditConfString -5,'fa-yc'                                  ,''
GO
spEditConfString -5,'fa-yc-square'                           ,''
GO
spEditConfString -5,'fa-yelp'                                ,''
GO
spEditConfString -5,'fa-yen'                                 ,''
GO
spEditConfString -5,'fa-yoast'                               ,''
GO
spEditConfString -5,'fa-youtube'                             ,''
GO
spEditConfString -5,'fa-youtube-play'                        ,''
GO
spEditConfString -5,'fa-youtube-square'                      ,''
GO

spEditConfString -5,'ba-banner-1'                      ,'background1.jpg'
GO
spEditConfString -5,'ba-banner-2'                      ,'background2.jpg'
GO
spEditConfString -5,'ba-banner-3'                      ,'background3.jpg'
GO
spEditConfString -5,'ba-banner-4'                      ,'background4.jpeg'
GO

spEditConfString -5,'HomePage_Head_Banner'                   ,'background4.jpeg'
GO



SELECT * FROM Strings
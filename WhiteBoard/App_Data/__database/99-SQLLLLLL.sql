﻿CREATE PROCEDURE spCreateQuestionBank
@InstructorID int,
@LessonTitle varchar(500),
@Description Varchar(500)
AS
BEGIN
	INSERT INTO QuestionBank VALUES(@InstructorID, @LessonTitle, @Description)
END
GO

CREATE PROCEDURE spGetQuestionBankbyID
@ID INT
AS
BEGIN
	SELECT * FROM QuestionBank WHERE QuestionBank.InstructorID=@ID;
END
GO

CREATE PROCEDURE spAddQuiz
@Quiz_title varchar(500),
@lessonID int,
@QuestionBankID int,
@items int,
@Passing int
as 
Begin
 Insert into Lesson_Quiz values(@Quiz_title,@lessonID,@QuestionBankID,@items,@Passing)
end 
Go

CREATE PROCEDURE spGetQuestionsbyID
@ID INT
AS
BEGIN
	SELECT * FROM Questions WHERE Questions.QuestionID=@ID;
END
GO
CREATE PROCEDURE spGetQuestionID
@question varchar(500)
AS
BEGIN
	SELECT * FROM Questions WHERE Questions.Question=@question;
END
GO


CREATE PROCEDURE spAddChoice
@QuestionID INT,
@ChoiceA varchar(500),
@ChoiceB varchar(500),
@ChoiceC varchar(500),
@ChoiceD varchar(500)
AS
BEGIN
	INSERT INTO MultipleChoice VALUES(@QuestionID,@ChoiceA,@ChoiceB,@ChoiceC,@ChoiceD)
END
GO



CREATE PROCEDURE spAddQuestion
@QuestionID int,
@QuestionType varchar(500),
@Question Varchar(500),
@Answer Varchar(500)
AS
BEGIN
	INSERT INTO Questions VALUES (@QuestionID, @QuestionType, @Question,@Answer)
END
GO
CREATE PROCEDURE spReadLessons
AS
BEGIN
	SELECT * FROM Lesson
END
GO

create Table QuestionBank(
ID int identity(1,1) PRIMARY KEY,
	InstructorID INT FOREIGN KEY REFERENCES Instructor(ID) NOT NULL,
	 LessonTitle varchar(500) NOT NULL,
	 [Description] varchar(500) not null,

)
create Table Questions(
ID int identity(1,1)PRIMARY KEY,
QuestionID INT FOREIGN KEY REFERENCES QuestionBank(ID) NOT NULL,
 QuestionType varchar(500) NOT NULL,
  Question varchar(500) not null,
  Answer varchar(500) not null
)

Create table Choices(
choiceID int identity(1,1)PRIMARY KEY,
QuestionID int foreign key references Questions(ID) not null,
Choice varchar(64) not null
)
CREATE TABLE MultipleChoice(
	Choice_ID INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	questionID INT FOREIGN KEY REFERENCES Questions(ID) not null,
	choice1 VARCHAR(100),
	choice2 VARCHAR(100),
	choice3 VARCHAR(100),
	choice4 VARCHAR(100),
);

CREATE TABLE lesson_Quiz(
quiz_id INT NOT NULL PRIMARY KEY IDENTITY(9000,1),
Quiz_title varchar(500),
	lesson_id INT FOREIGN KEY REFERENCES Lesson(ID),
	qb_id INT FOREIGN KEY REFERENCES QuestionBank(ID),
	 items INT,
	passing INT
)
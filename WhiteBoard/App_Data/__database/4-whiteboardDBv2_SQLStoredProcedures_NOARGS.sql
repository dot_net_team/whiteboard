/*
	 stored procedures for whiteboardDBv2
	 
	 STATIC GET ONLY PROCEDURES!! (NO ARGS)

use master
USE whiteboardDBv2
 
 */


CREATE TABLE CountryList(
	ID INT IDENTITY(100,1) PRIMARY KEY,
	CountryName VARCHAR(35)
)
DELETE FROM CountryList;
INSERT INTO CountryList VALUES ('Philippines')
INSERT INTO CountryList VALUES ('Test Country')

DELETE FROM SubscriptionPlanList;
INSERT INTO SubscriptionPlanList VALUES (1,'Basic Plan',9)
INSERT INTO SubscriptionPlanList VALUES (2,'Standard Plan',19)
INSERT INTO SubscriptionPlanList VALUES (3,'Premium Plan',49)

DELETE FROM PaymentModeList;
INSERT INTO PaymentModeList VALUES (1,'Monthly')
INSERT INTO PaymentModeList VALUES (2,'Yearly')

CREATE TABLE NameTitle(
	Title VARCHAR(5)
)
DELETE FROM NameTitle;
INSERT INTO NameTitle VALUES ('Mr.')
INSERT INTO NameTitle VALUES ('Ms.')
INSERT INTO NameTitle VALUES ('Dr.')

DELETE FROM Instructor;
DELETE FROM Student;

DELETE FROM AccountDetails;
DELETE FROM Account;
/* default Password is: qwertyqQ1!*/
/* Hard coded admin account */
EXECUTE spRegisterAccount 'admin@whiteboard.com','2acbb9e451e301cb1fe55ea02ad8b9b1009888d7bb802b2b818fb6b2ea3b42bf6d41d06fac4a8614a6d6df878f56405aab4c853c87747bd673e87160c633a0ec',0,'Admin001','Admin001','Test Country', null
GO
/*insert instructor template

	note: run queries 2,3, and 4 before creating an instructor as it is dependent on those, thanks

 */

EXECUTE spRegisterInstructor 'instructor@whiteboard.com','2acbb9e451e301cb1fe55ea02ad8b9b1009888d7bb802b2b818fb6b2ea3b42bf6d41d06fac4a8614a6d6df878f56405aab4c853c87747bd673e87160c633a0ec','fname-instructor','lname-instructor','mr.','math','mapua',1,1,'2016-12-12','test country'
GO
/*insert student template */
EXECUTE spRegisterStudent 'student@whiteboard.com','2acbb9e451e301cb1fe55ea02ad8b9b1009888d7bb802b2b818fb6b2ea3b42bf6d41d06fac4a8614a6d6df878f56405aab4c853c87747bd673e87160c633a0ec','fname-student','lname-student','school-mapua','Test Country'
GO

EXECUTE spCreateCourse 3000000, 'Hello', 'This is a description of Hello', 'This is an image location of Hello',10

-- prerequisites adding a student
update Course set CategoryID=1001 where ID= 1003
update Course set CategoryID=1000 where ID= 1001
update Course set CategoryID=1000 where ID= 1003
update Course set CategoryID=1001 where ID= 1002
update Course set CategoryID=1001 where ID= 1004
insert into Favorites values
(1001,2000000)


go
/* ##############@@@@@@@@@@  COUNTRY LIST  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetCountryList
AS
BEGIN
	SELECT * FROM CountryList
END
GO
/* ##############@@@@@@@@@@  PAYMENT LIST  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spPaymentModeList
AS
BEGIN
	SELECT * FROM PaymentModeList
END
GO

/* ##############@@@@@@@@@@  SUBS LIST  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetSubscriptionPlanList
AS
BEGIN
	SELECT * FROM SubscriptionPlanList
END
GO
/* ##############@@@@@@@@@@ TITLE LIST  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetNameTitle
AS
BEGIN
	SELECT * FROM NameTitle
END
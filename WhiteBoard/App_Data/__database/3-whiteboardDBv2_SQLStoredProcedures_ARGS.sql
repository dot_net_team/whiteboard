/*
	 stored procedures for whiteboardDBv2
	 
	 STATIC READERS ONLY!! (WITH ARGS)
use master
USE whiteboardDBv2	 
 */


/* ##############@@@@@@@@@@  GET ACCOUNT bY ID  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetAccountByID
@ID INT
AS
BEGIN
	SELECT * FROM Account 
	JOIN AccountDetails ON AccountDetails.AccountID=Account.ID
	WHERE Account.ID=@ID	
END
GO
/* ##############@@@@@@@@@@  GET ACCOUNT bY EMAIL  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetAccountByEmail
@Email VARCHAR(255)
AS
BEGIN
	SELECT * FROM Account 
	JOIN AccountDetails ON AccountDetails.AccountID=Account.ID
	WHERE Account.Email=@Email	
END
GO
/* ##############@@@@@@@@@@  GET ROLE BY Token  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetTokenInfo
@Token VARCHAR(64)
AS
BEGIN
	BEGIN TRY
		SELECT * FROM COOKIE WHERE Token=@Token
	END TRY
	BEGIN CATCH
		
	END CATCH
END
GO
/* ##############@@@@@@@@@@  GET ROLE BY AccoountID  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetAccountRoleByID
@AccountID INT
AS
BEGIN
	SELECT Roles.RoleName AS Name FROM Account 
	JOIN Roles ON Roles.RoleId=Account.[Role]
	WHERE Account.ID=@AccountID
END
GO
/* ##############@@@@@@@@@@  GET ROLE BY EMAIL  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetAccountRoleByEmail
@Email VARCHAR(255)
AS
BEGIN
	SELECT Roles.RoleName AS Name FROM Account 
	JOIN Roles ON Roles.RoleId=Account.[Role]
	WHERE Account.Email=@Email
END
GO

/* ##############@@@@@@@@@@  GET STUDENT by ID  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetStudentById
@ID INT
AS
BEGIN
	SELECT * FROM Student WHERE Student.AccountID=@ID	
END
GO
/* ##############@@@@@@@@@@  GET Instructor by ID  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetInstructorById
@ID INT
AS
BEGIN
	SELECT * FROM Instructor 
	JOIN InstructorSubscription ON InstructorSubscription.InstructorID=Instructor.ID
	WHERE Instructor.AccountID=@ID AND InstructorSubscription.Active=1
END
GO
/* ##############@@@@@@@@@@  Detele Cookie  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spDeleteUserCookie
@AccountID INT
AS
BEGIN
	DELETE FROM COOKIE WHERE COOKIE.AccountID=@AccountID
END
GO
/* ##############@@@@@@@@@@  GET Course by ID  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@PAGUNTALAN@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetCourseById
@ID INT
AS
BEGIN
	SELECT * FROM Course WHERE Course.ID=@ID
END
GO
/* ##############@@@@@@@@@@  GET Course  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@PAGUNTALAN@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetCourse
AS
BEGIN
	SELECT * FROM Course 
END
GO
/* ##############@@@@@@@@@@  GET Instructor Info  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@PAGUNTALAN@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetInstructorName
@ID INT
AS
BEGIN
	SELECT (FirstName+' '+LastName) AS Fullname FROM AccountDetails 
	WHERE AccountDetails.AccountID = 
	(Select AccountID from Instructor Where  Instructor.ID =
	(Select InstructorID from Course Where Course.ID=@ID))
END
GO
/* ##############@@@@@@@@@@  GET CourseSubscription  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@PAGUNTALAN@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetCourseSubscriptionByStudID
@ID INT,
@CourseID INT
AS
BEGIN
	SELECT * FROM CourseSubscription WHERE CourseSubscription.StudentID =
	(Select ID from Student WHERE Student.AccountID = @ID)
	AND CourseSubscription.CourseID = @CourseID
END
GO



/* ##############@@@@@@@@@@  spGetSubscriptnioPrice  @@@@@@@@@@###############  */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetSubscriptionPrice
@ID INT
AS
BEGIN
	SELECT * FROM SubscriptionPlanList WHERE ID=@ID
END
GO

/* ##############@@@@@@@@@@  spUpdateSubscriptnioPrice  @@@@@@@@@@###############  */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spUpdateSubscriptionPrice
@ID INT,
@Price INT
AS
BEGIN
	UPDATE SubscriptionPlanList SET SubscriptionPrice=@Price WHERE ID=@ID
END
GO

/* ##############@@@@@@@@@@  spUpdateLastLogin  @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spUpdateLastLogin
@ID INT
AS
BEGIN
	UPDATE AccountDetails SET LastLogin=GETUTCDATE() WHERE AccountID=@ID
END
GO

/* ##############@@@@@@@@@@  GET GetCourseFavorites  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@PAGUNTALAN@@@@@@@@@@@@###############  */
--spGetFavoriteCourses 1000002
--Select ID From Student Where AccountID = 1000002 , 2000000
--Select TOP 1  TagID From Favorites WHERE StudentID = 2000000
create PROCEDURE spGetFavoriteCourses
@StudID2 INT
AS
BEGIN
	Declare @StudId int
	declare @tagid int;

	set @studId = (Select ID From Student Where AccountID = @StudID2)
	
	set @tagid = (Select TOP 1 TagID From Favorites WHERE StudentID = @studId )

	SELECT * FROM Course Where CategoryID =@tagid
END
GO
/* ##############@@@@@@@@@@  TEST QUERIES  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */


/* ##############@@@@@@@@@@  spGetCourseByInstructorID  @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetCourseByInstructorID
@ID INT
AS
BEGIN
	SELECT * FROM Course WHERE Course.InstructorID=@ID
END
GO

/* ##############@@@@@@@@@@  spCreateCourse  @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spCreateCourse
@ID INT,
@TITLE Varchar(255),
@DESC text,
@IMGLOC Varchar(255),
@PRICE Decimal(19,4)
AS
BEGIN
	INSERT INTO Course(InstructorID,Title,[Description],ImageLocation,CoursePrice) VALUES(@ID, @TITLE, @DESC, @IMGLOC, @PRICE)
END
GO

/* ##############@@@@@@@@@@  spUpdateCourse  @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spUpdateCourse
@ID INT,
@TITLE Varchar(255),
@DESC text,
@IMGLOC Varchar(255),
@PRICE Decimal(19,4)
AS
BEGIN
	UPDATE Course SET Title=@TITLE, [Description]=@DESC, ImageLocation=@IMGLOC, CoursePrice=@PRICE
	WHERE (ID=@ID) 
END
GO

/* ##############@@@@@@@@@@  spDeleteCourse  @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spDeleteCourse
@ID INT
AS
BEGIN
	DELETE FROM Course WHERE (ID=@ID)
END
GO
/* ##############@@@@@@@@@@  GET All Account iD  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetAllAccountID
AS
BEGIN
	SELECT ID,[Role] FROM Account 
END
GO
/* ##############@@@@@@@@@@  Insert into CourseSubscription  @@@@@@@@@@###############  */ 
/* ##############@@@@@@@@@@@@@@@@@@PAGUNTALAN@@@@@@@@@@@@###############  */ 
CREATE PROCEDURE spInsertIntoCourseSubscription 
@StudentID INT, 
@CourseID INT 
AS 
BEGIN 
INSERT into CourseSubscription values (@StudentID,@CourseID,'','') 
END 
go
/* ##############@@@@@@@@@@  spCheckFavoritebyStudentID   @@@@@@@@@@###############  */ 
/* ##############@@@@@@@@@@@@@@@@@@PAGUNTALAN@@@@@@@@@@@@###############  */ 
CREATE PROCEDURE spCheckFavoritebyStudentID 
@Stud_ID int
AS 
BEGIN 
	SELECT * FROM Favorites WHERE Favorites.StudentID = 
	(Select ID From Student Where AccountID = @Stud_ID)
END 
go
/* ##############@@@@@@@@@@  spGetCourseByCategory   @@@@@@@@@@###############  */ 
/* ##############@@@@@@@@@@@@@@@@@@PAGUNTALAN@@@@@@@@@@@@###############  */ 
CREATE PROCEDURE spGetCourseByCategory
@Category_ID int
AS 
BEGIN 
	SELECT * FROM Course Where Course.CategoryID = @Category_ID
END 

/* ##############@@@@@@@@@@  spGetCourseSubscriptionsOfStudent  @@@@@@@@@@###############  */ 
/* ##############@@@@@@@@@@@@@@@@@@PAGUNTALAN@@@@@@@@@@@@###############  */ 
CREATE PROCEDURE spGetCourseSubscriptionsOfStudent
@StudentID int
AS 
BEGIN 

	(SELECT CourseID FROM CourseSubscription Where CourseSubscription.StudentID =
	(Select ID From Student Where AccountID = @StudentID))
END 
/* ##############@@@@@@@@@@  spRemoveFromFavourite  @@@@@@@@@@###############  */ 
/* ##############@@@@@@@@@@@@@@@@@@PAGUNTALAN@@@@@@@@@@@@###############  */ 
CREATE PROCEDURE spRemoveFromFavourite
@StudentID int,
@TagID int
AS 
BEGIN 
     DELETE FROM Favorites WHERE Favorites.TagID = @TagID AND Favorites.StudentID =
	 (Select ID From Student Where AccountID = @StudentID)
	
END 
/* ##############@@@@@@@@@@  spAddToFavourite  @@@@@@@@@@###############  */ 
/* ##############@@@@@@@@@@@@@@@@@@PAGUNTALAN@@@@@@@@@@@@###############  */ 
CREATE PROCEDURE spAddToFavourite
@TagID int,
@StudentID int
AS 
BEGIN 
     INSERT INTO Favorites values (@TagID,@StudentID)
	
END 

GO
spGetCourseSubscriptionByStudID 1000005,1000 
GO
spGetFavoriteCourses 1000005

--drop procedure spGetFavoriteCourses

--drop procedure spGetCourseSubscriptionByStudID




/* ##############@@@@@@@@@@  TEST QUERIES  @@@@@@@@@@###############  */
/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */

/* ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */

--SELECT * FROM Student 
--select * from Account
--select * from AccountDetails
--Select * from Instructor
--Select * From COOKIE
--Select * From Course
--Select * From InstructorSubscription
--Select * From CourseSubscription
--select * from Instructor
--select * from Course
--select * from Account
--GO
--spGetStudentById 1000005
--GO
--spGetCourse 
--GO
--spGetCourseById 1000 
--GO
--spGetInstructorName 1000
--GO
--spGetCourseSubscriptionByStudID 1000002,1000 
--GO

--select * from Course
--go
--drop procedure spCheckFavoritebyStudentID 
--spCheckFavoritebyStudentID  1000005
--spGetCourseByCategory 1000
--drop procedure spGetCourseSubscriptionsOfStudent
--spGetCourseSubscriptionsOfStudent 1000005
--drop procedure spAddToFavourite
--spAddToFavourite 1001,2000001
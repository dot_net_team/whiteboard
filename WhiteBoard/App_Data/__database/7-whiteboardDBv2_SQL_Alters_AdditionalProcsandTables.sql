--use whiteboardDBv2

--ALTER TABLE Course
--ADD CoursePrice decimal(19,4) --this is already added to the main query

insert into Course values 
(3000000,'Philosophy','Test Description','philosophy.png',1000,399), 
(3000000,'Calculus','Test Description','calculus.jpg',1000,399), 
(3000000,'Current issues','Test Description','currentissues.jpg',1000,399), 
(3000000,'Algebra','Reunion of the broken pieces','algeb.jpg',1000,399), 
(3000000,'Pol Sci','Capitalism? more like Crapitalism','polsci.jpg',1000,399) 
GO

CREATE Table Lesson(
	ID INT IDENTITY(1000,1) PRIMARY KEY,
	CourseID INT FOREIGN KEY REFERENCES Course(ID) NOT NULL,
	TextContent VARCHAR(MAX)
)

ALTER TABLE Lesson
ADD Title VARCHAR(255) not null

CREATE Table FileUpload(
	ID INT IDENTITY(1000,1) PRIMARY KEY,
	LessonID INT FOREIGN KEY REFERENCES Lesson(ID) NOT NULL,
	FileLocation VARCHAR(255) NOT NULL
)
GO

/* ##############@@@@@@@@@@  spGetLessonByCourseID  @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetLessonByCourseID 
@ID INT
AS
BEGIN
	SELECT * FROM Lesson WHERE CourseID=@ID
END
GO
/* ##############@@@@@@@@@@  spCreateLesson  @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spCreateLesson
@CourseID INT,
@Title Varchar(255),
@Content Varchar(MAX)
AS
BEGIN
	INSERT INTO Lesson VALUES(@CourseID, @Content, @Title)
END
GO

/* ##############@@@@@@@@@@  spUpdateLesson  @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spUpdateLesson
@ID INT,
@CourseID INT,
@Title Varchar(255),
@Content Varchar(MAX)
AS
BEGIN
	UPDATE Lesson SET CourseID=@CourseID, TextContent=@Content, Title=@Title
	WHERE (ID=@ID)
END
GO


/* ##############@@@@@@@@@@  spDeleteLesson  @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spDeleteLesson
@ID INT
AS
BEGIN
	DELETE FROM Lesson WHERE (ID=@ID)
END
GO

/* ##############@@@@@@@@@@  spGetLessonByID  @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetLessonByID
@ID INT
AS
BEGIN
	SELECT * FROM Lesson WHERE ID=@ID
END
GO


/* ##############@@@@@@@@@@  spCreateCourse @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spCreateCourse
@ID INT,
@TITLE Varchar(255),
@DESC text,
@IMGLOC Varchar(255),
@CATID INT,
@PRICE Decimal(19,4)
AS
BEGIN
	INSERT INTO Course(InstructorID,Title,[Description],ImageLocation,CategoryID, CoursePrice) VALUES(@ID, @TITLE, @DESC, @IMGLOC, @CATID, @PRICE)
END
GO

/* ##############@@@@@@@@@@  spUpdateCourse  @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spUpdateCourse
@ID INT,
@TITLE Varchar(255),
@DESC text,
@IMGLOC Varchar(255),
@CATID INT,
@PRICE Decimal(19,4)
AS
BEGIN
	UPDATE Course SET Title=@TITLE, [Description]=@DESC, ImageLocation=@IMGLOC, CategoryID=@CATID, CoursePrice=@PRICE
	WHERE (ID=@ID) 
END
GO


/* ##############@@@@@@@@@@  spGetCategoryName  @@@@@@@@@@###############   */
/*not sure kung san gagamitin pero di ko na dinelete just in case haha*/
CREATE PROCEDURE spGetCategoryName
@ID INT
AS
BEGIN
	SELECT CategoryName from Categories where (TagID=@ID)
end
go

/* ##############@@@@@@@@@@  spReadAllCategories  @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spReadAllCategories
AS
BEGIN
	SELECT * FROM Categories
end
go

select * from FileUpload
go
/* ##############@@@@@@@@@@  spGetInstructorIDbyAccID  @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetInstructorIDbyAccID
@ID INT
AS
BEGIN
	SELECT ID from Instructor WHERE (AccountID=@ID)
END
GO

/* ##############@@@@@@@@@@  spGetInstructorIDbyCourseID  @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetInstructorIDbyCourseID
@ID INT
AS
BEGIN
	SELECT InstructorID from Course WHERE (ID=@ID)
END
GO 


---added june 22
DROP PROCEDURE spDeleteCourse
go
DROP PROCEDURE spUpdateLesson
go
CREATE PROCEDURE spGetLessonIDbyCourseID
@ID INT
AS
BEGIN
	select * from Lesson where (CourseID=@ID)
END
GO

CREATE PROCEDURE spDeleteFileByLessonID
@ID INT
AS
BEGIN
	delete from FileUpload where (LessonID=@ID)
END
GO

CREATE PROCEDURE spDeleteCourse
@ID INT
AS 
BEGIN
	delete from Lesson where (CourseID=@ID)
	delete from CourseSubscription where (CourseID=@ID)
	delete from Course where (ID=@ID)
END
GO

CREATE PROCEDURE spUpdateLesson
@ID INT,
@Title Varchar(255),
@Content Varchar(MAX)
AS
BEGIN
	UPDATE Lesson SET TextContent=@Content, Title=@Title
	WHERE (ID=@ID)
END
GO

ALTER TABLE FileUpload
ADD FileTitle varchar(255) not null
go
/* ##############@@@@@@@@@@  spAddFile @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spAddFile
@LessonID INT,
@FileLoc varchar(255),
@FileTitle varchar(255)
AS
BEGIN
	insert into FileUpload values(@LessonID, @FileLoc, @FileTitle)
END
GO 

/* ##############@@@@@@@@@@  spDeleteFile @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spDeleteFile
@ID int
AS
BEGIN
	delete from FileUpload where(ID=@ID)
END
GO 

/* ##############@@@@@@@@@@  spReadFilesByLessonID @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spReadFilesByLessonID
@LessonID int
as
begin
	select * from FileUpload where (LessonID=@LessonID)
end
go

/* ##############@@@@@@@@@@  spGetLessonIDbyFileID @@@@@@@@@@###############   */
/*       ##############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###############  */
CREATE PROCEDURE spGetLessonIDbyFileID
@FileID int
as
begin
	select LessonID from FileUpload where (ID=@FileID)
end
go

spGetLessonIDbyFileID 1009
select * from FileUpload
CREATE TABLE studentProgress(
ID int identity(1000,1) PRIMARY KEY,
StudentID INT FOREIGN KEY REFERENCES Student(ID) NOT NULL,
CourseID INT FOREIGN KEY REFERENCES Course(ID) NOT NULL,
Progress INT
)
go
CREATE PROCEDURE spGetProgressforInstructor
@ID INT
AS
BEGIN
	select studentProgress.StudentID, studentProgress.CourseID, studentProgress.Progress from studentProgress
 Inner Join Course on studentProgress.CourseID = Course.ID where (InstructorID =@ID)
END
GO 

CREATE PROCEDURE spReadFilesByID
@ID int
as
begin
	select * from FileUpload where (ID=@ID)
end
go

CREATE PROCEDURE spGetCoursePriceByCourseID
@ID int
as
begin
	select CoursePrice from Course where (Course.ID=@ID)
end
go

CREATE PROCEDURE spGetAccountIDFromStudID
@StudID int
as
begin
	select AccountID from Student where (ID=@StudID)
end
go

spGetAccountIDFromStudID 2000000
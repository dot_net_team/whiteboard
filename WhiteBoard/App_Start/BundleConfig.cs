﻿using System.Web;
using System.Web.Optimization;

namespace WhiteBoard
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/bootstrap-select.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-select.css"
                      /*,"~/Content/site.css"*/));



            bundles.Add(new StyleBundle("~/Content/basicCSS").Include(
                      "~/Content/wb/bootstrap.min.css"
                      /*,"~/Content/site.css"*/));


            bundles.Add(new StyleBundle("~/fontawesome").Include(
                      "~/Content/wb/bootstrap.min.css"
                      /*,"~/Content/site.css"*/));

            //Added during Integration -noli
            //scripts
            bundles.Add(new ScriptBundle("~/integ/angular").Include(
                        "~/Scripts/angular.js",
                        "~/Scripts/angular-charts.js"));

            bundles.Add(new ScriptBundle("~/integ/prev").Include(
                        "~/Scripts/integ/jquery.js",
                        "~/Scripts/integ/bootstrap.js",
                        "~/Scripts/integ/jquery.easing.min.js",
                        "~/Scripts/integ/classie.js",
                        "~/Scripts/integ/cbpAnimatedHeader.js",
                        "~/Scripts/integ/agency.js" 
                        ));

            bundles.Add(new ScriptBundle("~/integ/oldModal").Include(
                        "~/Scripts/integ/jquery.js",
                        "~/Scripts/integ/bootstrap.js",

                        "~/Scripts/integ/agency.js"
                        ));

            //homejs default scripts for homepage - itsmeniel :D
            bundles.Add(new ScriptBundle("~/wbJS/homejs").Include(
                        "~/Scripts/wbJS/lib/jquery.min.js",
                        "~/Scripts/wbJS/scrollmagic/uncompressed/ScrollMagic.js",
                        "~/Scripts/wbJS/scrollmagic/uncompressed/plugins/debug.addIndicators.js",
                        "~/Scripts/wbJS/jquery.js",
                        "~/Scripts/wbJS/bootstrap.js",
                        "~/Scripts/wbJS/jquery.easing.min.js",
                        "~/Scripts/wbJS/whiteboard.js"
                        ));


            //Added during Integration -noli
            //css
            bundles.Add(new StyleBundle("~/integ/home_css").Include(
                     "~/Content/integ_css/home.css",
                     "~/Content/integ_css/pricing.css",
                     "~/Content/integ_css/font-awesome.css",
                     "~/Content/integ_css/GoogleMontserratCSS.css"
                     ));

            //Home bundle for new Template - itsmeniel :D
            bundles.Add(new StyleBundle("~/wb/wb_home_css").Include(
                     "~/Content/wb/whiteboard_home.css",
                     "~/Content/wb/normalize.css",
                     "~/Content/wb/fonts/font-awesome/css/font-awesome.min.css"
                     ));
            bundles.Add(new StyleBundle("~/integ/main_css").Include(
                     "~/Content/integ_css/main.css",
                     "~/Content/integ_css/font-awesome.css",
                     "~/Content/integ_css/GoogleMontserratCSS.css"
                     ));




            //NEW ADMIN/STUDENT/INSTRUCTOR TEMPLATE - itsmeniel
            bundles.Add(new StyleBundle("~/admin/admin_header").Include(
                     "~/Content/admin/css/fullcalendar.css",
                     "~/Content/admin/css/datatables/datatables.css",
                     "~/Content/admin/css/datatables/bootstrap.datatables.css",
                     "~/Content/admin/scss/chosen.css",
                     "~/Content/admin/scss/font-awesome/font-awesome.css",
                     "~/Content/admin/css/app.css"
                     ));


            //NEW ADMIN/STUDENT/INSTRUCTOR SCRIPT - itsmeniel
            bundles.Add(new ScriptBundle("~/admin/admin_footer").Include(
               "~/Scripts/admin/jquery.sparkline.min.js",
               "~/Scripts/admin/bootstrap/tab.js",
               "~/Scripts/admin/bootstrap/dropdown.js",
               "~/Scripts/admin/bootstrap/collapse.js",
               "~/Scripts/admin/bootstrap/transition.js",
               "~/Scripts/admin/bootstrap/tooltip.js",
               "~/Scripts/admin/jquery.knob.js",
               "~/Scripts/admin/fullcalendar.min.js",
               "~/Scripts/admin/datatables/datatables.min.js",
               "~/Scripts/admin/chosen.jquery.min.js",
               "~/Scripts/admin/datatables/bootstrap.datatables.js",
               "~/Scripts/admin/raphael-min.js",
               "~/Scripts/admin/morris-0.4.3.min.js",
               "~/Scripts/admin/for_pages/color_settings.js",
               "~/Scripts/admin/application.js",
               "~/Scripts/admin/for_pages/dashboard.js"
               ));



        }
    }
}
